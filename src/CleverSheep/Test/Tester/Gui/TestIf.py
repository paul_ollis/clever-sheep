#!/usr/bin/env python
"""The interface to the Tester.

<+Detailed multiline documentation+>
"""


import threading

import wx

from CleverSheep.Test import Tester


myEVT_TEST_STATE_CHANGED = wx.NewEventType()
EVT_TEST_STATE_CHANGED = wx.PyEventBinder(myEVT_TEST_STATE_CHANGED, 1)

class TestStateChangedEvent(wx.PyCommandEvent):
    def __init__(self, testId=None, 
            oldState=None, newState=None, runState=None):
        wx.PyCommandEvent.__init__(self, myEVT_TEST_STATE_CHANGED, 0)
        self.testId = testId
        self.oldState = oldState
        self.newState = newState
        self.runState = runState


class TestThread(threading.Thread):
    def __init__(self, frame, suite, options):
        threading.Thread.__init__(self)
        self.suite = suite
        self.options = options
        self.runner = None
        self.running = False
        self.frame = frame

    def postRunningState(self):
        text = ("Not Running", "Running")[bool(self.running)]
        event = TestStateChangedEvent(runState=text)
        wx.PostEvent(self.frame, event)

    def run(self):
        self.runner = Tester.makeRunner(self.options.get_option_value("select"))
        self.running = True
        self.postRunningState()
        try:
            Tester.runTests(self.suite, self.runner)
        finally:
            self.running = False
            self.postRunningState()

    def stop(self):
        """Tries to stop further test from being executed"""
        if self.runner:
            self.runner.terminate()


