#!/usr/bin/env python
"""Support for munging keys.

Rather than directly use the wxPython constants for the various keys, I prefer
to convers key events into textual representations. This means I can have
simpler code along the lines of::

    key = Keys.event2keyName(event)
    if key == "Ctrl-C":
        ...
"""



import string

import wx


#: A string containing all the control characters.
_ctrlChars = "".join([chr(ord(s) - 64) for s in string.uppercase])


#: A lookup table that can convert between wxPython key names and the symbolic
#: value. The names are the wxPython symbols without the leading 'WXK_'.
_keyMap = {}
for name in dir(wx):
    if name.startswith("WXK_"):
        v = getattr(wx, name)
        _keyMap[v] = name[4:]
        _keyMap[name[4:]] = v


def event2keyName(event):
    keyCode = event.m_keyCode
    if keyCode in _keyMap:
        c = _keyMap[keyCode]
    else:
        try:
            c = chr(keyCode)
            # The keyCode should not be things like 17 (Ctrl-O), but some times
            # it is. So we convert it to the non-Ctrl value.
            if c in _ctrlChars:
                c = chr(ord(c) + 64)
        except ValueError:
            c = "Chr(%d)" % keyCode
        else:
            if c not in string.printable:
                c = "Chr(%d)" % keyCode

    s = ""
    if event.m_shiftDown:
        if s: s += "-"
        s += "Shift"
    if event.m_controlDown:
        if s: s += "-"
        s += "Ctrl"
    if event.m_altDown:
        if s: s += "-"
        s += "Alt"
    if event.m_metaDown:
        if s: s += "-"
        s += "Meta"

    if s: s += "-"
    s += c
    return s

