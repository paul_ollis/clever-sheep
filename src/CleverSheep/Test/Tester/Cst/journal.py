"""The journal sub-command.

Usage: journal TBD

"""



from CleverSheep.Test.Tester import Cst
from CleverSheep.Test.Tester import Journal


class Test(object):
    def __init__(self):
        self.records = []
        self.notRun = []

    def addRunRecord(self, r):
        self.records.append(r)

    def addNotRun(self, r):
        self.notRun.append(r)


class Collection(object):
    def __init__(self):
        self.tests = {}

    def getItemByUid(self, uid):
        if uid not in self.tests:
            self.tests[uid] = Test()
        return self.tests[uid]

    def recordCount(self):
        n = 0
        for t in self.tests.values():
            n += len(t.records)
        return n

    def notRunCount(self):
        n = 0
        for t in self.tests.values():
            n += len(t.notRun)
        return n


def execute(context, args):
    if len(args) < 1:
        return Cst.SHOW_HELP

    cmd, args = args[0], args[1:]
    subCommand = Cst.getSubCommand(cmd, globals())
    subCommand.parse(args)


def do_cst_info(context, args):
    """The cst journal info subcommand.

    Usage: info

    The journal files are automatically detected.

    """
    journal = Journal.Journal()
    collection = Collection()
    journal.load(collection, addAll=True)
    count = collection.recordCount()
    if count:
        print("Journal contains %d records" % count)
    else:
        print("Journal is empty")

    for name in "magic long sum pickle type".split():
        if name in journal.problems:
            print("Error: %s = %s" % (name, journal.problems[name]))

    count = collection.notRunCount()
    if count:
        print("Warning: Journal contains %d NOT_RUN records" % count)
