.. CleverSheep documentation master file

Welcome to CleverSheep's documentation!
=======================================

CleverSheep is a Python package largely focused on high-level automated
testing. It provides a number of features including a test runner, support for
custom command line arguments and the ability to write asynchronous tests
through the test event store.

Some key features include:
  * A built in test runner with a wealth of command line options.
  * Support for user defined command line options.
  * Support for complex test selection logic.

If you want to raise an issue visit https://gitlab.com/LCaraccio/clever-sheep

Release Notes and Roadmap
-------------------------

See the :ref:`release_notes` for the details of each release.

See the :ref:`road_map` for details on the future direction of the library.

Installation
------------

You can simply install CleverSheep using pip:

.. code-block:: none

    pip install CleverSheep

Or if you prefer you can get the latest version from the :ref:`downloads` page.

FAQ
---

Got a question, see the :ref:`faq` page.

Documentation
=============

**Glossary**
    The :ref:`glossary` contains definitions of some of the terms used in this
    documentation.

**Basics**
    The :ref:`basics` of using CleverSheep as a test runner.

**CleverSheep Tests**
   More in depth documentation on :ref:`clever_sheep_tests` and the features
   they provide.

**Command Line Options**
   The :ref:`command_line_options` provided by CleverSheep, how to add you own
   and how to control what tests will be run.

**CleverSheep Testing Model**
   The :ref:`clever_sheep_testing_model` section gives an overview on the conceptual
   model of testing that is used.

**Library Options**
   Documentation on the :ref:`library_options` within CleverSheep.

**Poll Manager**
   In depth documentation on what the :ref:`poll_manager` is and how to use it
   to schedule operations in your tests.

**The Test Event Store**
   In depth documentation on what the :ref:`test_event_store` is, how to use
   it and how it allows asynchronous testing.

**Mock Support**
   Examples of how to use the :ref:`mock_support` provided by CleverSheep to
   act as both client and server in socket based communication.

**Troubleshooting**
   Tips to help with :ref:`troubleshooting` test issues.

**Code Snippets**
   Helpful :ref:`code_snippets` showing examples of how to do things in CleverSheep.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

CleverSheep contains some code that has been reused or simply copied.

  +----------------+-----------------------------------------------------+
  | Module         | Information                                         |
  +================+=====================================================+
  | ultraTB.py     | A neat module written by Nathan Gray.               |
  +----------------+-----------------------------------------------------+
  | decorator.py   | Signature preserving decorating, written by         |
  |                | Michele Simionato.                                  |
  |                |                                                     |
  |                | This is used pretty much unmodified.                |
  +----------------+-----------------------------------------------------+
  | Struct.py      | Inspired by an ASPN recipe by Brian McErlean.       |
  |                |                                                     |
  |                | It is possible that some snippits of his code have  |
  |                | made it into mine.                                  |
  +----------------+-----------------------------------------------------+

If you think this list is incomplete email `cleversheepframework@gmail.com` and
let us know.

A big thanks to the original author of CleverSheep Paul Ollis.