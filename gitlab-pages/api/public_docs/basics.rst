.. _basics:

Basics
######

This quick start guide will take you over the basics of CleverSheep and get you
started using it. This page contains some of the basics this should be your
starting point before continuing on to other parts of the documentation.

.. contents::
    :local:
    :depth: 1

.. highlight:: python

Overview
========

The CleverSheep test framework can support testing a single function right up
to a multi process multi box system.

The framework is based upon the common pattern used in unit testing frameworks,
but in order to accommodate higher level testing scenarios and the problems they
raise, CleverSheep has evolved to include various additional features.

Example Test File
=================

Tests in CleverSheep are organized into Suites. Each suite contains a group of
tests that normally share some relation to each other, for example they all
relate to testing a particular feature, and often need similar pre test setup
and post test teardown.

The :class:`~Test.Tester.Suite` class which all test suites should directly or
indirectly inherit from has a number of functions which can be overridden as
required to help with setup and teardown of tests. Below is an example.

::

    #!/usr/bin/env python
    """
    An example CleverSheep Test Suite
    """

    from CleverSheep.Test.Tester import Suite
    from CleverSheep.Test.Tester import runModule


    class ExampleTestSuite(Suite):
        """An example CleverSheep test Suite"""

        def suiteSetUp(self):
            """
            This function will be called once before any tests
            in this suite are run and can be used for common
            setup that only needs to be done once
            """
            pass

        def suiteTearDown(self):
            """
            This function will be called once after all tests
            in the suite have run and can be used for common
            teardown required by the tests in this suite

            It is important that this does not fail, it should not
            assume any object it will call exists as the setup may
            have failed
            """
            pass

        def setUp(self):
            """
            Per test setup, examples of things to do here may be
            restart the software, reset any config, clear any log files,
            start any other parts of the test framework
            """
            pass

        def tearDown(self):
           """
           Per test tear down, examples of things to do here, stop any
           other parts of the test framework, archive log files
           """
           pass

        # Tests should be decorated with the @test decorator
        @test(testID="TEST_ONE")
        def test_001_startup(self):
            """
            Test that process A will start up
            """
            pass

    # This should be included so that this suite can be run directly on the command line stand alone
    if __name__ == "__main__":
        runModule()

The above example is a very simple one as it is a single test in a single file,
for more in-depth examples see the :ref:`clever_sheep_tests` section.

.. note:: Test suite files (which contain the tests) should start with `test_`
          for example `test_001_startup_and_shutdown.py`. If this naming
          convention is not followed they will not be picked up and run by
          CleverSheep.

Project Root
============

CleverSheep expects there to be a file called '.PROJECT_ROOT' in the root folder
of your project. It doesn't need any contents it simply needs to exist,
CleverSheep's behavior without this file is undefined.

Adding this file will allow you to add a single file with common imports/setup,
see the :ref:`code_snippets` page for more details.

Logging
=======

Any output from the SUT or from within CleverSheep tests will be put into a file
called 'test.log' which will be placed into the tree generally where the
'all_tests.py' is (see `Running the tests`_ for what this file is).

CleverSheep contains its own logging which can be used by:

::

    from CleverSheep.Test.Tester import log

    @test
    def test_002_logging_example(self):
        """Example of the logging"""
        log.debug("This is a log message")


The levels available are 'debug', 'info', 'warning', 'error' and 'critical'.

Terminal output
===============

If the notation `#> Message` is used with CleverSheep test code it will appear
on the command line while the tests are running and will stay until another line
with the annotation is reached. This is helpful for knowing what a test is
currently doing. It is more relevant for longer running tests such as system
tests. As an alternative for pep8 compliance `# > Message` can also be used.

::

    @test
    def test_003_terminal_output_examples(self):
        """Example of the logging"""
        #> Launch the process we are testing
        self.start_process()

       #> Send 1000 messages
       self.traffic_manager_send()

        #> Wait for delivery of messages after re-encoding by sut
        self.expect_event(msgs=1000)


Alternatively :func:`console.log()` can be used this will print a static line out to
the console that is not overwritten by another call to :func:`console.log()`. This can
be useful for temporary debug to speed up debugging of a test but shouldn't be
left in as any useful information should go to the log file, it is not
compatible with the `#>` notation.

Running the tests
=================

Assuming the example above has been followed, specifically the file having a
shebang line and :func:`runModule()` in it you should be able to simply execute the
test file. CleverSheep has a range of command line options and ways to run
tests, see the :ref:`clever_sheep_tests` and :ref:`command_line_options` pages
for more details and for how to run multiple test files together.

.. _basics_marking_tests:

Marking tests
=============

As mentioned in the :ref:`basics` each test should be decorated with the
:attr:`Test.Tester.test` decorator.

.. code-block:: python

    @test
    def test_name(self):
        """Test description"""

This decorator can also be used to mark tests with meta-data. There are some
in built values which alter how the CleverSheep test runner will handle the
test and you may also choose to add your own meta-data usually in combination
with a command line option.

See the :ref:`command_line_options_user_defined_options` section for more
details on adding your own options and examples of what you might use the
meta-data for. The inbuilt options are listed below.

.. note:: These tags are case sensitive.

Mark a test as bug
------------------

Unreliable tests can be marked as `bug` while the cause is investigated.
The test will still be run but will not report as a failure. If a test marked
as bug fails the output result is `BUG` if they pass it is `BUG_PASS`. Below is
an example.

::

    @test("bug")
    def test_004_bug_test(self):
        """
        This is an example of a test marked as bug
        """
        pass

Mark a test as broken
---------------------

A test can be marked as `broken` if it simply doesn't work. It will not be run by
default or show up in test output such as the summary of tests. Below is an
example of how to do so. See the :ref:`command_line_options` for how to include
broken tests in a run.

::

    @test("broken")
    def test_005_broken_test(self):
        """
        This is an example of a test marked as broken
        """
        pass

Mark a test as todo
-------------------

A test can be marked as `todo`. This will cause it to be skipped but it will
still show up in the test summary and command line output with a result of `TODO`.
Below is an example of how to mark a test as 'todo'. This is most useful when
you want to write the outlines but not full implementation of some tests. For
example you might outline the tests you are going to write in comments before
beginning the work with your product owner reviewing them to make sure you've
not missed or misunderstood something.

::

    @test("todo")
    def test_006_todo_test(self):
        """
        This is an example of a test marked as todo
        """
        pass

Mark a test to be forked
------------------------

This will cause the test to be run in a fork, it can be helpful if the test
consumes a large amount of resources as it will enable them to be given back
sooner. Without this option it has been observed that in some instances the
amount of resources being used continues to grow until a crash occurs. Be
careful about using this option, running a test in a separate process may cause
you issues as CleverSheep is in general intended to work single threaded.

::

    @test(cs_fork=True)
    def test_007_fork_test(self):
        """
        This is an example of a test marked as to be forked
        """
        pass

Test Id
-------

Gives the test a test id which should be unique and can be used to run the
tests and is output in the summary.

::

    @test(testID="example-test-with-id")
    def test_008_has_test_id_test(self):
        """
        This is an example of a test with a test id
        """
        pass

