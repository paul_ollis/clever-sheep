.. _faq:

FAQ
###

You may want to read the :ref:`troubleshooting` page if you are having specific
issues with CleverSheep. You may also want to take a look at the :ref:`code_snippets`
page if you looking for how to do something specific.

.. contents::
    :local:
    :depth: 1

What is it?
===========
CleverSheep is a python package focused on high-level automated testing.
It is event driven in nature, namely events are made based on observable
behavior of the system under test (SUT) which can then be expected to
occur asynchronously in testing.

When should I use it?
=====================
The more complicated your system the better of a choice CleverSheep is. If you
have a single process or only a couple written in the same language CleverSheep
may be overkill for your needs.

If however you have a large timing dependant system with multiple processes and
boxes CleverSheep is an ideal choice with its asynchronous behaviour and support
for mocking socket connections.

What OS variants are supported?
===============================

It should work on any Unix based system. The tests are run on Alpine, CentOS,
Ubuntu, Fedora and Debian.

Can I use the twisted or tornado event loop managers?
=====================================================

Yes see the :ref:`code_snippets` page for more details