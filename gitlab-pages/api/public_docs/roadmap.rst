.. _road_map:

Road Map
########

The aim for this project is to fix the known bugs worth fixing and add any small
user requested features, at this point an LTS release will be produced and
going forward only bug fixes made. The focus would then shift to a python3
version of the library which will be a re-write.

This decision is driven by the fact that python2 will lose support in the next
couple of years and it simply isn't worth investing the time to fully fix this
library given that. A python3 version selectively porting and rewriting the code
a piece at a time as required will have a much better chance at being maintainable
long term.
