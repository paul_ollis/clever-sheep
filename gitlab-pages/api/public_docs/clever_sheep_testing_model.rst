.. _clever_sheep_testing_model:

CleverSheep Testing Model
#########################

This section covers conceptually how CleverSheep is generally used when testing
above unit level.

.. contents::
    :local:
    :depth: 2

.. highlight:: python


What is testing?
================

Whilst it can be reasonably argued that testing is difficult, especially when
multiple programs are involved, it is also conceptually quite simple. Any test
bench consists of two things:

* The system under test (:term:`SUT`).
* The thing that tests the :term:`SUT`.

And every test essentially follows the same pattern, which can be (trivially)
represented by the following example:

.. code-block:: none

    Stimulate the SUT with input A
    Expect behaviour B.
    Expect behaviour C.
    Stimulate the SUT with input D
    Expect behaviour E.
    ... and so on

Stimulating the :term:`SUT` is simply a matter of sending it messages, calling functions,
etc. Examining the behaviour is a case of seeing what the :term:`SUT` does after the input.
The difficulty (or complexity) arises from a number of factors, including:

* Some or all of the following can be non-trivial to describe or implement:
    * The :term:`SUT`.
    * Input A, Input B, etc.
    * ‘Stimulate’.
* Secondly:
    * It might be perfectly acceptable for behaviour C to happen before behaviour B.
* Thirdly:
    * Managing to do all of this automatically is not trivial.

Furthermore:

* Any complex :term:`SUT` may need to interact with a number of other systems and will
  only behave as predicted provided those other subsystems do the right thing for
  the test scenario. In practice this means the harness must typically play the
  part of those other systems, in order to drive the system through all
  possible execution paths.
* The interaction with the other systems can be (essentially) asynchronous.
* Things do not necessarily happen in the same order every time. Sometimes
  behaviour C may (validly) occur before behaviour B. Both sequences “B then C”
  and “C then B” must be interpreted as correct behaviour.

There are some other common, non-trivial to handle features, such as when the
output information is written to log files rather than sent over a socket for example.

The CleverSheep model
---------------------

Given the above CleverSheep is designed to support something more like:

.. image:: images/ComplexSutDiagram.png

TestBase / Tests
````````````````

This is what controls the whole piece, triggering stimuli, checking behaviour
and defining the expectations.

SUT Manager
```````````

Controls the SUT dealing with such tasks as shutdown/setup.

Config Manager
``````````````

Handles changing/checking system configuration.

Input Manager
`````````````

Handles stimulating the SUT, there could be many of these each of a different
type depending on the system. They would be responsible for tasks such as:

* Sending data over a TCP link
* Making rest requests
* Triggering clicks on a UI

They may be real processes/tools your test framework starts or mocked processed,
see :ref:`mock_support` for more details about mocking with CleverSheep.

Output Manager
``````````````

Again there would likely be multiple of these each of a different type. So examples could be:

* Watching log files
* Receiving traffic over a TCP link
* Receiving and responding to REST requests

Again mocks can be useful for this and in each case you would probably want to
add an event to the :attr:`Test.TestEventStore.eventStore`. See the
:ref:`test_event_store` page for more details about this.