.. _glossary:

====================
CleverSheep Glossary
====================

.. glossary::
   :sorted:

   SUT
     System under test.

   Test Package
     A directory containing one or more Test Modules.

   Test Module
     A python file containing one or more Test Suites.

   Test Suite
     A class that contains one or more Tests and inherits from the :class:`~Test.Tester.Suite` class.

   Test
     An individual test method decorated with ``@test``.