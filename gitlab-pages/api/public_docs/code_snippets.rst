.. _code_snippets:

Code Snippets
#############

This page assumes that you have read the :ref:`basics` page and the other more
specific documentation. It provides stand alone code snippets detailing how to
do certain things using CleverSheep.

.. contents::
    :local:
    :depth: 1

.. highlight:: python

Log the event store contents
============================

.. code-block:: python

    from CleverSheep.Test import TestEventStore
    from CleverSheep.Test.Tester import log

    def logEventStore(self):
        """Log the contents of the event store"""
        log.debug("***** Contents of event store *****")
        for i, (t, ev) in enumerate(TestEventStore.eventStore._store):
        if i == 0:
            baseTime = t
        deltaT = t - baseTime
        if not isinstance(ev, TestEventStore.TestEvent):
            print " %-3d: %8.3f => %s (Unable to interpret)" % (i, deltaT, ev)
            continue
        print " %-3d: %8.3f" % (i, deltaT)
        for name in sorted(ev.info):
            print "%s[%r]" % (name, ev.info[name]),
        print
        log.debug("***********************************")


Output the test results as xml
==============================

Below shows an example cruise.py file, this file should be put in a common folder for test files, for example a folder called "test_support" that is at the top of the test tree. This file should then be imported into the base suite for your tests, if you have more than one top level base file import it into each of them, no direct calls onto this class are required. Once your tests are finished there will be a file called "test_results_report.xml" which can then be imported into CI systems or similar.

.. code-block:: python

    """
        Example cruise.py file, gives support for generating Cruise Control compatible test reports.
    """

    import os

    try:
        import xml.etree.cElementTree as ElementTree
    except:
        from elementtree import ElementTree

    from xml.dom.minidom import parseString

    from CleverSheep.Test import Tester

    def prettyPrint(element):
        txt = ElementTree.tostring(element)
        return parseString(txt).toprettyxml()

    class CruiseControlReporter(Tester.Reporter):

        def __init__(self):
            self.root = ElementTree.Element("cunit_testsuites")
            self.tree = ElementTree.ElementTree(self.root)
            self.stack = [self.root]

        def enter_suite(self, has_tests, title, doc_lines):
            if has_tests:
                suite = ElementTree.SubElement(self.stack[-1], "testsuite",
                    name=title)
                self.stack.append(suite)

        def leave_suite(self, has_tests, state):
            if has_tests:
                self.stack.pop()

        def start_test(self, number, title, doc_lines):
            pass

        def end_test(self, number, title, doc_lines, state, error):
            result_str = str(state)

            case = ElementTree.SubElement(self.stack[-1], "testcase",
                name="%d: %s" % (number, title),
                result=result_str)

            if error:
                el = ElementTree.SubElement(case, "error")
                el.text = "\n"
                for line in error.splitlines():
                    el.text += '%s\n' % line

        def finish(self):
            f = open(os.path.join(Tester.execDir, "test_results_report.xml"), "w")
            f.write(prettyPrint(self.root))

    Tester.set_reporter(CruiseControlReporter())


Run a post test check
=====================

Note: like setUp and tearDown this is an inbuilt method of Suite, it is not in all versions of Clever Sheep and should only be assumed to exist in 0.5.10 and up

.. code-block:: python

    from CleverSheep.Test import TestEventStore

    def postCheck(self):
        """Check the event store is empty at the end of the test,
        this is good practice to make sure only what you expect to be
        happening is occurring
        """
        def prefix(p, s):
        s2 = ""
        for line in s.splitlines():
            s2 += "{}{}\n".format(p, line)
        return s2

        def msg():
        l = "EventStore not empty at end of test:\n"
        for _, event in TestEventStore.eventStore._store:
                l += prefix("  ", str(event))
        log.debug(l)
        c = len(TestEventStore.eventStore)
        return "EventStore not empty at end of test: {} events".format(c)

        # Any events that always want to be ignored could be dropped here
        ignoredEventTypes = ["TypeA", "TypeB"]

        for evType in ignoredEventTypes:
            self.dropEvents(type=evType)

        failUnlessEqual(0, len(TestEventStore.eventStore), makeMessage=msg)

Add a repeating timeout
=======================

As CleverSheep is single threaded if you want to perform an action repeatedly such as reading off a queue the easiest way to do this is to add a repeating timeout.

.. code-block:: python

    def startTimer():
        """Add a repeating timer that..."""
        # In this example the callback method will be called every 0.1 seconds
        # Note this example assumes this function is part of a class that inherits from Suite
        self.timerId = self.control.poll.addRepeatingTimeout(0.1, self.callback_method)

    def stopTimer():
        """Remove the repeating timer that..."""
        if self.timerId:
            self.control.poll.removeTimeout(self.timerId)

Add custom command line options
===============================

You can add your own command line options to allow a number of use cases,
see the :ref:`command_line_options` page for instructions on how to do this.

Use the twisted or tornado event loop managers
==============================================

The PollManager class can be given on `impl` param on creation which can be
'twisted' or 'tornado', this will cause a different event loop manager to be used.

Track log file changes
======================

CleverSheep gives you support for tracking log files and creating events from them so that you can expect certain values in the log files as part of your tests as they run. There is no need to wait for the test to finish and extra logging in the file added later will not break the test.

While there are certain situations where relying on a log file is unavoidable it is in general not good practice as it will make your tests brittle, changing the text of a log message shouldn't break your automated test suite so use this with caution.

Creating a tracker
------------------

To create a tracker, you need to crate a class that inherits from one of the classes in the :class:`Test.LogTracker` module. For example:

.. code-block:: python

    from CleverSheep.Test import LogTracker
    from CleverSheep.Test.TestEventStore import eventStore, TestEvent

    class SUTLogTracker(LogTracker.LineLogTracker):
        def onRead(self, code):
            for line in self.lines:
                if line.startswith("Info: "):
                    ev = TestEvent(msg=line)
                    eventStore.addEvent(ev)
                    print(line)
            self.lines[:] = []

This example uses the :class:`Test.LogTracker.LineLogTracker`, which ensures that you can always
process whole lines. The onRead method is the only thing you need to over ride.
It is automatically called when new lines become available. In this example, the
tracker looks for informational messages and stores them in the eventStore.

Note that the self.lines list is emptied on line 11. The :class:`Test.LogTracker.LineLogTracker` only
appends to this list. This is to allow, for example, you to treat multiple lines
as a single message/event without needing to implement your own buffering.

Using the tracker
-----------------

Once you have the tracker, you create it with a PollManager instance and the name of a file to track. This is often done in your set-up method:

.. code-block:: python

    def setUp(self):
        self.tracker = SUTLogTracker(self.control.poll, "sut.log")
        self.client = subprocess.Popen(["./sut.py"])

You will probably want to write a helper function to expect the log messages:

.. code-block:: python

    def wait_for_log_message(self, maxWait, msg):
        def check():
            return eventStore.find(expect, chop=True)

        def err():
            return ("SUT did not log expected message within %.2f seconds"
                    "\n  Expected: %r" % (maxWait, msg))

        expect = TestEvent(msg=msg)
        self.control.expect(maxWait, check, err)

It is the call to self.control.expect that allows the tracker to actively watch the log file. Then you can write your test, which expects to see certain messages appear in the log.

.. code-block:: python

    @test
    def request_failure(self):
        """Verify a request failure is correctly handled."""
        self.wait_for_log_message(1.0, "Info: Initiated connection")
        self.wait_for_log_message(1.0, "Info: Connection established")
        self.wait_for_log_message(1.0, "Info: Sending request")
        self.wait_for_log_message(1.0, "Info: Request rejected")

Standard Imports
================

Assuming you have defined a project root file as described on the
:ref:`basics` page you can add a file called `_fixup_.py` in the same folder
as the project root file, CleverSheep will look for and load this file.
This can be useful for common setup tasks such as adding a folders to the python path.