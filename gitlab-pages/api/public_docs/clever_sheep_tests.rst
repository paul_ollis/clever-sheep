.. _clever_sheep_tests:

CleverSheep Tests
#################

In the :ref:`basics` we covered an example test file which itself could be run
and would give access to the CleverSheep test runner and the
:ref:`command_line_options` that go with it.

That example was a simplistic one only covering a single test in a single file.
In reality it is more likely to have multiple test files in multiple folders
containing multiple tests. CleverSheep is designed to support this and the
following explains how.

.. contents::
    :local:
    :depth: 2

.. highlight:: python

The Building Blocks
===================

The are multiple building blocks that can be used to structure your test tree
these are:

* Test Package : A directory containing one or more Test Modules
* Test Module  : A python file containing one or more Test Suites
* Test Suite   : A class that contains one or more Tests
* Test         : An individual test method decorated with `@test`

Test Package
------------

While each test file (Module) can be run individually as described in the
:ref:`basics` you will also want to be able to run all the tests in a given
folder (Package) and its subfolders. Each folder in your test tree that contains
CleverSheep tests should contain an ``all_tests.py`` file that looks like the following:

::

    #!/usr/bin/env python
    """Run all of the REST API tests"""

    from CleverSheep.Test import Tester

    if __name__ == "__main__":
        Tester.runTree()

When you run this the framework will find all the test modules in a directory
tree, starting in the current directory, using the following basic rules:

* Any python modules in the directory with a name of the form test_<some- name>.py
  will be loaded and all test suites added to the test tree.
* For each sub-directory, if there is a file called all_tests.py then import it.
  Then recursively perform step 1, (above).

The docstring at the top of the 'all_tests.py' file is important. It is used as
the Package of the collection of tests within the directory and below.

You can use the full set of :ref:`command_line_options`
provided by CleverSheep and any you've added when you invoke this file.

The reason for loading any 'all_tests.py' modules in step 2 is because that
script can modify the test discovery behaviour, for example by excluding some
sub-directories from the search.

Test Module
-----------

This is simply the `test_<something>.py` file and the doc string from this file
is used. It should contain one or more Test Suites. It should contain a shebang
line and the :func:`runModule()` as described in the :ref:`basics` section.

Test Suite
----------

A class that directly or indirectly inherits from the CleverSheep Suite class
which contains one or more tests, again the doc string from the class will be used:

::

    from CleverSheep.Test.Tester import Suite

    class RestApiTestSuite(Suite):
        """A suite of tests for the rest api"""
        pass

Test
----

A test within a Suite which should be decorated with '@test', again the doc
string is used:

::

    @test
    def example_rest_test(self):
        """This is an example rest test"""
        pass

An Example
----------

As you can see in the above the docstrings are important (and required) within
CleverSheep. As an example consider the following folder layout for a test tree:

.. code-block:: none

    Process/
    |-- all_tests.py
    |-- ProcessA
    |   |-- all_tests.py
    |   |-- test_rest_req_a.py
    |   `-- test_rest_req_b.py
    |-- ProcessB
    |   |-- all_tests.py
    |   |-- test_addition.py
    |   `-- test_subtraction.py
    `-- ProcessC
        |-- all_tests.py
        |-- test_tcp_connection.py
        `-- test_udp_connection.py

Depending on the doc strings your test output would for this example then look something like:

.. code-block:: none

    Run all of the Process Tests
      Run all of the Process tests for Process A
        Process A tests for rest request A
              Rest request A tests
            1.    : Test a rest request A with value X gives a response of resp1 .......................................... PASS
            2.    : Test a rest request A with value Y gives a response of resp2 .......................................... PASS
        Process A tests for rest request B
              Rest request B tests
            1.    : Test a rest request B with value X gives a response of resp3 .......................................... PASS
            2.    : Test a rest request B with value Y gives a response of resp4 .......................................... PASS
      Run all of the Process tests for Process B
        ...
      Run all of the Process tests for Process C
        ...

+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| Text                                                                                                                 | Source                                         |
+======================================================================================================================+================================================+
| Run all of the Process Tests                                                                                         | From the all_tests.py in the Process folder    |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| Run all of the Process tests for Process A                                                                           | From the all_tests.py in the ProcessA folder   |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| Process A tests for rest request A                                                                                   | From the file doc string of test_rest_req_a.py |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| Rest request A tests                                                                                                 | From the Suite class doc string                |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| 1.    : Test a rest request A with value X gives a response of resp1 .......................................... PASS | From the test doc string                       |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| 2.    : Test a rest request A with value Y gives a response of resp2 .......................................... PASS | From the test doc string                       |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| Process A tests for rest request B                                                                                   | From the file doc string of test_rest_req_b.py |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| Rest request B tests                                                                                                 | From the Suite class doc string                |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| 1.    : Test a rest request B with value X gives a response of resp3 .......................................... PASS | From the test doc string                       |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+
| 2.    : Test a rest request B with value Y gives a response of resp4 .......................................... PASS | From the test doc string                       |
+----------------------------------------------------------------------------------------------------------------------+------------------------------------------------+

Under The Hood
==============

There are 3 phases that occur when you use CleverSheep to run tests, these
are outlined below. The examples assume you are running the tests from an
``all_tests.py`` file rather than running a single test file.

Phase 1 walk the tree
---------------------

The first thing CleverSheep does when you try to run tests is as descried above
look in the current directory and sub directories to identify each :term:`Test Suite`
and the tests within them.

Phase 2 select the tests
------------------------

The second phase is to select which tests should be run, filtering out any
broken tests or those not matching a user criteria, see the :ref:`command_line_options_user_defined_options`
section for more details on this.

Phase 3 run the tests
---------------------

The final phase is to run the actual tests, this is obviously a bit more complex
that what it seems. The actual sequence of events is as follows:

For each Suite:
    * Call the :meth:`~Test.Tester.Suites.Suite.suiteSetUp` method.
    * For each test in the Suite:
        * Call the :meth:`~Test.Tester.Suites.Suite.setUp` method.
        * Call the :meth:`~Test.Tester.Suites.Suite.postSetUp` method. In terms
          of behaviour this is considered part of the
          :meth:`~Test.Tester.Suites.Suite.setUp`.
        * Call the actual test method.
        * Call the :meth:`postCheck` method.
        * Call the :meth:`~Test.Tester.Suites.Suite.tearDown` method.
        * Output the test result to the test log and to screen.
    * Call the :meth:`~Test.Tester.Suites.Suite.suiteTearDown` method.

Output a summary of the test run.

.. _clever_sheep_tests_failure_behaviour:

Failure Behaviour
=================

If any of the steps described above fail either with a CleverSheep `fail` or
due to something like an uncaught exception the failure will be output to the
console and the log then execution will stop unless the `--keep-going` flag
is specified. In all failure cases the exit code will be non zero.

For each step if it fails and the behaviour will be as follows:

* `suiteSetUp` - Skip straight to `suiteTearDown`
* `setUp` - Skip straight to `tearDown`
* The test - Skip straight to `tearDown`
* `postCheck` - Skip straight to `tearDown`
* `tearDown` - Skip straight to `suiteTearDown`
* `suiteTearDown` - Exit `suiteTearDown`

Execution stops after the `suiteTearDown` of a suite with a failure and before
the `suiteSetUp` of the next suite.

Keep in mind exceptions in teardown functions will cause them to exit
immediately, as such you may want to put anything that can fail during teardown
in try except statements and then either perform a fail or re-raise the
exception at the end of the function once all other teardown has complete.

Failure Summary
---------------

At the end of a test run you will see a summary of the results, if a test
failed you will also see a summary of the failures. If multiple failures
occur during a test the first failure reached is the one reported as the overall
suite result. For example if the `suiteSetUp` fails and then the `suiteTearDown`
fails the suite will report with 'BAD_SUITE_SETUP', if just the `suiteTearDown`
were to fail it would report with 'BAD_SUITE_TEARDOWN'. This is because the
first failure may be what is causing any following failures.

Exit Suite
----------

There is one case where a non zero exit code can be returned but execution will
continue without having to specify the `--keep-going` flag. This is if
`exit_suite` is called. This will cause the current suite to exit but execution
to continue.

.. warning:: The `exit_suite` function will be removed in version 0.6.4 and
             should not be used. Performing a fail and specifying the
             `--keep-going` flag will emulate the same behaviour in most cases.