.. _release_notes:

Release Notes
#############


Note: This page only contains release notes for v0.6.0 and onward.

.. contents::
    :local:
    :depth: 2

v0.6.X
======

Below are the changes made for each 0.6.X release.

v0.6.5
------

Changes
```````
* A new command line option has been added, '--disable-run-and-trace', this can
  be used to disable the special '#>' comments which appear on the command line
  and in test log files. Users may wish to do this as it can improve run time
  performance of the library.
* The `PipeConn` class `_binary` flag has been renamed to `addMsgLengthOnSend`
  and can now be set via the '__init__' or indirectly via the `openInputPipe` of
  the `Component` class.
* The packing method in the mocks has been changed from '@' to '=' when using native.
  This means the integer size will be consistent which the code requires.
* The `Connecter` class in `Test.Mock.Comms` will now allow up to 5 errors
  before giving up on a connection. 'EINPROGRESS' errors are not counted. The
  previous behaviour was to abandon any connection attempt on the first non
  'EINPROGRESS' error.

Deprecated Features
```````````````````

* The deprecated function "exit_suite" has been remove along with the ExitSuite
  exception.
* gcov has been removed from CleverSheep.

Bug Fixes
`````````
* The unhandled exception when trying to import call from mock has been resolved.
* An exception in the PipeConn mock on initialization has been fixed.

Known Issues
````````````

* The '--timeout' command line option does not work as intended and should not be used.
* Using versions 0.7.X of twisted and higher seems to cause an import error and should not be used.


v0.6.4
------

If non string objects are passed in as the error message for `fail` calls a more
informative error message will be raised.

Known Issues
````````````

- The '--timeout' command line option does not work as intended and should not be used.
- Using versions 0.7.X of twisted and higher seems to cause an import error and should not be used.
- `from mock import call` will cause an exception, this seems to be due to `call` being a tuple.
  Currently there is no fix for the issue, to work around it import mock then
  use `mock.call`.

v0.6.3
------

This version contains very little, it was required to work around a pypi issue.

New Features
````````````
Two new logging calls are now valid when using the CleverSheep logger, these are
'warning' and 'fatal'. This brings it more in line with the Python logging
library which is what is used under the hood.

Known Issues
````````````

* The '--timeout' command line option does not work as intended and should not be used.
* Using versions 0.7.X of twisted and higher seems to cause an import error and should not be used.

v0.6.2
------

New Features
````````````

* When string filters are used they will be output on the command line and
  in the test log, the reason for this is if a user mistypes a command line
  option it will be treated as a string filter.

Behaviour Changes
`````````````````

Issues in the framework functions such as `suiteTearDown` will, going forward,
be treated the same as if the actual test in terms of how it is handled
although the reporting should highlight exactly what has failed.

* Failures in `suiteSetUp`, `suiteTearDown` and `tearDown` will now stop
  execution unless the `--keep-going` flag has been specified. This will bring
  them in line with the other test functions and make the behaviour consistent.
* Failure summaries will always come out in the terminal output.

Bug Fixes
`````````

* `suiteTearDown` failures were not being reported. They will now be reported
  on the command line and in the test log.
* `suiteTearDown` failures were not causing non zero exit status. They will now.
* `tearDown` failures were not causing non zero exit status. They will now.
* Fixed a bug causing the `--max-exec-time` command line option to not work.

Regression Fixes
````````````````

* Fixed a regression where the help text was not displaying the user defined
  options. This regression was introduced in 0.6.1.

Deprecated Features
```````````````````

* The :mod:`CleverSheep.Debug` module has been removed as it is deprecated, if you
  want to use `ultraTB.py` use the version in :mod:`CleverSheep.Extras`.

* The :mod:`CleverSheep.VisTools.Process` module has been removed, please use
  :mod:`CleverSheep.Prog.Process` instead.

* The :mod:`CleverSheep.decorator` module has been removed, please use
  :mod:`CleverSheep.Extras.decorator` instead.

* The :mod:`CleverSheep.Prog.Ustr` module has been removed, please use
  :mod:`CleverSheep.Prog.Intern` instead.

* The :mod:`CleverSheep.Test.Tester.Discovery` module has been removed.

* The :func:`CleverSheep.Test.Tester.set_reporter` alias has been removed, use
  :func:`CleverSheep.Test.Tester.registerReporter` instead.

* The :func:`CleverSheep.Prog.Files.findDir` alias has been removed, use
  :func:`CleverSheep.Prog.Files.findDirAbove` instead.

* The :mod:`CleverSheep.IOUtils` module has been removed. The only class in
  this module was a fake file that simply dropped the output, it would be trivial
  for a user to implement should they want this behavior so it has been removed
  from the library.

* The :mod:`CleverSheep.Rst` module has been removed.

Misc
````

* Update the error messages on failure of setup, teardown etc.
* The `PropertyError` exception has been removed from 'CleverSheep/Prog/Aspects.py'
  'CleverSheep/Test/Tester/Collection.py'. It shouldn't have been duplicated and
  does not serve a purpose any longer so has been removed.
* The exit_suite function has been deprecated and will be removed in version
  0.6.4
* The ExitSuite exception has been deprecated and will be removed in version
  0.6.4
* In the short term at least public access to the test states has been
  removed, they are not well defined. As part of this a number of unused states
  have been removed.
* Test state reporting has been simplified, there is now only the state, the
  concepts of reason codes and the `Result` class have been removed.

Known Issues
````````````

* The '--timeout' command line option does not work as intended and should not be used.
* Using versions 0.7.X of twisted and higher seems to cause an import error and should not be used.

v0.6.1
------

optparse to argparse migration
``````````````````````````````

The major change in this release is that optparse has been replaced with argparse.
This effects a number of areas outlined below.

* The `-z` option has been removed, user options whose names clash with built in option names are no longer supported.
* You will need to update your :meth:`add_option` calls to instead be :meth:`add_argument`. Any type variables also need to actual types rather that strings, for example `type=int` not `type='int'`. For more details on this see https://docs.python.org/2/library/argparse.html#upgrading-optparse-code

Other changes
`````````````

* Fixed an issue where the '--details' command would crash with an unhandled exception if any of the tests are not within a Suite.
* An alternative format of `# > Message` is now supported for pep8 compliance.

Known Issues
````````````

* The '--timeout' command line option does not work as intended and should not be used.
* Using versions 0.7.X of twisted and higher seems to cause an import error and should not be used.

v0.6.0
------

Command line option changes
```````````````````````````

* The command line option '--dump-tag' has been added.
* The command line option '--run-timeout' has been added.
* '--run-disabled' : This option has no effect within CleverSheep so it has been removed.
* '--list-tested' :  This option has no effect within CleverSheep so it has been removed.
* '--show-status' :  This option has no effect within CleverSheep so it has been removed.
* '--pure' :         This undocumented option has no effect within CleverSheep so it has been removed.
* '--ids-summary' : Moved to be undocumented, users should use '--summary' or '--details'.
* '--c-cover' :  This option is considered redundant within CleverSheep so it has been removed,
  there are specific external tools to generate coverage for C/C++ programs.
* '--c-all' :    This option has no effect within CleverSheep so it has been removed.
* '--c-erase' :  This option has no effect within CleverSheep so it has been removed.
* '--c2-cover' : This undocumented option is considered redundant within CleverSheep so it has been removed,
  there are specific external tools to generate coverage for C/C++ programs.
* '--c3-cover' : This undocumented option is considered redundant within CleverSheep so it has been removed,
  there are specific external tools to generate coverage for C/C++ programs.

Deprecated modules
``````````````````

* The Gcov module has been marked as deprecated and will be removed in a future release.

Bug Fixes
`````````

A variety of bug fixes have been made including:

* A recursion issue when collecting test spec info
* Socket reset handling.
* SSL and I/O issues.

Known Issues
````````````

* The '--timeout' command line option does not work as intended and should not be used.
* Using versions 0.7.X of twisted and higher seems to cause an import error and should not be used.
