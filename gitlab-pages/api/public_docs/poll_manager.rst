.. _poll_manager:

Poll Manager
############

The CleverSheep framework contains a relatively general purpose component called
the :mod:`~Test.PollManager`.

This component provides an event driven (reactor) framework for programs that
need to interact with other asynchronous entities; typically communicating via
sockets or pipes. It basically uses the standard ``select.poll`` interface to
provide a higher level event driven framework.

.. contents::
    :local:
    :depth: 1

.. highlight:: python

Basic Use
=========

The simplest way to use the PollManager is for timeouts, as shown in the
following trivial program:

::

    from CleverSheep.Test import PollManager

    def tick():
        print("tick")

    def finish():
        manager.quit()

    manager = PollManager.PollManager()
    manager.addRepeatingTimeout(0.3, tick)
    manager.addTimeout(1.0, finish)

    # Invoke the manager's run method to start event driven execution.
    manager.run()
    print("The end")

It should be reasonably clear what this will do. The ``tick`` callback function is
called repeatedly every 0.3 seconds. The ``finish`` callback function is called
after 1.0 second and it tells the ``manager`` to quit; i.e. return from the call
to ``run()``. So we expect to see the work ‘tick’ 3 times.

.. code-block:: none

    $ ./timeouts.py
    tick
    tick
    tick
    The end

You can also also ask the PollManager to monitor open files for activity and
invoke callback functions in response. The following, program will accept a TCP
connection and then echo anything anything it receives.

::

    import socket

    from CleverSheep.Test.PollManager import PollManager

    def onConnectAttempt(fd, event, s):
        global connection
        connection, addr = s.accept()
        p.addInputCallback(connection, onReceive, connection)

    def onReceive(fd, event, connection):
        data = connection.recv(1024)
        print("Recv %r`" % data)
        connection.send("%s back" % data)

    p = PollManager()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("localhost", 7899))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.listen(1)

    p.addInputCallback(s, onConnectAttempt, s)
    p.run()

You can have many sockets and timeout active at any time. The PollManager does
all the housekeeping and invokes callbacks as appropriate.

General rules when using the PollManager
========================================

There is a large volume of information on event driven programming already written.
What follows is simply tailored to the this framework.

Event driven programming can be simpler than using threads because there is no
need to implement any resource locking and so no risk of deadlock, livelock,
etc. However, there are some rules you need to follow in order to keep event
driven programs responsive.

Callback functions (ones passed to :func:`addInputCallback`, :func:`addTimeout`, etc.)
should execute quickly. If a callback takes, say, several seconds then during
those seconds then the program will be unresponsive to any other inputs
(appearing to temporarily hang). In general callbacks should take a fraction
of a second at most.

Callbacks that handle file input, should normally try to process all pending
input, but (of course) without unnecessarily violating the previous rule.
Often this is as simple as reading from a socket until there is no more data
left to read. However, if the input data rate can occasionally be heavy then
you might perhaps do (say) 100 reads at a time, thus allowing other events
and I/O to get serviced. A more general approach is to track execution time
and stop processing the input after (say) 100 ms.

Use :func:`addOutputCallback`/``POLLOUT`` with care. This means when your socket or
pipe’s buffers are full. If you, for example, use :func:`addOutputCallback` on a
socket that can accept data for writing then your callback function will get
called repeatedly, effectively making you program busy-wait on the socket.
Generally, you should only set callbacks for output when you get an ``EAGAIN``
error. And the callback should remove the ``POLLOUT`` callback - unless it also
gets ``EAGAIN``.

Set sockets and pipes to non-blocking mode. Otherwise a code may block on a
socket operation then no other processing can occur until the block clears.

More about sockets
==================

As mentioned earlier, sockets should be non-blocking as should open pipes.
To make a socket non-blocking use:

::

    my_socket.setblocking(0)

Note: it is neither necessary nor desirable to do this for a listening socket.

Non-blocking I/O needs some careful handling (a small price for avoiding
threads and locking mechanisms). For reading a socket you should have logic
something like this in your input callback.:

::

    # WARNING: Unproven code.

    def handle_intput(fd, event, sock):
        while True:
            try:
                data = sock.recv(1024)
            except IOError, exc:
                if exc.errno == errno.EAGAIN:
                    # No more data to read, so stop trying
                    return
                # Something bad has happened - handle it!

            if len(data) == 0:
                # The socket has closed.
                manager.removeInputCallback(fd)
                return

            # Something was read, process it.

For socket output you should have (somewhat more complex) logic something like:

::

    # WARNING: Unproven code.

    def write(self, sock, data):
        output_buf.append(data)
        if self.out_fid:
            return
        self.send_all()

    def send_all(self):
        while output_buf:
            try:
                sock.send(output_buf[0])
                output_buf.pop(0)
            except IOError, exc:
                if exc.errno == errno.EAGAIN:
                    # Output buffer is full.
                    self.out_fid = self.manager.addOutputCallback(
                                                      sock, onOutputReady)
                    return

                # Something bad has happened - handle it!

        if self.out_fid:
            self.manager.removeOutputCallback(self.out_fid)
            self.out_fid = None

      def onOutputReady(self, sock, data):
          self.manager.removeOutputCallback(self.out_fid)
          self.out_fid = None
          self.writesock, data)

The Component module provides a much higher level interface, which handles most
of these complexities. Using mocks can also assist with these issues.

Other Frameworks
================

The built-in PollManager implementation is fairly functional, but there are a
number of alternative frameworks, which provide very similar features. The
PollManager can be made to use some of these alternatives rather than its
built-in implementation. Currently the supported frameworks are:

* Tornado
* Twisted

A common reason for doing this would be when using CleverSheep to test code
that is designed to use one of these alternatives.