.. _test_event_store:

Test Event Store
################

The CleverSheep :class:`~Test.TestEventStore.EventStore` provides support for tracking and querying the events
that occur whilst exercising the :term:`SUT`. In many ways it is little more than a list
of Python objects, but it provides a very useful abstraction when writing
high-level tests.

Testing essentially involves stimulating the :term:`SUT`, observing the results and
checking they are what you expected to happen. When testing with CleverSheep
your mentality should be to expect something to occur in a time window and poll
until it does or the time runs out, you should not do point in time checks. This has the
advantage of less brittle tests and no need for long delays, by polling you return
as soon as the event happens rather than waiting a fixed amount of time to allow
the desired behavior to occur and then checking it did.

One thing to be aware of is that CleverSheep is single threaded in nature and
does not play well with multiple threads. If you wish to put events into the
:attr:`~Test.TestEventStore.eventStore` this requires special handling, see below for more details.

The other implication of this is that if you have log file watchers reading
files or mocks reading messages off of queues as an example they must be given time to
do so. Calls to :meth:`control.delay(X)` give these other processes time to run. Time
is also given to them when you are expecting events to occur.

The result of this is that in rare situations you can fail to find an expected
event simply due to the framework not having read it off a queue and put it in
the :attr:`~Test.TestEventStore.eventStore` yet. If you have very message heavy queues or indeed and
framework that needs to be highly performant you may want to start the part of
the framework reading off the queue in a separate thread and have it add events
via a callback as described below.

.. contents::
    :local:
    :depth: 1

.. highlight:: python

Background
==========

The problem
-----------

Checking that your expectations have been met can be difficult because:

* Events of interest can be a number of things; log files being created, messages written to log files, TCP connections being made or broken, messages being received, processes starting or stopping, etc.
* Hence there might be a lot of different types of events that a test script must observe and check.
* The order in which events can be expected to occur is often not fully deterministic. For example, it may be perfectly OK for events A and B to occur in the order A then B or B then A, provide that they both occur.

Events can be generated from more than one source.

The solution
------------

The :attr:`~Test.TestEventStore.eventStore` provides a single place where a number of
mocks, log watchers, etc. can deposit events as they occur. The event driven
features allow events to be added to the store in a way that does not interfere
with the main flow of a test script. All the test script has to do is query the
store to verify that expected events have occurred.

The event store is time ordered and FIFO. Events older that a configurable age
are discarded and the length of the FIFO can also be configured.

You can put any Python object into the store. However, it is recommended to use
the TestEvent object, which is designed to store information about an event and
make querying easier.

Using the TestEventStore
========================

Importing
---------

The typical way is to import is:

::

    from CleverSheep.Test.TestEventStore import eventStore, TestEvent


The :attr:`~Test.TestEventStore.eventStore` object is a default instance of :class:`TestEventStore`, which is
typically all you need.

The :attr:`~Test.TestEventStore.eventStore` provides a single store for all events
generated or detected during a test run.

Adding Events
-------------

To add an event, simply use the :func:`addEvent()` method. For most situations using
the TestEvent (or a derived class) will be appropriate. For example:

::

    eventStore.addEvent(TestEvent(message="Hello", type="greeting", index=msgIndex))

The keyword arguments passed to the :class:`~Test.TestEventStore.TestEvent` constructor are stored in the
event’s :attr:`~Test.TestEventStore.TestEvent.info` dictionary. You can store any arbitrary combination of information
in the event. The added event is time stamped and appended to the end of the
store’s FIFO.

Adding Events from another thread
`````````````````````````````````

CleverSheep is NOT thread safe so if you wish to add events to the event store
from another thread it should be done in the following manner.

::

    from CleverSheep.Test import TestEventStore
    from CleverSheep.Test.Tester.Suites import control

    # Have a simple function that adds events to the event store
    def addEvent(event):
        """Add an event to the event store"""
        TestEventStore.eventStore.addEvent(event)

    # Add a callback that will put the event in the event store,
    # by adding the event via a callback it allows clever sheep
    # to decide when to add the event and stops multiple access
    # to the test event store (which causes strange/undesirable
    # behaviour)
    def addEventViaCallback(event):
        """Add an event to the event store via a callback"""
        control().addCallback(addEvent, event)

Finding and removing events
---------------------------

The store has a :class:`~Test.TestEventStore.EventStore.find` function, to allow you to search the store. This provides
a number of ways of checking events in the store for a match. One way is to use
a :class:`~Test.TestEventStore.TestEvent` instance with the required information to match. For example, to
find an event matching the one stored in the previous example you could do:

::

    eventStore.find(evt=TestEvent(message="Hello", type="greeting", index=msgIndex))

In this case we pass it a :class:`~Test.TestEventStore.TestEvent` instance, which has the same values as
those we wish to match. The :class:`~Test.TestEventStore.TestEvent` class has an equality operator that does
the hard work. See the :meth:`~Test.TestEventStore.EventStore.find` for more details of how to match events.

The :meth:`~Test.TestEventStore.EventStore.find` method searches sequentially, starting with the oldest event. So if
more than one matching event is in the store, the oldest one will be found.

Often, having found an event you will want to remove it and all earlier events.
There are two ways to do this. Firstly you can request old events are chopped
when you do the find.

::

    eventStore.find(evt=TestEvent(message="Hello", type="greeting", index=msgIndex), chop=True)

Alternatively, you can use the chop() method.

::

    evt = eventStore.find(evt=TestEvent(message="Hello", type="greeting", index=msgIndex))
    eventStore.chop(evt)

You can also remove just a single event, having found it:

::

    evt = eventStore.find(evt=TestEvent(message="Hello", type="greeting", index=msgIndex))
    eventStore.remove(evt)

:attr:`chop` can cause issues in tests if used when not appropriate. You should only
use it if you are sure you won't want any older events than the one you've just
found. If you want to expect two events A and then B you should only use :attr:`chop`
with A if you are sure B cannot occur before it.

:meth:`find` is normally used in combination with :meth:`control.expect` see the section
Using the store with control.expect below for more details.

Managing the store
------------------

There are a number of things you can do to manage the the event store.

* You can empty the store by simply invoking the
  :meth:`~Test.TestEventStore.EventStore.clear` method.
* You can change the maximum length (how many events is holds before old events
  are discarded) by setting the :attr:`maxLength` attribute. If you set this to
  ``None`` then the size of the store is not constrained.
* You can change the maximum age (how old an event may become before it is
  discarded) by setting the :attr:`maxAge` attribute. If you set this to ``None``
  then the is no limit to the age of events.
* You can reset the store to its originally created state by using the
  :meth:`reset()` method. It is often a good idea to to do this in a
  :meth:`~Test.Tester.Suites.Suite.setUp` method.

As an example to change the maximum age and length of the store:

::

    from CleverSheep.Test import TestEventStore

    def __init__(self):
        """
        This would be the __init__ function of a class that inherits from
        Suite either directly or indirectly
        """
        # Set the amount of time in seconds an event will sit in the
        # event store from when it is added. Once this time has passed
        # it will be automatically removed
        TestEventStore.eventStore.maxAge = 60

        # The maximum number of events that can be in the
        # event store, once it is full the oldest event will be removed
        TestEventStore.eventStore.maxLength = 200

.. warning:: You should always try to keep the number of events in the store to a minimum. Searches of the store are performed linearly, starting with the oldest event. If you do not remove events from the store once they are no longer needed then:

    * You will slow down searching unnecessarily.
    * You risk false test passes because an old event might incorrectly match an expected event.

    To avoid this you should clear the store between tests.

Using the store with control.expect
===================================

The best way to check events is using the
:meth:`~Test.Tester.Suites.Controller.expect` method to wait for things
to happen. Nearly any test that uses the event store will want to wait for one
or more events to occur such as waiting for the SUT to be running or stopped,
for the SUT to ack a message etc.

An example system
-----------------

For this example, we will assume that the SUT writes to a log file (sut.log).
We use a log file tracker to follow the log during a test run. When the tracker
see important entries in the log, it converts them to events and puts them into
the :attr:`~Test.TestEventStore.eventStore`. See the (HowTo)[how-to] page for notes on how to do this.

There are four messages that we are interested in, which the tracker converts to
the following events:

::

    TestEvent(name="Startup complete")
    TestEvent(name="Normally first")
    TestEvent(name="Normally second")
    TestEvent(name="Shutdown complete")

Expecting in order events
-------------------------

The following test script will run the :term:`SUT` and verify that the startup and
shutdown appear in sequence.

::

    #!/usr/bin/env python
    """Test example, using expect to wait for event store events."""
    __docformat__ = "restructuredtext"

    from CleverSheep.Test.Tester import *
    from CleverSheep.Test.TestEventStore import eventStore, TestEvent

    import tracker
    import sut_control


    class EventEg(Suite):
        """Another example."""
        @test
        def wait_for_events(self):
            """Expect start up and then shutdown."""
            self.tracker = tracker.Tracker(self.control.poll, "sut.log")
            self.tracker.start()
            sut_control.start_sut()

            self.wait_for_event(1.0, name="Startup complete")
            self.wait_for_event(1.0, name="Shutdown complete")

        def wait_for_event(self, maxWait, chop=True, **kwargs):
            def check():
                found = eventStore.find(expect, chop=chop)
                if found and not chop:
                    eventStore.remove(found)
                return found

            def err():
                return ("Event matching %r did not occur within %.2f seconds"
                        % (kwargs, maxWait))

            expect = TestEvent(**kwargs)
            self.control.expect(maxWait, check, err)


    if __name__ == "__main__":
        runModule()

In this example our test 'wait_for_events' starts a log tracker and the SUT
(the underlying details are not relevant for this example). It then expects two
events to occur in order allowing up to 1.0 seconds for each event.

The ``wait_for_event`` method hands over to :meth:`~Test.Tester.Suites.Controller.expect`.
This allows other activity to occur (such as the tracker to poll the log file
and add events to the :attr:`~Test.TestEventStore.eventStore`). The nested
function check detects the arrival of the event using the :meth:`~Test.TestEventStore.EventStore.find` method.
The chop argument to ``wait_for_event`` defaults to True,
this is what makes this an ordered check. If the 'Shutdown Complete' message
comes out before the 'Startup' it will be thrown away due to the :attr:`chop` option
so the test will fail.

Expecting out of order events
-----------------------------

To expect out of order events simply do the same as above but with :attr:`chop` set to
False. If you do not care about the order of events it is recommended you set
:attr:`chop` to false by default.

More complex event expecting
----------------------------

::

    from CleverSheep.Test import TestEventStore

    def expectEvent(self, maxWait, onFail=None, **kwargs):
        """ Utility to allow asserts on an expected event.

                maxWait: Maximum time to wait for the event to come in
                onFail : Function to call if the event is not found within maxWait
                kwargs : Used by the match function to determine if an event matches

                Example call: event = self.expectEvent(maxWait=wait,
                                          type='SystemEvent',
                                          category=category,
                                          status=status,
                                          severity=severity)

                This would find an event with a type of 'SystemEvent' that has  a matching category, status and severity.
        """
        if onFail is None:
            # If we don't find the expected event log everything in the event store
            onFail = self.logEventStore

        def match(event):
            """
                    Detailed event matching by field. This function can be as complex or simple as you require
            """
            for attr, value in kwargs.iteritems():
                if not hasattr(event, attr):
                    return

                if getattr(event, attr) != value:
                    return
            return event

        def check():
            """
                    Wrapper around match for a single test event.
            """
            return TestEventStore.eventStore.find(predicate=match)

        def err():
            """
                    Report problems when events do not match.
            """
            if onFail is not None:
                onFail()
            return "Did not see expected event %s" % kwargs

            # Note control is a member variable of Suite, it is assumed the class
            # this method belongs to inherits from Suite directly or indirectly
        ev = self.control.expect(maxWait, check, err)
        TestEventStore.eventStore.remove(ev)
        return ev

Drop events from the event store
================================

In your test :meth:`tearDown` you may wish to check the :attr:`~Test.TestEventStore.eventStore` is empty, before
doing so you may want to drop events of particular types that you're not
interested in.

::

    from CleverSheep.Test import TestEventStore
    from CleverSheep.Test.Tester import log

    def dropEvents(self, **kwargs):
        """Remove any matching events from the event store

        This function should be used to throw away events
        that are not of interest
        """
        def match(event, **kwargs):
            """Match function to select which events to drop"""
            for attr, value in kwargs.iteritems():
                if not hasattr(event, attr):
                    return False
                if getattr(event, attr) != value:
                    return False
                return True

            drop = []

            for (_, event) in TestEventStore.eventStore._store:
                if match(event, **kwargs):
                    drop.append(event)

            for event in drop:
                log.debug("DROP EVENT - %s", event.info)
                TestEventStore.eventStore.remove(event)

