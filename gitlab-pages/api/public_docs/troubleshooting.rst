.. _troubleshooting:

Troubleshooting
###############

This page contains tips for helping you debug issues.

.. contents::
    :local:
    :depth: 1

Common Points
=============

* Test files names must start with `test_`
* Suites and tests must have doc strings

Don't use time.sleep()
======================

Don't use `time.sleep(N)` or anything similar in the CleverSheep tests or framework
code, instead use :meth:`self.control.delay` assuming you are in a test suite.

The reason for this is `time.sleep(N)` will simply block the program,
:meth:`self.control.delay` will allow the run loop to perform other tasks.

Where's my standard out/standard err output?
============================================

All standard out and standard error output is put into the log file which is
called test.log by default and will be in the folder the tests were run from.

Accessing CleverSheep elements from suite __init__
==================================================

Don't try to access CleverSheep elements such as the user options from Suite
`__init__` functions, the library won't be fully setup yet and this can cause
problems.

My mock is unreliable
=====================

If you've created a mock but it occasionally doesn't get messages try putting
in a :meth:`self.control.delay`, it may be that the mock isn't being given enough
time to read messages and put them into the event store, this will effectively
yield some time.

I can't find my event in the event store
========================================

Is the event store full or has the event timed out of the event store?
