.. _command_line_options:

Command Line Options
####################

When using the CleverSheep test runner as described on the :ref:`basics` page
there are a number of command line options provided by default. This page
provides some examples and the full description of the options along with how
to add your own.

.. contents::
    :local:
    :depth: 1

.. highlight:: python

Behaviour
=========

The command line options are handled using :mod:`argparse`, it uses multiple parsers,
initially to consume the built in command line params and then to check for any
user defined params. Finally if there are any leftover params they are assumed
to be specifying the tests to be run, if extra command line params are passed
in that are unknown they may cause issues due to this.

The items set via command line params can be set in other manners as well,
see the :ref:`library_options` page for more details.

You may also wish to read :ref:`clever_sheep_tests_failure_behaviour` to see
the behaviour of the tests when failures occur.

.. warning:: Pre version 0.6.1 optparse was used as the command line argument parsing
             module, if you are using 0.6.0 or older the examples on this page will
             need modifying before they can be used.

Examples
========

This section provides some common examples of how to run the tests.
The examples show the tests being run through an `all_tests.py` file but the
same still applies even if singular test files are being run.

Output a summary of the tests that will be run
----------------------------------------------

.. code-block:: none

    ./all_tests.py --summary


Note: As long as it is clear which option is intended (there would only be one
match) then a shortened version of the options can be used:

.. code-block:: none

    ./all_tests.py --sum


Run all tests and keep going on failure
---------------------------------------

Runs all the tests and will continue to run even on test failure.

.. code-block:: none

    ./all_tests.py --keep-going

Specific Test Selection
-----------------------

You can select the tests to be run by specifying the test number or number to
run or the tests to run based on a string that is in the test doc string.

These selections are not exclusive, if you specify a number range and a string
tests matching **either** condition will run, not tests matching both.

Run tests based on test doc string
``````````````````````````````````

You can run test based on the content of the test doc string, you can either
check just the test summary or the summary and description. The summary is
considered to be the text in the test doc string before any new lines with the
remainder of the doc string being the description. For example:

.. code-block:: python

    @test
    def example_of_summary(self):
    """This would be part of the summary

    This is not part of the summary it is the description
    """

The next example runs any tests which have 'Performance' or 'Limit' in the
summary, by default this is a case sensitive check. You can make it case
insensitive using the ``-i`` command line option.

.. code-block:: none

    ./all_tests.py Performance Limit

To search the summary and description prefix the string with ``d:``:

.. code-block:: none

    ./all_tests.py d:Performance d:Limit

.. note:: Each word you wish to look in the description for must be prefixed
          with ``d:``, it is not enough to simply prefix the first word.

.. note:: Any provided strings to use for searching are output on the command
          line and in the test log. This is to avoid confusion if a command line
          option is mistyped and make it obvious it is being used as a search
          string.

.. In theory you should be able to use a '~' to provide a regex, this doesn't
   seem to work as expected however. Issue #75 has been raised to look at this.

Run selected tests by number
````````````````````````````

Run selected tests by their test number, as the test number can change when
new tests are added this should not be used in automated scripts.

.. code-block:: none

    ./all_tests.py 1 4 77

Run selected tests by number range
``````````````````````````````````

Run test 5 onwards to the end of the test run

.. code-block:: none

    ./all_tests.py 5-

Run tests 17-29 inclusive.

.. code-block:: none

    ./all_tests.py 17-29

Usage
=====


In the below `all_tests.py` could also be replaced with a test file as long as
run module has been specified. See the :ref:`basics` page for more details.

`all_tests.py [options] [test-nums ...]`

Standard Options
================

`-h, --help`
    Show this help message and exit

`--uids=UIDS`
    Select IDs to be run. Use multiple options and or comma separated list. This
    requires the tests to have been annotated with testIDs. See also `--list-ids`.

::

    @test(testID="this-is-the-test-id")

`--modules=MODULES`
    Select modules to be run. Use multiple options and or comma separated list.
    Partial names will match. The module in this context is the name of a file
    containing CleverSheep tests. See Also `--list-modules`.

`--summary`
    Output a summary of the loaded tests.

`-i, --ignore-case`
    Ignore case when searching test descriptions.

`--skip-passed, --resume`
    Resume execution, skipping all previously passed tests

`--run-broken`
    Run tests marked as broken in their test annotation.

::

    @test("broken")

`--keep-going`
    Keep going when a test fails.

`--patch-subprocess`
    Patch sub-process functions to provide status output

`--add-status-comment=ADD_STATUS_COMMENT`
    Define additional comments used for status line, in addition to the standard '#>' form.

`--log-file=PATH`
    Set the log file to PATH (default = test.log)

`--log-level=LEVEL`
    Set the logging level LEVEL (default = debug). Valid values: debug info warning error critical.

`--no-times`
    Do not put time stamps in the log file.

`--columns=COL`
    Try to restrict output to COL columns (min=60)

`--unsorted`
    Do not sort tests into numeric order. The test numbers must be provide on the command line.

`--random=N`
    Use N as seed for random test execution order (default=0) The default of
    zero implies no randomisation. A negative number means seed using system
    time. A positive number means seed using N.

`--max-exec-time=T`
    Try to avoid running tests that take longer than T seconds. Timing
    information is built up as the tests are run, you will need to have run the
    tests at least once before you can use this flag.

`--times`
    List average execution times for selected tests. Timing information is built
    up as the tests are run, you will need to have run the tests at least once
    before you can use this flag.

`--all-times`
    List all execution times for selected tests. Timing information is built up
    as the tests are run, you will need to have run the tests at least once
    before you can use this flag.

`--timeout=T`
    Timeout the test run after T seconds. This option has known issues and should not be used.

`--run-timeout=T`
    .. note:: Added in 0.6.0

    Timeout the test run after T seconds.

`-q, --quiet`
    Only report pass, fail, etc. Omit details of failures.

`-n, --no-action`
    Do not perform any actions, but still invoke registered reporters

Documentation Options
=====================
For generating documentation from tests.

`--details`
    Dump a detailed list of the loaded tests.

`--list-ids`
    List the IDs of all selected tests.

`--dump-tag=TAG`
    .. note:: Added in 0.6.0

    Provides the meta information held by the tag. Specifying this option with
    no value will output all the known tags. This option can be specified
    multiple times to output multiple values.

`--list-modules`
    List the modules of matching tests.

`--list-no-ids`
    List tests without test IDs

`--sphinx=DIR`
    Generate documentation in Sphinx format DIR.

`--doc-root=PATH`
    Set documentation root to PATH (default=Use full paths).

Journalling Options
===================

`--enable-journal`
    Enable the long-term journal - this will stores information about the test
    run on a per test basis. Currently there is no command line method to view
    this data.

Debugging Options
=================

Useful when debugging problems with tests.

`--full-stack`
    Print  the full stack traceback for test failures.

`--full-inner-stack`
    Print the full inner stack traceback for test failures.

`--partial-inner-stack`
    Print a partial inner stack traceback for test failures.

`--all-diffs`
    Show all differences when strings do not match.

`--num-diffs=N`
    Display N differences when strings do not match (default=1)

`--fail-on=N`
    Make each test fail on the Nth assertion.

.. _command_line_options_user_defined_options:

Adding user defined options
===========================

You can add your own command line options which can be used for a number of use
cases and allow you to customize the test runner behavior for your needs..

These are quite often combined with user meta data put in the test decorator
of each test as mentioned in the :ref:`basics_marking_tests` section.

Custom param use case examples
------------------------------

Examples of some situations where you may want to add a custom param:

* To allow CI system based setup (for example start a web browser in headless mode).
* To run a subset of tests (for example a smoke set or a set for FAT).
* To enable extra logging/output within the test harness.
* To run a specific configuration (for example run under valgrind).
* To enable specialized logging and delays (for example to facilitate demos).
* To select groups of tests (for example tests related to any of X Y Z build types/features/customers).

Adding user defined meta-data
-----------------------------

You can define your own meta data in the same way the inbuilt options are defined:

.. code-block:: python

    @test(my_meta_data="Data")
    def test_name(self):
        """Test description"""

The library places no meaning on these, it is up to the user to give them
meaning.

You can access also access this meta-data from setUp/tearDown functions and the
test itself using the :func:`Test.Tester.currentTestInfo` function.

.. warning:: Don't start you meta names with ``cs_``, these names are reserved by
             the library.

Adding a command line option
----------------------------

To add command line parameters you should create a python file to contain them
(the suggested name for which is TestControl.py). This file should be imported
into your base suite files (TestBase) so that all the suites below can use them.
There should only be one file of this type imported into a test suite.

CleverSheep uses :mod:`argparse` under the hood so for a more detailed explanation
of the examples below see the :mod:`argparse` documentation.

Added user options will appear when the `--help` option is passed to the test
runner after the standard options.

::

    #!/usr/bin/env python
    """Test control support to add flags to test runner"""

    from CleverSheep.Test import Tester

    # Add a smoke flag
    Tester.add_argument("--smoke", action="store_true",
                        help="Subset of tests that provide confidence the"
                        " code is working and should be run on check in.")

    # Add a flag to specify which tests to run based on their run time
    Tester.add_argument("--max-runtime", action="store", type=int, default=0,
                        metvar="T",
                        help="Select only tests that claim to run in less than T seconds")

    # Add a flag to specify if jenkins is running the tests
    # This would be used to change the test behaviour when the CI system is using it
    Tester.add_argument("--jenkins", action="store_true",
                        help="Mark the tests as being run in jenkins so alternate setup can be used")

    # Add a flag that takes a list of values
    Tester.add_argument("--feature-sets", action="store_true",
                        metvar="LIST", default=[],
                        help="Specify the feature sets to run tests for")

Positional Arguments
--------------------

Positional arguments are in theory supported but heavily discouraged. The
reason for this is CleverSheep assumes any command line arguments not handled
by the command line parsers are the tests you wish to run. This is very likely
to be broken if positional arguments are used with the main culprit being using
optional positional arguments or getting the nargs value wrong. Given this the
possible benefit that could be gained from using positional args doesn't seem
worth the possibility of breaking the test runner behaviour.

If you have a specific use case that you believe warrants a change to the
library to more fully support positional arguments please raise an issue
outlining your use case and why it needs positional arguments to be achieved.

Using the command line options
------------------------------

In general there tends to be two main use cases for used defined option:

1. Filter which tests will be run based on the options passed in.
2. Enable different test behavior based on the flags passed in.

Filtering which tests will be run
`````````````````````````````````

To filter which tests are run add a function in TestControl.py which will be
called for each test and have access to the command line options and the values
defined in the test decorator. This function is then marked as the test filter
for the Tester, see below for a detailed example.

.. code-block:: python

    #!/usr/bin/env python
    """Test control support to add flags to test runner"""

    from CleverSheep.Test import Tester

    def matches_max_runtime(cs_test_info, max_runtime):
        """Check if the test runtime is less than the max runtime,
        also checks a runtime has been specified"""
        # If a maximum runtime for the test has not been specified run the test
        if not max_runtime:
            return True

        # If the --max-runtime flag has not been passed on the command line
        # run the test
        # For this to be true the test would need to be decorated with a value called runtime, for example:
        #    @test(runtime=100)
        if cs_test_info.runtime is None:
            return True

        return cs_test_info.runtime < max_runtime

    def selectTests(func, cs_test_info):
        """
        Filter tests to run based on flags passed in
        @param func : Unused
        @param cs_test_info: The values defined in the test decorator

        As an example if a test decorator was:
            @test(smoke=True)
        This value could be accessed with:
            cs_test_info.smoke
        """
        options = Tester.userOptions

        # Check if the smoke flag has been set
        if options.smoke:
            # If it has check the test decorator has marked the test as a smoke test
            if not cs_test_info.smoke:
                # This test wasn't a smoke test so return false
                return False

        # Only run tests that run within the specified run time
        if not matches_max_runtime(cs_test_info, options.max_runtime):
            return False

        return True

    # Set the filter to be applied when running tests
    Tester.addTestFilter(selectTests)


Based on what you add in the selectTests function (or equivalent) you can achieve a wide range of behavior such as:

* Only run tests if they have the flag specified

.. code-block:: python

    if options.smoke:
        if not cs_test_info.smoke:
            return False


* Don't run tests with the given flag

.. code-block:: python

    # In the test file
    @test(unreliable=True)

    # The command line flag
    Tester.add_argument("--skip-unreliable", action="store_true",
                        help="Skip running the unreliable tests")

    # In the selectTests functions
    if options.unreliable:
        if cs_test_info.skip_unreliable:
            return False

* Run grouped sets of tests

.. code-block:: python

    # In the test file
    # This test help prove feature 1 4 and 7
    @test(feature_sets=[1,4,7])

    # In the test selection file
    def optionToIntList(option):
        """Change an option list to be integers"""
        if not option:
            return set()
        return set([int(i) for i in option.split(","))

    def selectTests(func, cs_test_info):
        """
        Filter tests to run based on flags passed in
        @param func : Unused
        @param cs_test_info: The values defined in the test decorator
        """
        options = Tester.userOptions
        selected_features_to_test = optionToIntList(options.feature_sets)

        if not cs_test_info.feature_sets:
            # If the test has not been decorated with features it specifically tests we are
            # going to skip it, you may want to invert this behavior depending on your
            # use case
            return False

        # Check to see if this test covers any features we've selected, if not return False
        if len(set(cs_test_info.feature_sets).intersection(selected_features_to_test )) < 1:
            return False

Note the names of the command line options that have been stored are based of
their flag names with the leading dashes removed and any dashes in the name
changed to underscores. For example `--feature-sets` is stored in a param
called `feature_sets`.

Enable different test behavior
``````````````````````````````

The values of user defined flags can be checked in test setup or in the tests
themselves in the following way.

.. code-block:: python

    from CleverSheep.Test import Tester

    # An example of checking the --jenkins flag to change the test setup behavior
    if Tester.userOptions.jenkins:
        # Perform jenkins specific setup
    else:
         # Perform generic setup


.. warning:: Do not try to access the user options from a method that is called
             from a Suite `__init__` method. The Suites are created before the
             user options are populated so trying to access them from within a
             Suite `__init__` will cause an exception.

