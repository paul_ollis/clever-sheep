.. _mock_support:

Mock Support
############

The CleverSheep framework contains some support for mocking high level
components; i.e. other processes and external entities than communicate using
sockets. This part of the framework requires the :mod:`Test.PollManager` to operate and
you many want to read the :ref:`poll_manager` documentation before continuing.

.. contents::
    :local:
    :depth: 1

.. highlight:: python

What is the purpose of Mocks in high level testing?
===================================================

Mocks can serve a number of purposes.

1. Just keeping the :term:`SUT` happy, by (for example) making/accepting connections, responding normally to messages and sending keep-alives. Thus the test script itself need only deal with the details of the functions being tested.
2. Drive the :term:`SUT` in specific ways, as required by certain tests. This is quite typical and tends to be a layer of functionality on top of purpose (1) above.
3. Provide ways of extracting information about the :term:`SUT`, by logging messages, responses or otherwise communicating with the SUT.
4. Allow you to force the :term:`SUT` to experience scenarios that are difficult or impossible to cause using real components.

One thing that mocks in a CleverSheep test often do is put events into the
:attr:`~Test.TestEventStore.eventStore` so that the test script can check that
expected events occur in a timely manner.

CleverSheep Mocks
=================

The :mod:`Test.Mock.Component` module provides the specific support. To create
a mock, you create a class that inherits from
:class:`Test.Mock.Component.Component` and override certain methods, depending
on your needs.

.. rubric:: Mocking a server:

You would typically over-ride :func:`onIncomingConnect`, :func:`onReceive` and,
optionally, :func:`onClose` and :func:`onError`.

.. rubric:: Mocking a client:

You would typically over-ride :func:`onOutgoingConnect` and, optionally,
:func:`onClose` and :func:`onError`.

A Simple example
================

The most basic thing a mock does is keep the :term:`SUT` by pretending to be the
the component in question.

To illustrate the above here is a simple, if not very realistic, example.

We have an application called wc.py, which takes a number of command line
arguments and prints out the total number of words in all the arguments. However
the way it is implemented is that it concatenates the arguments and then sends
the resulting string to a wc_server program, using a TCP socket. The server
counts the words and send the number back as a string.

We wish to test the wc.py (client) application.

The Mock component
------------------

As we want to test wc.py in isolation we need to mock the ``wc_server``. The
following will do this:

::

    from CleverSheep.Test.Mock import Component

    class Server(Component.Component):
        def __init__(self, *args, **kwargs):
            super(Server, self).__init__(*args, **kwargs)
            self.listenFor("Client", ("localhost", 7899))

        def onIncomingConnect(self, s, peerName):
            print("Connect from", peerName)

        def onReceive(self, conn):
            data = conn.read()
            print("From %s: %r`" % (conn.peerName, data))
            n = len(data.split())
            self.sendTo(conn.peerName, "%d" % n)

Obviously this example is unrealistically trivial, but it should give some
indication of the relative ease with which mocks can be constructed. The mock
is created by inheriting from :class:`~Test.Mock.Component.Component` and we arrange for the component to
start listening for connections immediately upon construction with the call to
:func:`listenFor`. The name “Client” in the :func:`listenFor` method is a symbolic name for
the peer that is expected to connect; the ``wc.py`` client in this case.

The :meth:`~Test.Mock.Component.Component.onIncomingConnect` method is automatically called when a new connection
is received (and accepted). We over-ride it just to log the connection, but
otherwise need not have bothered.

The :meth:`~Test.Mock.Component.Component.onReceive` method is automatically
called when there is data to read from a connected socket. This mocks up the
expected behaviour; counts words and sends back the result.

The Test
--------

Now lets look at a test script which uses this mock and does some testing of the
client.

::

    #! /usr/bin/env python
    """Tests for word count client (wc.py)."""

    import os
    import subprocess

    from CleverSheep.Test.Tester import *

    import wc_server_mock        # The mock component.


    class WcClient(Suite):
        """Tests for word counting client."""
        def setUp(self):
            self.server = wc_server_mock.Server(self.control.poll)
            self.client = None

        def tearDown(self):
            if self.client:
                if self.client.poll() is None:
                    os.kill(self.client.pid, 15)

        @test
        def single_word(self):
            """Single sentence, single word."""
            sentence = "Hello"
            f = open("client.log", "w")
            self.client = subprocess.Popen(["/usr/bin/python", "./wc.py", sentence], stdout=f)

            # This could cause an infinite loop if the client fails to stop
            # See below for more on this
            while self.client.poll() is None:
                self.control.delay(0.1)
            f.close()

            v = open("client.log").read().strip()
            failUnlessEqual("1", v)

        # More tests here...


    if __name__ == "__main__":
        runModule()


We use the :func:`setUp` to create an instance of the server mock. In the
:func:`tearDown` we make sure that the ``wc.py`` client process is stopped, in
case it fails to stop correctly during any test. This is simplified code. In
reality we should allow a grace period for the client to die, verify it has
terminated and terminate with extreme prejudice if necessary.

Notice that the object :attr:`self.control.poll` is passed to the mock in the :func:`setUp`.
It is this that ensures that the mock is properly managed by the test framework.

Obviously we need to run the wc.py program in order to test it. It might seem
that the obvious approach would be to use the standard :func:`commands.getoutput`
function. As follows:

::

    v = commands.getoutput("./wc.py %s" % sentence)
    failUnlessEqual("1", v)

This is simpler that the actual test code. However this approach has some problems:

* There is no guarantee that the call to :func:`commands.getoutput` will complete meaning the test will not complete.
* It hands control to the :term:`SUT`, which is not a good idea. The test script should retain control.
* It will not work. The test framework uses the :class:`PollManager`, running in single
  thread. While the script is in the :func:`commands.getoutput` call the thread cannot do anything else; i.e the mock cannot accept the connection from the client.

Hence we run the client as a subprocess and then loop waiting for the client
process to finish. This does work because the call to :func:`self.control.delay`
hands control to the :func:`self.control.poll` object (which is a
:class:`Test.PollManager.PollManger` instance) for 0.1 seconds at a time. During this
time the poll object ensures that the mock server can respond to the connection
request and subsequent message.

The :attr:`self.control` member is key to writing tests that require this kind of high
level mocking. Essentially the script should hand over control to :attr:`self.control`
whenever the script needs to wait for something to happen. Typically, in this
kind of testing, most of the elapsed time occurs under the management of
:attr:`self.control`. Normally control is handed over when expecting events, see the
:ref:`test_event_store` page for more details on this.

A more CleverSheep-esque version
--------------------------------

As highlighted above the current test script could fall into an infinite loop
if the client `wc.py` fails to stop. This can of course be fixed by looping a
set number of times:

::

    for i in range(10):
        if self.client.poll() is not None:
            break
        self.control.delay(0.1)
    else:
        # We looped 10 times, the client did not terminate.
        fail("Client failed to terminate after about 1 second")

This can be a suitable approach in some instances, but the framework provides
a more elegant approach. For this we add some helper methods and change the
test method.

::

    def client_has_terminated(self):
     return self.client.poll() is not None

    @test
    def single_word(self):
     """Single sentence, single word."""
     timeout = 1.0

     def describe_timeout_failure():
         return "Client failed to terminate after %.1f seconds" % timeout

     sentence = "Hello"
     f = open("client.log", "w")
     self.client = subprocess.Popen(["/usr/bin/python", "./wc.py", sentence], stdout=f)

     self.control.expect(timeout, self.client_has_terminated, describe_timeout_failure)
     f.close()

     v = open("client.log").read().strip()
     failUnlessEqual("1", v)

This may look more complicated (in some ways it is), but is is arguably cleaner
and more elegant. For example, we no longer have a loop so the script reads much
more like a sequence of steps.

At the script hands control over to :attr:`self.control` by calling the :meth:`expect` method.
The first argument 1.0 tells the method that we expect something to happen within
1.0 second. The second argument provides a function that can be used to check if
the event has happened. The final argument provides a function that will return
a suitable error message if the event fails to happen. The
:meth:`client_has_terminated method` will get called occasionally and it must return a
True value if it determines that the event has occurred.

The :meth:`self.control.expect` method will return as soon as the
:meth:`client_has_terminated` a True value, it will only wait the full second
if the event does not occur, in which case it will make the test fail.

Notice that the :meth:`describe_timeout_failure` function is nested inside the test
method. This can be useful in test methods because it allows (as in this example)
the generated message to include details of the failure context; the timeout
period in this case.