#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# Move into this folder so we know where we are
cd $BASEDIR

# Move into the CleverSheep source folder
cd ../src/CleverSheep

# Run the pycodestyle command and store the number of issues, don't let this
# command fail if there are issues
ISSUE_COUNT="$(pycodestyle --count . 2>&1 > $BASEDIR/generated_files/pycodestyle_output.txt || true)"

echo $ISSUE_COUNT

# Write the number of issues to file so that it can be archived and used later
echo -n $ISSUE_COUNT > $BASEDIR/generated_files/pycodestyle_issue_count.txt

# Return non-zero if any command in the script has failed
test $err -eq 0