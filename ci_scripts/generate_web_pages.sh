#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# cd into this folder so we know where we are
cd $BASEDIR

# Store the plyint score for later use
PYLINT_SCORE=$(<$BASEDIR/generated_files/pylint_score.txt)

# Store the number of pycodestyle issues for later use
PYCODESTYLE_ISSUES=$(<$BASEDIR/generated_files/pycodestyle_issue_count.txt)

# Store the coverage percent for later use
COVERAGE_PERCENT=$(<$BASEDIR/generated_files/coverage_percent.txt)

# Copy the templates into place
pushd ../gitlab-pages > /dev/null
cp templates/* generated/

echo "Coverage"

# Set the coverage percent
sed -i "s/COVERAGE_PERCENT/$COVERAGE_PERCENT/g" generated/index.html

echo "Pylint"

# Set the pylint score
sed -i "s/PYLINT_SCORE/$PYLINT_SCORE/g" generated/index.html

echo "PyCodeStyle"

# Set the number of pycodestyle issues
sed -i "s/PYCODESTYLE_ISSUES/$PYCODESTYLE_ISSUES/g" generated/index.html

echo "Last Updated"

# Set the last updated time
LAST_UPDATED="$(date)"

sed -i "s/LAST_UPDATED/$LAST_UPDATED/g" generated/index.html

mkdir -p api

pushd api > /dev/null
# Generate API docs

# Clear out the build folder
rm -rf _build/*

# Generate api and config blacklist
DIRS=`find ../../src/CleverSheep/ -type d`
APIBLACKLIST=()
CONFBLACKLIST=()
CONFBLACKLIST+="'_build'"
CONFBLACKLIST+=", 'Thumbs.db'"
CONFBLACKLIST+=", '.DS_Store'"
for file in `find ../../src/CleverSheep -regex .*.py`; do
  if ! grep -q $file ../../ci_scripts/api_whitelist.txt; then
    CONFBLACKLIST+=", '$file'"
    APIBLACKLIST+=" $file"
  fi
done

for dir in $DIRS; do
  if ! grep -q $dir ../../ci_scripts/api_whitelist.txt; then
    CONFBLACKLIST+=", '$dir'"
    APIBLACKLIST+=" $dir"
  fi
done

# Generate restructed text files
sphinx-apidoc -o ./ ../../src/CleverSheep/ $APIBLACKLIST

# Generate html and copy to appropriate location
make html
mkdir -p ../generated/api
cp -r _build/html/* ../generated/api

popd > /dev/null

popd > /dev/null

# Copy the coverage html into place
cp -r $BASEDIR/generated_files/coverage_html ../gitlab-pages/generated/

# Return non-zero if any command in the script has failed
test $err -eq 0
