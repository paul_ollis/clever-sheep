#!/usr/bin/env bash
# Script to run the CleverSheep tests with code coverage

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# cd into this folder so we know where we are
cd $BASEDIR

# Go into the test folder
pushd ../test

# Run the tests
./all_tests.py --keep-going $@

popd

# Return non-zero if any command in the script has failed
test $err -eq 0