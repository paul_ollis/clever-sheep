#!/usr/bin/env python
"""CleverSheep.Log.LogSupport unit tests"""

import six

import mock
from six.moves import builtins
import six

from six.moves import cStringIO as StringIO

# noinspection PyUnresolvedReferences
import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Log import LogSupport


class F(object):
    def __init__(self):
        self.flushed = False

    def flush(self):
        self.flushed = True


class TestPublicLogFunction(Suite):
    """Test the openFullPublicLog function"""

    @test
    @mock.patch('CleverSheep.Prog.Files.chmod')
    @mock.patch.object(builtins, 'open')
    def test_open_full_public_log(self, mock_open, mock_chmod):
        """Test that openFullPublicLog gives a file full read/write permissions
        """
        # Arrange
        file_path = "/tmp/test_file.txt"
        mode = "w"
        mock_file = mock.MagicMock()
        mock_open.return_value = mock_file

        # Act
        LogSupport.openFullPublicLog(file_path, mode)

        # Assert
        mock_open.assert_called_once_with(file_path, mode)
        mock_chmod.assert_called_once_with(file_path, "a+rw")


class TestHangStream(Suite):
    """Tests for the HangStream class."""

    @test
    def test_write_single_line(self):
        """Test the write method with a single line."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.write("A single line\n")
        failUnlessEqual("PREFIX:A single line\n", f.getvalue())

    @test
    def test_write_multi_line(self):
        """Test the write method with a multi line."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.write("Several\nlines\n")
        failUnlessEqual("PREFIX:Several\n       lines\n", f.getvalue())

    @test
    def writelines(self):
        """Test the writelines method."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.writelines(["Several\n", "lines\n"])
        failUnlessEqual("PREFIX:Several\n       lines\n", f.getvalue())

    @test
    def write_no_lines(self):
        """Test the writelines method gracefully handles no lines."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.writelines([])
        failUnlessEqual("", f.getvalue())

    @test
    def write_partial_lines(self):
        """Test the writelines tracks when newline is required."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.writelines(["Several\n", "li"])
        h.writelines(["nes\n"])
        failUnlessEqual("PREFIX:Several\n       lines\n", f.getvalue())

    @test
    def empty_prefix(self):
        """Test that an empty prefix is okay."""
        f = StringIO()
        h = LogSupport.HangStream(f)
        h.writelines(["Several\n", "lines\n"])
        failUnlessEqual("Several\nlines\n", f.getvalue())

    @test
    def flushinc(self):
        """Test that flush is passed to the underlying file."""
        f = F()
        f.flushed = False
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.flush()
        failUnless(f.flushed)


if __name__ == "__main__":
    runModule()
