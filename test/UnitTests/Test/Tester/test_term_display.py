#!/usr/bin/env python
"""Test the classes defined in TermDisplay.py"""

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester import fail
from CleverSheep.Test.Tester import log
from CleverSheep.Test.Tester.TermDisplay import LogReporter


class LogReporterTestSuite(Suite):
    """Test the Log Reporter"""

    @test()
    def test_invalid_message_string_handling(self):
        """Test the library handling when it is given a non string as an
        error message in a fail function."""

        not_string = {"not_a_string": 12}

        try:
            exc = Exception()
            exc.message = not_string
            exc.stack = ""

            reporter = LogReporter()

            reporter._logFailException(exc)
            fail("Expected exception was not raised")
        except SyntaxError as e:
            log.debug("An exception occurred {0}".format(e))


if __name__ == "__main__":
    runModule()