#!/usr/bin/env python
"""Test the classes defined in Core.py"""

import CheckEnv
import mock

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule

from CleverSheep.Test.Tester import fail
from CleverSheep.Test.Tester import failUnless
from CleverSheep.Test.Tester import failUnlessEqual

from CleverSheep.Test.Tester import Core
from CleverSheep.Test.Tester.Core import RunRecord
from CleverSheep.Test.Tester.Core import StepNames
from CleverSheep.Test.Tester.Core import StepRecord
from CleverSheep.Test.Tester.Core import StepRecordList


class RunRecordTestGeneric(Suite):
    """Unit tests for the RunRecord class that are not targeted at specific
    functions"""

    @test
    def test_add_step_record_simple(self):
        """Test adding a simple record"""
        # Given
        record = RunRecord()

        name = StepNames.RUN

        # When
        record.addStepRecord(name)

        # Then
        failUnlessEqual(1, len(record._records))
        failUnless(name in record._records)
        failUnless(type(record._records[name]) is StepRecord)

    @test
    def test_add_step_record_simple_duplicate(self):
        """Test adding a simple record multiple times. This should cause an
        assertion error"""
        # Given
        record = RunRecord()

        name = StepNames.SET_UP

        # When
        record.addStepRecord(name)

        try:
            record.addStepRecord(name)

            fail("No assertion error raised")
        except AssertionError:
            # Then
            pass

    @test
    def test_add_step_record_list(self):
        """Test adding a list record"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_TEAR_DOWN

        # When
        record.addStepRecord(name)

        # Then
        failUnlessEqual(1, len(record._records))
        failUnless(name in record._records)
        failUnless(type(record._records[name]) is StepRecordList)
        failUnlessEqual(1, len(record._records[name]))

    @test
    def test_add_step_record_list_duplicate(self):
        """Test adding a list record multiple times. This should add the entry
        to the list of entries"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_SET_UP

        # When
        record.addStepRecord(name)
        record.addStepRecord(name)

        # Then
        failUnlessEqual(1, len(record._records))
        failUnless(name in record._records)
        failUnless(type(record._records[name]) is StepRecordList)
        failUnlessEqual(2, len(record._records[name]))

    @test
    def test_get_step_record_no_records(self):
        """Call the getStepRecord function where there are no records"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_TEAR_DOWN

        # When
        step_record = record.getStepRecord(name)

        # Then
        failUnlessEqual(None, step_record)

    @test
    def test_get_step_record_simple_record(self):
        """Call the getStepRecord function for a simple record, expect
        the record to be returned."""
        # Given
        record = RunRecord()

        name = StepNames.RUN

        record.addStepRecord(name)

        # When
        step_record = record.getStepRecord(name)

        # Then
        failUnless(type(step_record) is StepRecord)

    @test
    def test_get_step_record_list_record(self):
        """Call the getStepRecord function for a list record, expect
        the record to be returned."""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_TEAR_DOWN

        record.addStepRecord(name)

        # When
        step_record = record.getStepRecord(name)

        # Then
        failUnless(type(step_record) is StepRecord)

    @test
    def test_get_step_record_multiple_list_records(self):
        """Test that with multiple records for a list where none have
        failed the first is returned"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_TEAR_DOWN

        record.addStepRecord(name)
        record.addStepRecord(name)

        # Give the records different states so we can distinguish
        # between them
        record._records[name].entries[0].state = Core.State.PASS
        record._records[name].entries[1].state = Core.State.SKIPPED

        # When
        step_record = record.getStepRecord(name)

        # Then
        failUnless(type(step_record) is StepRecord)

        # We expect the first record to be returned as neither failed
        failUnless(Core.State.PASS, step_record.state)

    @test
    def test_get_step_record_failing_list_record(self):
        """Test that with multiple records for a list where one has
        failed the failed record is returned"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_TEAR_DOWN

        record.addStepRecord(name)
        record.addStepRecord(name)

        # Give the records different states so we can distinguish
        # between them
        record._records[name].entries[0].state = Core.State.PASS
        record._records[name].entries[1].state = Core.State.FAIL

        # When
        step_record = record.getStepRecord(name)

        # Then
        failUnless(type(step_record) is StepRecord)

        # We expect the failed record to be returned
        failUnless(Core.State.FAIL, step_record.state)

    @test
    def test_has_failed_no_records(self):
        """Test the hasFailed method with no step records"""
        # Given
        record = RunRecord()

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is False)

    @test
    def test_has_failed_no_failing_records(self):
        """Test the hasFailed method with no failing records"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_SET_UP

        record.addStepRecord(name)
        record._records[name].entries[0].state = Core.State.PASS

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is False)

    @test
    def test_has_failed_suite_set_up(self):
        """Test the hasFailed method with a failing suite set up"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_SET_UP

        record.addStepRecord(name)
        record._records[name].entries[0].state = Core.State.FAIL

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is True)

    @test
    def test_has_failed_set_up(self):
        """Test the hasFailed method with a failing set up"""
        # Given
        record = RunRecord()

        name = StepNames.SET_UP

        record.addStepRecord(name)
        record._records[name].state = Core.State.FAIL

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is True)

    @test
    def test_has_failed_run(self):
        """Test the hasFailed method with a failing run step"""
        # Given
        record = RunRecord()

        name = StepNames.RUN

        record.addStepRecord(name)
        record._records[name].state = Core.State.FAIL

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is True)

    @test
    def test_has_failed_tear_down(self):
        """Test the hasFailed method with a failing tear down step"""
        # Given
        record = RunRecord()

        name = StepNames.TEAR_DOWN

        record.addStepRecord(name)
        record._records[name].state = Core.State.FAIL

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is True)

    @test
    def test_has_failed_suite_tear_down(self):
        """Test the hasFailed method with a failing suite tear down"""
        # Given
        record = RunRecord()

        name = StepNames.SUITE_TEAR_DOWN

        record.addStepRecord(name)
        record._records[name].entries[0].state = Core.State.FAIL

        # When
        has_failed = record.hasFailed

        # Then
        failUnless(has_failed is True)

    @test
    def test_state_no_step_records(self):
        """Test getting the state of a run record when it has no step records
        """
        # Given
        record = RunRecord()

        # When
        state = record.state

        # Then
        failUnlessEqual(Core.State.NOT_RUN, state)


class RunRecordTestSuiteSetUpState(Suite):
    """Unit tests for the RunRecord state function when testing the suite set
    up step"""

    @staticmethod
    def perform_test(step_state, expected_state):
        """Helper function that performs a state test for a list step

        :param step_state: The state of the step for this test
        :param expected_state: The expected resultant state
        :return: None
        """
        # Given
        record = RunRecord()

        step_name = StepNames.SUITE_SET_UP

        record.addStepRecord(step_name)
        record._records[step_name].entries[0].state = step_state

        # When
        state = record.state

        # Then
        failUnlessEqual(expected_state, state)

    @test
    def test_state_suite_set_up_not_run(self):
        """Test getting the state of a run record when the suite setup is not
        run
        """
        self.perform_test(step_state=Core.State.NOT_RUN,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_suite_set_up_pass(self):
        """Test getting the state of a run record when the suite setup is pass
        """
        # If the record passed we should move on to the next record state,
        # there aren't any so we should still get not run
        self.perform_test(step_state=Core.State.PASS,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_suite_set_up_fail(self):
        """Test getting the state of a run record when the suite setup is fail
        """
        self.perform_test(step_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_SUITE_SETUP)

    @test
    def test_state_suite_set_up_exit_suite(self):
        """Test getting the state of a run record when the suite setup is exit
        suite
        """
        self.perform_test(step_state=Core.State.EXIT_SUITE,
                          expected_state=Core.State.EXIT_SUITE)

    @test
    def test_state_suite_set_up_exit_all(self):
        """Test getting the state of a run record when the suite setup is exit
        all
        """
        self.perform_test(step_state=Core.State.EXIT_ALL,
                          expected_state=Core.State.EXIT_ALL)


class RunRecordTestSetUpState(Suite):
    """Unit tests for the RunRecord state function when testing the set
    up step"""

    @staticmethod
    def perform_test(step_state, expected_state):
        """Helper function that performs a state test for a step

        :param step_state: The state of the step for this test
        :param expected_state: The expected resultant state
        :return: None
        """
        # Given
        record = RunRecord()

        step_name = StepNames.SET_UP

        record.addStepRecord(step_name)
        record._records[step_name].state = step_state

        # When
        state = record.state

        # Then
        failUnlessEqual(expected_state, state)

    @test
    def test_state_set_up_not_run(self):
        """Test getting the state of a run record when the setup is not run
        """
        self.perform_test(step_state=Core.State.NOT_RUN,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_set_up_pass(self):
        """Test getting the state of a run record when the setup is pass
        """
        # If the record passed we should move on to the next record state,
        # there aren't any so we should still get not run
        self.perform_test(step_state=Core.State.PASS,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_set_up_fail(self):
        """Test getting the state of a run record when the setup is fail
        """
        self.perform_test(step_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_SETUP)

    @test
    def test_state_set_up_exit_suite(self):
        """Test getting the state of a run record when the setup is exit suite
        """
        self.perform_test(step_state=Core.State.EXIT_SUITE,
                          expected_state=Core.State.EXIT_SUITE)

    @test
    def test_state_set_up_exit_all(self):
        """Test getting the state of a run record when the setup is exit all
        """
        self.perform_test(step_state=Core.State.EXIT_ALL,
                          expected_state=Core.State.EXIT_ALL)


class RunRecordTestRunState(Suite):
    """Unit tests for the RunRecord state function when testing the run step"""

    @staticmethod
    def perform_test(step_state, expected_state):
        """Helper function that performs a state test for a step

        :param step_state: The state of the step for this test
        :param expected_state: The expected resultant state
        :return: None
        """
        # Given
        record = RunRecord()

        step_name = StepNames.RUN

        record.addStepRecord(step_name)
        record._records[step_name].state = step_state

        # When
        state = record.state

        # Then
        failUnlessEqual(expected_state, state)

    @test
    def test_state_run_not_run(self):
        """Test getting the state of a run record when the run step is not run
        """
        self.perform_test(step_state=Core.State.NOT_RUN,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_run_pass(self):
        """Test getting the state of a run record when the run step is pass
        """
        self.perform_test(step_state=Core.State.PASS,
                          expected_state=Core.State.PASS)

    @test
    def test_state_run_fail(self):
        """Test getting the state of a run record when the run step is fail
        """
        self.perform_test(step_state=Core.State.FAIL,
                          expected_state=Core.State.FAIL)

    @test
    def test_state_run_exit_suite(self):
        """Test getting the state of a run record when the run step is exit
        suite
        """
        self.perform_test(step_state=Core.State.EXIT_SUITE,
                          expected_state=Core.State.EXIT_SUITE)

    @test
    def test_state_run_exit_all(self):
        """Test getting the state of a run record when the run step is exit all
        """
        self.perform_test(step_state=Core.State.EXIT_ALL,
                          expected_state=Core.State.EXIT_ALL)

    @test
    def test_state_run_skipped(self):
        """Test getting the state of a run record when the run step is skipped
        """
        self.perform_test(step_state=Core.State.SKIPPED,
                          expected_state=Core.State.SKIPPED)

    @test
    def test_state_run_bug(self):
        """Test getting the state of a run record when the run step is bug
        """
        self.perform_test(step_state=Core.State.BUG,
                          expected_state=Core.State.BUG)

    @test
    def test_state_run_bug_pass(self):
        """Test getting the state of a run record when the run step is bug pass
        """
        self.perform_test(step_state=Core.State.BUG_PASS,
                          expected_state=Core.State.BUG_PASS)

    @test
    def test_state_run_todo(self):
        """Test getting the state of a run record when the run step is todo
        """
        self.perform_test(step_state=Core.State.TODO,
                          expected_state=Core.State.TODO)


class RunRecordTestTearDownState(Suite):
    """Unit tests for the RunRecord state function when testing the tear down
    step"""

    @staticmethod
    def perform_test(step_state, expected_state):
        """Helper function that performs a state test for a step

        :param step_state: The state of the step for this test
        :param expected_state: The expected resultant state
        :return: None
        """
        # Given
        record = RunRecord()

        # Note these tests need a valid run value to work
        name = StepNames.RUN
        record.addStepRecord(name)
        record._records[name].state = Core.State.PASS

        step_name = StepNames.TEAR_DOWN

        record.addStepRecord(step_name)
        record._records[step_name].state = step_state

        # When
        state = record.state

        # Then
        failUnlessEqual(expected_state, state)

    @test
    def test_state_tear_down_not_run(self):
        """Test getting the state of a run record when the tear down is not run
        """
        self.perform_test(step_state=Core.State.NOT_RUN,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_tear_down_pass(self):
        """Test getting the state of a run record when the tear down is pass
        """
        self.perform_test(step_state=Core.State.PASS,
                          expected_state=Core.State.PASS)

    @test
    def test_state_tear_down_fail(self):
        """Test getting the state of a run record when the tear down is fail
        """
        self.perform_test(step_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_TEARDOWN)

    @test
    def test_state_tear_down_exit_suite(self):
        """Test getting the state of a run record when the tear down is exit
        suite
        """
        self.perform_test(step_state=Core.State.EXIT_SUITE,
                          expected_state=Core.State.EXIT_SUITE)

    @test
    def test_state_tear_down_exit_all(self):
        """Test getting the state of a run record when the tear down is exit all
        """
        self.perform_test(step_state=Core.State.EXIT_ALL,
                          expected_state=Core.State.EXIT_ALL)


class RunRecordTestSuiteTearDownState(Suite):
    """Unit tests for the RunRecord state function when testing the suite tear
    down step"""

    @staticmethod
    def perform_test(step_state, expected_state):
        """Helper function that performs a state test for a list step

        :param step_state: The state of the step for this test
        :param expected_state: The expected resultant state
        :return: None
        """
        # Given
        record = RunRecord()

        # Note these tests need a valid run value to work
        name = StepNames.RUN
        record.addStepRecord(name)
        record._records[name].state = Core.State.PASS

        step_name = StepNames.SUITE_TEAR_DOWN

        record.addStepRecord(step_name)
        record._records[step_name].entries[0].state = step_state

        # When
        state = record.state

        # Then
        failUnlessEqual(expected_state, state)

    @test
    def test_state_suite_tear_down_not_run(self):
        """Test getting the state of a run record when the suite tear down is
        not run
        """
        self.perform_test(step_state=Core.State.NOT_RUN,
                          expected_state=Core.State.NOT_RUN)

    @test
    def test_state_suite_tear_down_pass(self):
        """Test getting the state of a run record when the suite tear down is
        pass
        """
        self.perform_test(step_state=Core.State.PASS,
                          expected_state=Core.State.PASS)

    @test
    def test_state_suite_tear_down_fail(self):
        """Test getting the state of a run record when the suite tear down is
        fail
        """
        self.perform_test(step_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_SUITE_TEARDOWN)

    @test
    def test_state_suite_tear_down_exit_suite(self):
        """Test getting the state of a run record when the suite tear down is
        exit suite
        """
        self.perform_test(step_state=Core.State.EXIT_SUITE,
                          expected_state=Core.State.EXIT_SUITE)

    @test
    def test_state_suite_tear_down_exit_all(self):
        """Test getting the state of a run record when the suite tear down is
        exit all
        """
        self.perform_test(step_state=Core.State.EXIT_ALL,
                          expected_state=Core.State.EXIT_ALL)


class RunRecordTestCombinedStepStates(Suite):
    """Test the state function with combined steps"""

    @staticmethod
    def perform_test(suite_set_up_state, set_up_state, run_state,
                     tear_down_state, suite_tear_down_state, expected_state):
        """Helper function that performs the test based on the provided params

        :param suite_set_up_state: Expected suite set up state
        :param set_up_state: Expected set up state
        :param run_state: Expected run state
        :param tear_down_state: Expected tear down state
        :param suite_tear_down_state: Expected suite tear down state
        :param expected_state: Expected resultant state
        :return: None
        """
        # Given
        record = RunRecord()

        step_name = StepNames.SUITE_SET_UP
        record.addStepRecord(step_name)
        record._records[step_name].entries[0].state = suite_set_up_state

        name = StepNames.SET_UP
        record.addStepRecord(name)
        record._records[name].state = set_up_state

        name = StepNames.RUN
        record.addStepRecord(name)
        record._records[name].state = run_state

        name = StepNames.TEAR_DOWN
        record.addStepRecord(name)
        record._records[name].state = tear_down_state

        step_name = StepNames.SUITE_TEAR_DOWN
        record.addStepRecord(step_name)
        record._records[step_name].entries[0].state = suite_tear_down_state

        # When
        state = record.state

        # Then
        failUnlessEqual(expected_state, state)

    @test
    def test_state_with_all_steps_no_failures(self):
        """Test that when all steps are present add passing the run step state
        is used"""
        self.perform_test(suite_set_up_state=Core.State.PASS,
                          set_up_state=Core.State.PASS,
                          run_state=Core.State.BUG_PASS,
                          tear_down_state=Core.State.PASS,
                          suite_tear_down_state=Core.State.PASS,
                          expected_state=Core.State.BUG_PASS)

    @test
    def test_state_with_all_steps_suite_set_up_failure(self):
        """Test that when all steps are present if the suite set up fails it is
        reported even if later steps fail"""
        # Given
        self.perform_test(suite_set_up_state=Core.State.FAIL,
                          set_up_state=Core.State.NOT_RUN,
                          run_state=Core.State.NOT_RUN,
                          tear_down_state=Core.State.NOT_RUN,
                          suite_tear_down_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_SUITE_SETUP)

    @test
    def test_state_with_all_steps_set_up_failure(self):
        """Test that when all steps are present if the set up fails it is
        reported even if later steps fail"""
        self.perform_test(suite_set_up_state=Core.State.PASS,
                          set_up_state=Core.State.FAIL,
                          run_state=Core.State.NOT_RUN,
                          tear_down_state=Core.State.FAIL,
                          suite_tear_down_state=Core.State.PASS,
                          expected_state=Core.State.BAD_SETUP)

    @test
    def test_state_with_all_steps_run_failure(self):
        """Test that when all steps are present if the run step fails it is
        reported even if later steps fail"""
        self.perform_test(suite_set_up_state=Core.State.PASS,
                          set_up_state=Core.State.PASS,
                          run_state=Core.State.FAIL,
                          tear_down_state=Core.State.PASS,
                          suite_tear_down_state=Core.State.FAIL,
                          expected_state=Core.State.FAIL)

    @test
    def test_state_with_all_steps_tear_down_failure(self):
        """Test that when all steps are present if the tear down fails it is
        reported even if the run step passed and later steps fail"""
        self.perform_test(suite_set_up_state=Core.State.PASS,
                          set_up_state=Core.State.PASS,
                          run_state=Core.State.BUG_PASS,
                          tear_down_state=Core.State.FAIL,
                          suite_tear_down_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_TEARDOWN)

    @test
    def test_state_with_all_steps_suite_tear_down_failure(self):
        """Test that when all steps are present if the suite tear down fails it
        is reported even if the run step passed"""
        self.perform_test(suite_set_up_state=Core.State.PASS,
                          set_up_state=Core.State.PASS,
                          run_state=Core.State.BUG_PASS,
                          tear_down_state=Core.State.PASS,
                          suite_tear_down_state=Core.State.FAIL,
                          expected_state=Core.State.BAD_SUITE_TEARDOWN)


class SuiteState(Suite):
    """Test the Suite state method"""

    def __init__(self):
        """Define class variables"""
        self.suite = None
        # Children for the test
        self.children = []

    def setUp(self):
        """Create the object under test"""
        self.suite = Core.Suite(myDir=None, item=None,
                                uid=None, parentUid=None,
                                context=None)
        # Reset the child list
        self.children = []

    def add_child(self, child_state):
        """Add a child to the list of children that will be associated with
        the suite

        :param child_state: The state of the child
        :return: None
        """
        mock_test = mock.MagicMock()
        mock_test.state = child_state
        mock_test.parent = self.suite
        self.children.append(mock_test)

    def perform_single_child_test(self, child_state, expected_suite_state):
        """Setup a single child with the passed in state, check the state on
        the suite is as expected

        :param child_state: The state of the child
        :param expected_suite_state: The expected suite state
        :return: None
        """
        # Given
        mock_collection = mock.MagicMock()
        self.add_child(child_state)
        mock_collection.__iter__.return_value = self.children

        self.suite.setCollection(mock_collection)

        # When
        state = self.suite.state

        # Then
        failUnlessEqual(expected_suite_state, state)

    @test
    def test_suite_state_no_children(self):
        """Test that the suite state method returns not run if there are no
        children"""
        # Given
        mock_collection = mock.MagicMock()
        self.suite.setCollection(mock_collection)

        # When
        state = self.suite.state

        # Then
        failUnlessEqual(Core.State.NOT_RUN, state)

    @test
    def test_suite_state_fail(self):
        """Test that the suite state method returns child fail if a child
        reports its state as fail
        """
        self.perform_single_child_test(
            child_state=Core.State.FAIL,
            expected_suite_state=Core.State.CHILD_FAIL)

    @test
    def test_suite_state_child_fail(self):
        """Test that the suite state method returns child fail if a child
        reports its state as child fail
        """
        self.perform_single_child_test(
            child_state=Core.State.CHILD_FAIL,
            expected_suite_state=Core.State.CHILD_FAIL)

    @test
    def test_suite_state_bad_suite_set_up(self):
        """Test that the suite state method returns bad suite setup if a child
        reports its state as bad suite set up
        """
        self.perform_single_child_test(
            child_state=Core.State.BAD_SUITE_SETUP,
            expected_suite_state=Core.State.BAD_SUITE_SETUP)

    @test
    def test_suite_state_bad_set_up(self):
        """Test that the suite state method returns child fail if a child
        reports its state as bad set up
        """
        self.perform_single_child_test(
            child_state=Core.State.BAD_SETUP,
            expected_suite_state=Core.State.CHILD_FAIL)

    @test
    def test_suite_state_bad_tear_down(self):
        """Test that the suite state method returns child fail if a child
        reports its state as bad tear down
        """
        self.perform_single_child_test(
            child_state=Core.State.BAD_TEARDOWN,
            expected_suite_state=Core.State.CHILD_FAIL)

    @test
    def test_suite_state_bad_suite_tear_down(self):
        """Test that the suite state method returns bad suite tear down if a
        child reports its state as bad suite tear down
        """
        self.perform_single_child_test(
            child_state=Core.State.BAD_SUITE_TEARDOWN,
            expected_suite_state=Core.State.BAD_SUITE_TEARDOWN)

    @test
    def test_suite_state_part_run(self):
        """Test that the suite state method returns part run if a
        child reports its state as part run
        """
        self.perform_single_child_test(
            child_state=Core.State.PART_RUN,
            expected_suite_state=Core.State.PART_RUN)

    @test
    def test_suite_state_part_run(self):
        """Test that the suite state method returns not run if a
        child reports its state as not run
        """
        self.perform_single_child_test(
            child_state=Core.State.NOT_RUN,
            expected_suite_state=Core.State.NOT_RUN)

    @test
    def test_suite_state_exit_suite(self):
        """Test that the suite state method returns exit suite if a
        child reports its state as exit suite
        """
        self.perform_single_child_test(
            child_state=Core.State.EXIT_SUITE,
            expected_suite_state=Core.State.EXIT_SUITE)

    @test
    def test_suite_state_exit_all(self):
        """Test that the suite state method returns exit all if a
        child reports its state as exit all
        """
        self.perform_single_child_test(
            child_state=Core.State.EXIT_ALL,
            expected_suite_state=Core.State.EXIT_ALL)

    @test
    def test_suite_state_user_stopped(self):
        """Test that the suite state method returns user stopped if a
        child reports its state as user stopped
        """
        self.perform_single_child_test(
            child_state=Core.State.USER_STOPPED,
            expected_suite_state=Core.State.USER_STOPPED)

    @test
    def test_suite_state_pass(self):
        """Test that the suite state method returns pass if a
        child reports its state as one of the other state
        """
        for state in [Core.State.PASS, Core.State.SKIPPED, Core.State.TODO,
                      Core.State.BUG, Core.State.BUG_PASS]:
            self.perform_single_child_test(
                child_state=state,
                expected_suite_state=Core.State.PASS)
            self.children = []

    @test
    def test_suite_state_mix_of_run_and_not_run(self):
        """Test that if the suite has children reporting a mix of not run and
        pass states the returned state is part run"""
        # Add a passing child first
        self.add_child(Core.State.PASS)

        self.perform_single_child_test(
            child_state=Core.State.NOT_RUN,
            expected_suite_state=Core.State.PART_RUN)


if __name__ == "__main__":
    runModule()
