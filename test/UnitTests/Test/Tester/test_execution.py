#!/usr/bin/env python
"""Test the classes defined in Execution.py"""

import copy
import mock
import sys

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester import failUnless
from CleverSheep.Test.Tester import failUnlessEqual

from CleverSheep.Test.Tester.Execution import Parser
from CleverSheep.Test.Tester.Execution import ParserManager


class Namespace:
    """A simple class used to mimic a namespace"""

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class ParserTestSuite(Suite):
    """Test the extended argument parser behaviour"""

    def setUp(self):
        """Create a parser to be used in the tests with some arguments"""
        self.parser = Parser()
        self.parser.add_argument(
            "--test-arg",
            action="store",
            help="A dummy argument used for testing")
        self.parser.add_argument(
            "--number-arg",
            action="store",
            type=int,
            help="A dummy argument used for testing")

    @test
    @mock.patch('argparse.ArgumentParser.parse_known_args')
    def test_has_arg_valid_arg(self, mock_parse_known_args):
        """Test has_arg returns True for a valid arg"""
        mock_parse_known_args.return_value = Namespace(
            test_arg=True), Namespace()
        self.parser.parse_known_args(args=["--test-arg"])
        failUnless(self.parser.has_arg("test_arg"))

    @test
    @mock.patch('argparse.ArgumentParser.parse_known_args')
    def test_has_arg_invalid_arg(self, mock_parse_known_args):
        """Test has_arg returns False for an unknown arg"""
        mock_parse_known_args.return_value = Namespace(), Namespace(
            unknown_arg=True)
        self.parser.parse_known_args(args=["--test-arg"])
        failUnless(self.parser.has_arg("unknown_arg") is False)

    @test
    @mock.patch('argparse.ArgumentParser.parse_known_args')
    def test_get_arg(self, mock_parse_known_args):
        """Test get_arg_value returns the value of an arg"""
        mock_parse_known_args.return_value = Namespace(
            number_arg=17), Namespace()
        self.parser.parse_known_args(args=["--number-arg", 17])
        failUnlessEqual(17, self.parser.get_arg_value("number_arg"))


class ParserManagerTestSuite(Suite):
    """Test the parser manager behaviour"""

    def suiteSetUp(self):
        """Store the argv so it can be restored between tests"""
        self.argv = copy.copy(sys.argv)

    def setUp(self):
        """Create a parser to be used in the tests with some arguments"""
        self.parser_manager = ParserManager()
        sys.argv = copy.copy(self.argv)

    @test
    def test_has_arg_main_parser(self):
        """Test that has arg returns True for a main parser argument"""
        self.parser_manager.parse_main_arguments()
        failUnless(self.parser_manager.has_arg("summary"))

    @test
    def test_has_arg_user_parser(self):
        """Test that has arg returns True for a user parser argument"""
        self.parser_manager.add_user_argument(
            "--user-arg",
            action="store_true",
            help="A user arg for testing")
        self.parser_manager.parse_user_arguments()
        failUnless(self.parser_manager.has_arg("user_arg"))

    @test
    def test_has_arg_returns_false(self):
        """Test that has arg returns False for an argument not on any of the
        parsers"""
        failUnless(self.parser_manager.has_arg("bad_arg") is False)

    @test
    def test_get_args(self):
        """Check a selection of the expected args are returned by get_args"""
        # Arrange
        self.parser_manager.add_user_argument(
            "--user-arg",
            action="store_true",
            help="A user arg for testing")
        self.parser_manager.parse_main_arguments()
        self.parser_manager.parse_user_arguments()

        # Act
        args = self.parser_manager.get_args()

        # Assert
        #failUnless('log_file' in args)
        failUnless('summary' in args)
        failUnless('run_broken' in args)
        failUnless('user_arg' in args)

    @test
    def test_get_arg_value_main(self):
        """Get an arg value that should have been handled by the main parser
         and check it is correct"""
        # Arrange
        sys.argv.append("--keep-going")
        self.parser_manager.parse_main_arguments()

        # Act
        keep_going = self.parser_manager.get_arg_value("keep_going")

        # Assert
        failUnlessEqual(True, keep_going)

    @test
    def test_get_arg_value_user(self):
        """Get an arg value that should have been handled by the user parser
         and check it is correct"""
        # Arrange
        self.parser_manager.add_user_argument(
            "--user-arg",
            action="store_true",
            help="A user arg for testing")
        sys.argv.append("--user-arg")
        self.parser_manager.parse_user_arguments()

        # Act
        user_arg = self.parser_manager.get_arg_value("user_arg")

        # Assert
        failUnlessEqual(True, user_arg)

    @test
    def test_user_args(self):
        """Test getting the user defined argument values"""
        # Arrange
        self.parser_manager.add_user_argument(
            "--user-arg",
            action="store_true",
            help="A user arg for testing")
        self.parser_manager.add_user_argument(
            "--second_user-arg",
            action="store_true",
            help="A user arg for testing")
        sys.argv.append("--user-arg")
        self.parser_manager.parse_user_arguments()

        # Act
        user_args = self.parser_manager.get_user_arguments()

        # Assert
        failUnless(user_args.user_arg)
        failUnless(user_args.second_user_arg is False)

    @test
    def test_get_unhandled_args(self):
        """Test getting the unhandled arguments"""
        # Arrange
        unhandled_arg = "77-100"
        sys.argv.append(unhandled_arg)
        self.parser_manager.parse_main_arguments()
        self.parser_manager.parse_user_arguments()

        # Act
        unhandled_args = self.parser_manager.get_unhandled_args()

        # Assert
        failUnless(unhandled_arg in unhandled_args)


if __name__ == "__main__":
    runModule()
