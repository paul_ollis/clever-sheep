#!/usr/bin/env python
"""Test the classes defined in ReportMan.py"""

import logging

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester.ReportMan import ReportManager

import mock


class ReportManagerTestSuite(Suite):
    """Test the Report Manager behaviour"""

    def __init__(self):
        """Define instance variables"""
        self.report_manager = None

    def setUp(self):
        """Create a report manager instance"""
        self.report_manager = ReportManager()

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_debug(self, coordinator_mock):
        """Check the behaviour for a valid debug call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "DEBUG"
        self.report_manager.debug(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.debug.assert_called_with('%s', print_str)
        mock_logging.debug.assert_called_with('%s', print_str)

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_info(self, coordinator_mock):
        """Check the behaviour for a valid info call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "INFO"
        self.report_manager.info(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.info.assert_called_with('%s', print_str)
        mock_logging.info.assert_called_with('%s', print_str)

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_warn(self, coordinator_mock):
        """Check the behaviour for a valid warn call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "WARN"
        self.report_manager.warn(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.warn.assert_called_with('%s', print_str)
        mock_logging.warn.assert_called_with('%s', print_str)

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_warning(self, coordinator_mock):
        """Check the behaviour for a valid warning call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "WARNING"
        self.report_manager.warning(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.warning.assert_called_with('%s', print_str)
        mock_logging.warning.assert_called_with('%s', print_str)

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_error(self, coordinator_mock):
        """Check the behaviour for a valid error call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "ERROR"
        self.report_manager.error(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.error.assert_called_with('%s', print_str)
        mock_logging.error.assert_called_with('%s', print_str)

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_critical(self, coordinator_mock):
        """Check the behaviour for a valid critical call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "CRITICAL"
        self.report_manager.critical(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.critical.assert_called_with('%s', print_str)
        mock_logging.critical.assert_called_with('%s', print_str)

    @test
    @mock.patch("CleverSheep.Test.Tester.ReportMan.Coordinator")
    def test_fatal(self, coordinator_mock):
        """Check the behaviour for a valid fatal call"""
        # Given
        mock_reporter = mock.MagicMock()
        mock_logging = mock.MagicMock()

        coordinator_mock.getServiceProvider.side_effect = [mock_reporter,
                                                           mock_logging]

        # When
        print_str = "FATAL"
        self.report_manager.fatal(print_str)

        # Then
        calls = [mock.call("reporter"), mock.call("logging")]
        coordinator_mock.getServiceProvider.assert_has_calls(calls)

        mock_reporter.fatal.assert_called_with('%s', print_str)
        mock_logging.fatal.assert_called_with('%s', print_str)


if __name__ == "__main__":
    runModule()