#!/usr/bin/env python
"""test_pipe_conn.py"""
import os
import mock
import struct
from CleverSheep.Test.Tester import *
from CleverSheep.Test.Mock.Comms import PipeConn


class BasePipeConnTestSuite(Suite):
    """Base class for Pipe Conn test suites which contains common functions"""

    def __init__(self):
        """Setup the mocks used in these tests"""
        self.pipe_path = "/tmp/TESTPIPE"
        fd = open(self.pipe_path, "w")
        fd.close()
        self.pipe = None
        self.file_no = None
        self.poll_manager_mock = mock.MagicMock()
        self.on_receive_mock = mock.MagicMock()
        self.on_close_mock = mock.MagicMock()
        self.on_error_mock = mock.MagicMock()

    def setUp(self):
        """Reset the mocks ready for the test"""
        self.poll_manager_mock.reset()
        self.on_receive_mock.reset()
        self.on_close_mock.reset()
        self.on_error_mock.reset()


class ReadPipeConnTestSuite(BasePipeConnTestSuite):
    """Tests for the Comms.py PipeConn where it is reading from a pipe"""

    def setUp(self):
        """Open a pipe ready for writing to"""
        fd = os.open(self.pipe_path, os.O_WRONLY | os.O_NDELAY)
        self.pipe = os.fdopen(fd, "w")
        self.file_no = self.pipe.fileno()

    def tearDown(self):
        """Close the pipe"""
        self.pipe.close()

    def write_data_to_pipe(self, data):
        """Write some data to the input pipe"""
        sent = os.write(self.file_no, data)
        failUnlessEqual(sent, len(data), "Failed to send all data")

    @test
    def test_initialization(self):
        """Test that a read pipe connection can be created"""

        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        self.poll_manager_mock.addInputCallback.assert_called_once_with(
            pipe_conn.pipeF, pipe_conn._onInputActivity)

    @test
    def test_read(self):
        """Test the read function works as expected"""
        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        test_msg = "TestMessage".encode('latin-1')
        self.write_data_to_pipe(test_msg)
        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        pipe_conn._onInputActivity(None, None)

        data = pipe_conn.read()

        failUnlessEqual(test_msg, data)
        failUnlessEqual("", pipe_conn.inBuf)

    @test
    def test_peek(self):
        """Test the peek function works as expected"""
        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        test_msg = "PeekMessage".encode('latin-1')
        self.write_data_to_pipe(test_msg)
        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        pipe_conn._onInputActivity(None, None)

        data = pipe_conn.peek()

        failUnlessEqual(test_msg, data)
        failUnlessEqual(test_msg, pipe_conn.inBuf)

    @test
    def test_read_msg_empty_buf(self):
        """Test the readMsg function when the buffer is empty"""
        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)
        pipe_conn.inBuf = ""

        message = pipe_conn.readMsg()

        failUnlessEqual(None, message)

    @test
    def test_read_msg_incomplete_message(self):
        """Test the read message function when the buffer contains an
        incomplete message"""
        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        self.write_data_to_pipe(struct.pack("=L", 4) + "hom".encode('latin-1'))
        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        pipe_conn._onInputActivity(None, None)

        message = pipe_conn.readMsg()

        failUnlessEqual(None, message)

    @test
    def test_read_msg_success_native(self):
        """Test successfully reading a message from the buffer using native
        byte order
        """
        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=True)

        test_msg = struct.pack("=L", 8) + "Complete".encode('latin-1')
        self.write_data_to_pipe(test_msg)
        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        pipe_conn._onInputActivity(None, None)

        message = pipe_conn.readMsg()

        failUnlessEqual(test_msg, message)

    @test
    def test_read_msg_success_not_native(self):
        """Test successfully reading a message from the buffer when not using
        native byte order
        """
        pipe_conn = PipeConn(
            False, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        test_msg = struct.pack("!L", 8) + "Complete".encode('latin-1')
        self.write_data_to_pipe(test_msg)
        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        pipe_conn._onInputActivity(None, None)

        message = pipe_conn.readMsg()

        failUnlessEqual(test_msg, message)


class WritePipeConnTestSuite(BasePipeConnTestSuite):
    """Tests for the Comms.py PipeConn where it is writing to a pipe"""

    def setUp(self):
        """Open a pipe ready for reading from to"""
        fd = os.open(self.pipe_path, os.O_RDONLY | os.O_NDELAY)
        self.pipe = os.fdopen(fd, "r")
        self.file_no = self.pipe.fileno()

    def read_data_from_pipe(self, expected_data):
        """Read some data to the input pipe"""
        read = os.read(self.file_no, len(expected_data))
        failUnlessEqual(len(expected_data),
                        len(read),
                        "Failed to read all data")
        failUnlessEqual(expected_data, read)

    def read_packed_data_from_pipe(self, expected_data, native=False):
        """Read some data to the input pipe which has a header and has been
        packed"""
        if native:
            pack_method = "=L"
        else:
            pack_method = "!L"

        hdr_len = 4
        read = os.read(self.file_no, len(expected_data) + hdr_len)
        (msgLen,) = struct.unpack(pack_method, read[:hdr_len])
        failUnlessEqual(len(expected_data),
                        msgLen,
                        "Failed to read all data")
        failUnlessEqual(expected_data, read[hdr_len:msgLen+hdr_len])

    @test
    def test_initialization(self):
        """Test that a write pipe connection can be created"""
        PipeConn(True, self.pipe_path, "TestPeer", self.poll_manager_mock,
                 onReceive=self.on_receive_mock, onClose=self.on_close_mock,
                 onError=self.on_error_mock, native=False)

    @test
    def test_send(self):
        """Test the send function behaves as expected"""
        pipe_conn = PipeConn(
            True, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        test_msg = "testMsg".encode('latin-1')
        pipe_conn.send(test_msg)

        self.read_data_from_pipe(test_msg)

    @test
    def test_send_msg_with_header(self):
        """Test the send function behaves as expected"""
        pipe_conn = PipeConn(
            True, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False, addMsgLengthOnSend=True)

        test_msg = "testMsg2".encode('latin-1')
        pipe_conn.send(test_msg)

        self.read_packed_data_from_pipe(test_msg)

    @test
    def test_send_msg_with_header_native(self):
        """Test the send function behaves as expected"""
        pipe_conn = PipeConn(
            True, self.pipe_path, "TestPeer", self.poll_manager_mock,
            onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=True, addMsgLengthOnSend=True)

        test_msg = "testMsg3".encode('latin-1')
        pipe_conn.send(test_msg)

        self.read_packed_data_from_pipe(test_msg, native=True)
