#!/usr/bin/env python
"""test_connecter.py"""
import os
import mock
import struct
import socket
from CleverSheep.Test.Tester import *
from CleverSheep.Test.Mock.Comms import Connecter


class BasePipeConnTestSuite(Suite):
    """Tests for the Connecter class"""

    def __init__(self):
        super(BasePipeConnTestSuite, self).__init__()

        self.addr = ("127.0.0.1", 8000)

    @test
    def test_valid_connection(self):
        """Test that when a valid connection occurs the connect call back
        is called"""
        test_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        test_sock.bind(self.addr)
        test_sock.listen(5)

        mock_poll_manager = mock.MagicMock()
        mock_on_connect_cb = mock.MagicMock()
        mock_on_error_cb = mock.MagicMock()

        conn = Connecter(mock_poll_manager,
                         "test",
                         self.addr,
                         (0.0, 0.5),
                         onConnect=mock_on_connect_cb,
                         onError=mock_on_error_cb)
        mock_poll_manager.addTimeout.assert_called()
        mock_poll_manager.addRepeatingTimeout.assert_called()

        i = 0
        # Replicate the behaviour of the poll manager
        while i < 5 and len(mock_on_connect_cb.mock_calls) < 1:
            conn.onTryconnect()
            self.control.delay(0.1)
            i += 1

        test_sock.close()

        mock_poll_manager.removeTimeout.assert_called()

        # There is some weirdness stopping the assert_called mock functions
        # working, I assume due to the callback being wrapped in a CallbackRef
        # as such we're checking the call count this way
        failUnlessEqual(1, len(mock_on_connect_cb.mock_calls))
        failUnlessEqual(0, len(mock_on_error_cb.mock_calls))

    @test
    def test_failed_connection(self):
        """Test that on a failed connection the error callback is called"""
        mock_poll_manager = mock.MagicMock()
        mock_on_connect_cb = mock.MagicMock()
        mock_on_error_cb = mock.MagicMock()

        conn = Connecter(mock_poll_manager,
                         "test",
                         self.addr,
                         (0.0, 0.5),
                         onConnect=mock_on_connect_cb,
                         onError=mock_on_error_cb)
        mock_poll_manager.addTimeout.assert_called()
        mock_poll_manager.addRepeatingTimeout.assert_called()

        i = 0
        # Replicate the behaviour of the poll manager, note may take more than
        # 5 calls to get the 5 errors as sometimes the reported error is
        # EINPROGRESS
        while i < 15 and len(mock_on_error_cb.mock_calls) < 1:
            conn.onTryconnect()
            self.control.delay(0.1)
            i += 1

        mock_poll_manager.removeTimeout.assert_called()

        # There is some weirdness stopping the assert_called mock functions
        # working, I assume due to the callback being wrapped in a CallbackRef
        # as such we're checking the call count this way
        failUnlessEqual(0, len(mock_on_connect_cb.mock_calls))
        failUnlessEqual(1, len(mock_on_error_cb.mock_calls))

    @test
    def test_connection_after_failures(self):
        """Test connecting after a number of failed attempts"""
        mock_poll_manager = mock.MagicMock()
        mock_on_connect_cb = mock.MagicMock()
        mock_on_error_cb = mock.MagicMock()

        conn = Connecter(mock_poll_manager,
                         "test",
                         self.addr,
                         (0.0, 0.5),
                         onConnect=mock_on_connect_cb,
                         onError=mock_on_error_cb)
        mock_poll_manager.addTimeout.assert_called()
        mock_poll_manager.addRepeatingTimeout.assert_called()

        i = 0
        # Replicate the behaviour of the poll manager, allow a number of
        # failures to occur
        while i < 4 and len(mock_on_error_cb.mock_calls) < 1:
            conn.onTryconnect()
            self.control.delay(0.1)
            i += 1

        # Now start a connection to replicate a slow process coming up
        test_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        test_sock.bind(self.addr)
        test_sock.listen(5)

        i = 0
        # Replicate the behaviour of the poll manager - check we can now connect
        while i < 5 and len(mock_on_connect_cb.mock_calls) < 1:
            conn.onTryconnect()
            self.control.delay(0.1)
            i += 1

        test_sock.close()

        mock_poll_manager.removeTimeout.assert_called()

        # There is some weirdness stopping the assert_called mock functions
        # working, I assume due to the callback being wrapped in a CallbackRef
        # as such we're checking the call count this way
        failUnlessEqual(1, len(mock_on_connect_cb.mock_calls))
        failUnlessEqual(0, len(mock_on_error_cb.mock_calls))