# Environment setup

 - Before running the unit tests you will need to install the python development packages using apt/yum or similar.
 - 'pip install twisted==16.6.0' v17 seems to cause issues.
 - 'pip install service_identity'
 - 'pip install tornado'
 - 'pip install mock'

Note do not build and install the CleverSheep package itself.

# Running the tests

To run the tests simply invoke the all_tests.py file in this folder

# Test folders

In all the test folders you will find a `CheckEnv.py` file which adds the source
CleverSheep folder and any others required to the path.

## LegacyTests
The tests in this folder and `Tester` are legacy tests which are a mixture of
what one might consider unit tests and more functional tests that cover large
chunks of the library as a whole. They tend to be broken down by the library
file they are trying to test. The doc strings and test names can be somewhat
lacking, as can the way the tests work.
 
Eventually the goal will be to move any unit tests out of this folder into the
unit test folder, this will be done as and when new tests for the module need
writing and the test files would be being touched anyway.

## RegressionTests
These are tests that reproduce bugs in the library that have been fixed,
their purpose is to make sure the bug is not reintroduced.

## UnitTests
The tests in `UnitTests` are true unit tests that cover small chunks of
functionality in the standard way. The folder structure under the `UnitTests`
folder mirrors that of the library.
