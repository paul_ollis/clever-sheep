#!/usr/bin/env python
"""Sphinx format documentation.

The CleverSheep test framework can generate Sphinx (reStructuredText) format
files from the docstrings and special comments.

"""


import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker, ImpUtils
from CleverSheep.Test.Tester import *

import support


# TODO: Common code with test_Execution.py
class Common(support.TestRunner):
    def setUp(self):
        super(Common, self).setUp()
        os.chdir("query")
        # Change to the mock tests directory.
        self.d = {"base": os.path.abspath(os.path.dirname(__file__))}
        self.info = {
            "base": os.path.dirname(os.path.abspath(__file__)),
            "cwd": os.getcwd(),
        }
        self.cleanUp()


class Test_sphinx(Common):
    """Working with single modules.

    The --sphinx option works with a single module file. Given a test module
    called 'test_a.py' and option '--sphinx=spec' then the output will be in a
    file called 'spec/test_a.rst'.

    """
    @test
    def usesDocstrings(self):
        """Basic test outline is produced from docstrings.

        The module, class and method docstrings are used to create the basic
        spec from a test sript.

        """
        support.makePySingleTestFile("test.py", text='''
        | def a(self):
        |     """The test"""
        ''')
        support.run_test("test.py", "--sphinx=spec")

        expected_lines = DataMaker.literalText2Text('''
        | ===========
        | Suite: test
        | ===========
        |
        | Testing the tester.
        |
        |
        | Suite: SuiteA
        | =============
        |
        | Suite A
        |
        |
        | Test: a
        | -------
        |
        | :class: SuiteA
        | :method: a
        |
        | The test
        |
        ''')

        actual_lines = open("spec/test.rst").read()
        sanitized_actual_lines = []

        # Note we need to remove the :file: line from the check as it will change depending on where the checkout is
        for line in actual_lines.splitlines():
            if not ":file:" in line:
                sanitized_actual_lines.append(line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def stepsInTestMethodAreUsed(self):
        """If the test contains step comments they are included as the
        procedure.

        """
        support.makePySingleTestFile("test.py", text='''
        | def a(self):
        |     """The test"""
        |     #> Do first thing
        |     pass
        |
        |     #> Do second thing
        |     pass
        ''')
        support.run_test("test.py", "--sphinx=spec")

        expected_lines = DataMaker.literalText2Text('''
        | ===========
        | Suite: test
        | ===========
        |
        | Testing the tester.
        |
        |
        | Suite: SuiteA
        | =============
        |
        | Suite A
        |
        |
        | Test: a
        | -------
        |
        | :class: SuiteA
        | :method: a
        |
        | The test
        |
        |
        | .. rubric:: Procedure
        |
        | 1. Do first thing
        |
        | 2. Do second thing
        ''')

        actual_lines = open("spec/test.rst").read()
        sanitized_actual_lines = []

        # Note we need to remove the :file: line from the check as it will change depending on where the checkout is
        for line in actual_lines.splitlines():
            if not ":file:" in line:
                sanitized_actual_lines.append(line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def stepsInTestMethodAreUsedAlternativeFormat(self):
        """If the test contains step comments they are included as the
        procedure.

        """
        support.makePySingleTestFile("test.py", text='''
            | def a(self):
            |     """The test"""
            |     # > Do first thing
            |     pass
            |
            |     # > Do second thing
            |     pass
            ''')
        support.run_test("test.py", "--sphinx=spec")

        expected_lines = DataMaker.literalText2Text('''
            | ===========
            | Suite: test
            | ===========
            |
            | Testing the tester.
            |
            |
            | Suite: SuiteA
            | =============
            |
            | Suite A
            |
            |
            | Test: a
            | -------
            |
            | :class: SuiteA
            | :method: a
            |
            | The test
            |
            |
            | .. rubric:: Procedure
            |
            | 1. Do first thing
            |
            | 2. Do second thing
            ''')

        actual_lines = open("spec/test.rst").read()
        sanitized_actual_lines = []

        # Note we need to remove the :file: line from the check as it will change depending on where the checkout is
        for line in actual_lines.splitlines():
            if not ":file:" in line:
                sanitized_actual_lines.append(line)

        failUnlessEqualStrings(expected_lines,
                               "\n".join(sanitized_actual_lines))

    @test
    def stepsInImmediateCalledCodeAreIncluded(self):
        """Step comments in immediately called code are included.

        These are included as sub-steps.

        """
        support.makePySingleTestFile("test.py", text='''
        | def a(self):
        |     """The test"""
        |     #> Do first thing
        |     self.helper()
        |
        |     #> Do second thing
        |     pass
        |
        | def helper(self):
        |     #> Sub 1
        |     pass
        |
        |     #> Sub the second
        |     pass
        ''')
        support.run_test("test.py", "--sphinx=spec")

        expected_lines = DataMaker.literalText2Text('''
        | ===========
        | Suite: test
        | ===========
        |
        | Testing the tester.
        |
        |
        | Suite: SuiteA
        | =============
        |
        | Suite A
        |
        |
        | Test: a
        | -------
        |
        | :class: SuiteA
        | :method: a
        |
        | The test
        |
        |
        | .. rubric:: Procedure
        |
        | 1. Do first thing
        |
        |    1.1. Sub 1
        |
        |    1.2. Sub the second
        |
        | 2. Do second thing
        ''')

        actual_lines = open("spec/test.rst").read()
        sanitized_actual_lines = []

        # Note we need to remove the :file: line from the check as it will change depending on where the checkout is
        for line in actual_lines.splitlines():
            if not ":file:" in line:
                sanitized_actual_lines.append(line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def calledCodeStepsCanBePromoted(self):
        """Using the cs_inline decorator prevents called steps being nested.

        """
        support.makePySingleTestFile("test.py", text='''
        | def a(self):
        |     """The test"""
        |     #> Do first thing
        |     self.helper()
        |
        |     #> Do second thing
        |     pass
        |
        | @cs_inline
        | def helper(self):
        |     #> Sub 1
        |     pass
        |
        |     #> Sub the second
        |     pass
        ''')
        support.run_test("test.py", "--sphinx=spec")

        expected_lines = DataMaker.literalText2Text('''
        | ===========
        | Suite: test
        | ===========
        |
        | Testing the tester.
        |
        |
        | Suite: SuiteA
        | =============
        |
        | Suite A
        |
        |
        | Test: a
        | -------
        |
        | :class: SuiteA
        | :method: a
        |
        | The test
        |
        |
        | .. rubric:: Procedure
        |
        | 1. Do first thing
        |
        | 2. Sub 1
        |
        | 3. Sub the second
        |
        | 4. Do second thing
        ''')

        actual_lines = open("spec/test.rst").read()
        sanitized_actual_lines = []

        # Note we need to remove the :file: line from the check as it will change depending on where the checkout is
        for line in actual_lines.splitlines():
            if not ":file:" in line:
                sanitized_actual_lines.append(line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def zeroStepIsSuppressed(self):
        """Immediately calling a sub-step implies inlining.

        """
        support.makePySingleTestFile("test.py", text='''
        | def a(self):
        |     """The test"""
        |     self.helper()
        |
        |     #> Do second thing
        |     pass
        |
        | @cs_inline
        | def helper(self):
        |     #> Sub 1
        |     pass
        |
        |     #> Sub the second
        |     pass
        ''')
        support.run_test("test.py", "--sphinx=spec")

        expected_lines = DataMaker.literalText2Text('''
        | ===========
        | Suite: test
        | ===========
        |
        | Testing the tester.
        |
        |
        | Suite: SuiteA
        | =============
        |
        | Suite A
        |
        |
        | Test: a
        | -------
        |
        | :class: SuiteA
        | :method: a
        |
        | The test
        |
        |
        | .. rubric:: Procedure
        |
        | 1. Sub 1
        |
        | 2. Sub the second
        |
        | 3. Do second thing
        ''')

        actual_lines = open("spec/test.rst").read()
        sanitized_actual_lines = []

        # Note we need to remove the :file: line from the check as it will change depending on where the checkout is
        for line in actual_lines.splitlines():
            if not ":file:" in line:
                sanitized_actual_lines.append(line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))


if __name__ == "__main__":
    runModule()

