#!/usr/bin/env python
"""A test demonstrating some special import behaviour.

"""
from __future__ import print_function

from CleverSheep.Test.Tester import *

import util

util.fred()

import package.main
package.main.showName()
print("TEST", util.name)


if __name__ == "__main__":
    runModule()
