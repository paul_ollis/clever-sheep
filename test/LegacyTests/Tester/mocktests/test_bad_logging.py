#!/usr/bin/env python
"""A test demonstrating graceful handling of bad attempts to log.

"""

import logging

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


class TestIt(Suite):
    """This is a test suite"""

    @test
    def log_a_bit(self):
        """A test"""
        log.debug("Format but missing args %d %d", 1)


if __name__ == "__main__":
    runModule()
