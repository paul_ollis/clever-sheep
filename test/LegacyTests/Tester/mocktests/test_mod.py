#!/usr/bin/env python
"""A simple test script for module ``mod``.

This is not real test code. It is an example test script that is
run as part of the CleverSheep tests, in order to test the Tester
part of CleverSheep.
"""
from __future__ import print_function

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
module_under_test = "mod.py"
import mod

#modules_under_test = ["../../../CleverSheep/Test/Tester/Execution.py"]

from CleverSheep.Test import Cov

import sys

class TestIt(Suite):
    """This is a test suite"""

    @test
    def aaa(self):
        """Try addition of 2 and 3"""
        failUnlessEqual(5, mod.add(2, 3))

    @test
    def bbb(self):
        """Try addition of 2 and 3"""
        failUnlessEqual(5, mod.add(2, 3))


if __name__ == "__main__":
    runModule()
