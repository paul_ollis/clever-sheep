#!/usr/bin/env python
"""Example script to test the reporting hooks.

This checks detailed information.

"""


import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


class Reporter(object):
    def __init__(self):
        self.f = open("report.log", "w")

    def enter_suite(self, has_tests, title, doc_lines):
        self.f.write("Enter suite has_tests=%s\n" % has_tests)
        for l in doc_lines:
            self.f.write("D:%s\n" % (l))

    def leave_suite(self, has_tests, state):
        self.f.write("Leave suite\n")

    def start_test(self, number, title, doc_lines):
        self.f.write("Enter test %s\n" % (number))
        for l in doc_lines:
            self.f.write("T:%s\n" % (l))

    def end_test(self, number, title, doc_lines, state, error):
        self.f.write("Leave test\n")

    def finish(self):
        self.f.write("DONE\n")
        self.f.close()


Tester.registerReporter(Reporter())


class A(Suite):
    """The A suite

    With details provided.

    """
    @test
    def a(self):
        """The (a) test."""

    @test
    def b(self):
        """The (b) test."""


class B(Suite):
    """The B suite

    With details provided.

    """
    @test
    def c(self):
        """The (c) test."""


if __name__ == "__main__":
    runModule()




