#!/usr/bin/env python
"""A test demonstrating the reporter plug-in feature.

"""

import CheckEnv
from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester

class Reporter(Tester.Reporter):
    def __init__(self):
        self.f = open("report.txt", "w")

    def leave_suite(self, has_tests, state):
        self.f.write("Leave suite\n")
        self.f.write("Result = %s\n" % state)

    def end_test(self, number, title, doc_lines, state, error):
        self.f.write("Leave: %s\n" % title)
        self.f.write("Result = %s\n" % state)
        if error is not None:
            self.f.write("Details = %s\n" % error)

    def finish(self):
        self.f.close()
        

Tester.registerReporter(Reporter())

class TestIt(Suite):
    """This is a test suite"""

    @test
    def aaa(self):
        """The first test."""

    @test
    def bbb(self):
        """The second test."""
        fail("Oops")


if __name__ == "__main__":
    runModule()
