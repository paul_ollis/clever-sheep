#!/usr/bin/env python
"""Some tests to exercise the watchdog feature.

"""


import sys
sys.stdout.flush()
import time

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


Tester.add_option("--fail-at", action="append", type="int", default=[],
    metavar="T", help="Fail at test T")
Tester.add_option("--fail-setup", action="store_true",
    help="Fail setup for first suite")
Tester.add_option("--fail-teardown", action="store_true",
    help="Fail teardown for first suite")


class Base(Suite):
    def common(self):
        info = Tester.currentTestInfo()
        if info.cs_test_num in Tester.userOptions.fail_at:
            fail("Selected test")


class A(Base):
    """A"""
    def __init__(self):
        if "longload" in sys.argv:
            for i in range(5):
                n = time.sleep(1.0)
                sys.stdout.flush()

    def setUp(self):
        if Tester.userOptions.fail_setup:
            fail("Fail setup")

    def tearDown(self):
        if Tester.userOptions.fail_teardown:
            fail("Fail teardown")

    @test
    def a(self):
        """A-a"""
        self.common()

    @test
    def b(self):
        """A-b"""
        self.common()

    @test
    def c(self):
        """A-c"""
        self.common()


class B(Base):
    """B"""
    @test
    def a(self):
        """B-a"""
        self.common()

    @test
    def b(self):
        """B-b"""
        self.common()

    @test
    def c(self):
        """B-c"""
        self.common()


if __name__ == "__main__":
    runModule()
