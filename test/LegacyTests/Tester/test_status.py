#!/usr/bin/env python
"""Tests for the status line behaviour of Tester.

Currently these tests only verify that some of the monkey patched functions and
classes behave as intended.

What these tests **do not** do is verify that the status line is correctly
updated.

"""

import CheckEnv

import subprocess

from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *
from CleverSheep.Test.Tester import SubProcess


def getoutput(cmd):
    try:
        return subprocess.check_output(
            cmd, shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        return ""


def getstatusoutput(cmd):
    try:
        return 0, subprocess.check_output(
            cmd, shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as err:
        return err.returncode, err.output


class Monkeyed(Suite):
    """Verify behaviour of monkey patched functions."""
    def setUp(self):
        SubProcess.egregiouslyMonkeyPatch()

    def tearDown(self):
        SubProcess.unPatch()

    @test
    def bb(self):
        """Reading stdin"""
        proc = subprocess.Popen("cat -; echo bang >&2; sleep 0.11", shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                message="Subprocessing away!")
        a = proc.communicate("Line 1\nLine 99".encode("ascii"))
        proc.wait()
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | Line 1
        | Line 99
        | bang
        |
        """), "\n".join(a))

    @test
    def cc(self):
        """Write a lot"""
        input = "Hello paul\n" * 10000
        proc = subprocess.Popen("sleep 0.1; cat - >/dev/null", shell=True,
                stdin=subprocess.PIPE,
                message="Subprocessing away!")
        a = proc.communicate(input.encode("ascii"))
        proc.wait()
        failUnlessEqual((None, None), a)

    @test
    def dd(self):
        """Getoutput, with status"""
        stat, text = getstatusoutput(
                "sleep 0.1; echo hello; echo bang >&2")
        failUnlessEqual(0, stat)
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | hello
        | bang
        |
        """), text)

    @test
    def ee(self):
        """Getoutput"""
        text = getoutput("sleep 0.1; echo hello; echo bang >&2")
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | hello
        | bang
        |
        """), text)


if __name__ == "__main__":
    runModule()
