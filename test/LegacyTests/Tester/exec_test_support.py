"""Support for tests that involve execution of CleverSheep tests.

The easiest way to do some CleverSheep testing is to run actual test scripts.
Currently these all reside in the mocktests directory (which is a poor name).

"""

import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test.Tester import *

from support import run_test


class TestExecutor(Suite):
    """Base class for suites that need to run test scripts.

    """
    def cleanUp(self):
        Files.rmFile("subtest.log")
        Files.rmFile("report.txt")

    def setUp(self):
        # Change to the mock tests directory and remove detritus from any
        # previous run.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        #self.cleanUp()
        pass

    def getConsoleOutput(self):
        with open("subtest.log") as f:
            return f.read()
