"""Utilities to help with Tester testing.

"""

import re

rTime= re.compile(r'\d\d:\d\d:\d\d\.\d\d\d')
rDate= re.compile(r'\d\d/\d\d/\d\d\d\d')


class LogLine(object):
    def __init__(self, s):
        self.s = s.rstrip()
        self.a = rDate.sub(" " * 10, rTime.sub(" " * 12, self.s))

    def __getitem__(self, i):
        return self.a[i]

    def __len__(self):
        return len(self.s)

    def __lt__(self, rhs):
        return self.a < rhs.a

    def __gt__(self, rhs):
        return self.a > rhs.a

    def __eq__(self, rhs):
        return self.a == rhs.a

    def __hash__(self):
        return hash(self.a)

    def __str__(self):
        return self.s
