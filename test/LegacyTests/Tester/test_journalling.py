#!/usr/bin/env python
"""Exercise the test journalling feature.

CleverSheep's test framework can store a journal for all test runs.
The journal is used to track which tests have been executed, which have passed
or failed, plus the details each test run.

"""

import six

import os
import mmap

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker, ImpUtils
from CleverSheep.Test.Tester import *

import support


class Common(Suite):
    def cleanUp(self):
        #Files.rmFile("subtest.log")
        #Files.rmFile("temp.log")
        pass

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        self.cleanUp()


def twiddle(path, idx, offset=1):
    f = open(path, "rb+")
    f.seek(0, 2)
    v = mmap.mmap(f.fileno(), f.tell())
    if six.PY2:
        v[idx] = chr((ord(v[idx]) + offset) % 256)
    else:
        v[idx] = (v[idx] + offset) % 256
    v.flush()
    v.close()


class BasicJournal(Common):
    """Basic journaling behaviour.

    """
    @test
    def createNewJournal(self):
        """A journal is created if required.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin")
        support.run_test("test_multimode.py", enableJournal=True)
        failUnlessExists(".csdata/test_journal.bin")

        if 0:
            failUnlessEqualStrings(DataMaker.literalText2Text('''
            |
            '''), open("subtest.log").read())

    @test
    def noJournalingOfSummaryEtc(self):
        """A journal is not created for a summary, details, etc.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin", "Broken test")

        support.run_test("test_multimode.py", "--summary")
        failIfExists(".csdata/test_journal.bin")

        support.run_test("test_multimode.py", "--ids-summary")
        failIfExists(".csdata/test_journal.bin")

        support.run_test("test_multimode.py", "--details")
        failIfExists(".csdata/test_journal.bin")

        support.run_test("test_multimode.py", "--help")
        failIfExists(".csdata/test_journal.bin")

    @test
    def journalSupportResume(self):
        """The journal is used to figure out how to resume tests.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin")
        support.run_test("test_multimode.py", "--fail-at=2", exitCode=1)

        support.run_test("test_multimode.py", "--resume")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     2   : A-b...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        '''), open("subtest.log").read())


class ErrorHandling(Common):
    """Handling various journal error conditions.

    The journal format and code is designed to be extremely robust
    against corruption.

    """
    @test
    def corruptMagicHandled(self):
        """A corrupted journal magic causes re-sync.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin")
        support.run_test("test_multimode.py", "--fail-at=3", exitCode=1)
        twiddle(".csdata/test_journal.bin", 0)

        support.run_test("test_multimode.py", "--resume")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        '''), open("subtest.log").read())

    @test
    def corruptChecksum(self):
        """A corrupted checksum causes re-sync.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin")
        support.run_test("test_multimode.py", "--fail-at=3", exitCode=1)
        twiddle(".csdata/test_journal.bin", 4)

        support.run_test("test_multimode.py", "--resume")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        '''), open("subtest.log").read())

    @test
    def corruptLength2(self):
        """A corrupted length causes re-sync.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin")
        support.run_test("test_multimode.py", "--fail-at=3", exitCode=1)
        twiddle(".csdata/test_journal.bin", 22, 4)

        support.run_test("test_multimode.py", "--resume")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        '''), open("subtest.log").read())

    @test
    def corruptBody(self):
        """A corrupted body causes re-sync.

        """
        Files.rmFile(".csdata/test_journal.bin")
        failIfExists(".csdata/test_journal.bin")
        support.run_test("test_multimode.py", "--fail-at=3", exitCode=1)
        twiddle(".csdata/test_journal.bin", 24)

        support.run_test("test_multimode.py", "--resume")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        '''), open("subtest.log").read())


if __name__ == "__main__":
    runModule()
