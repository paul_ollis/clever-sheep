#!/usr/bin/env python
"""Test for the Tester module itself.

These tests target various features of the Tester module, which do not fit
neatly into any other category.

"""

import time

import CheckEnv

from CleverSheep.Test import Tester
from CleverSheep.Test.Tester import *

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Suites.py"
from CleverSheep.Test.Tester import Suites


class Test_Info(Suites.Suite):
    """Test accessing information during each phase."""
    def setUp(self):
        self.a = Tester.currentTestInfo()

    @test
    def info_during_test(self):
        """Accessing currentTestInfo.

        The ``Tester.currentTestInfo`` object should give details
        about the running test.

        """
        self.b = Tester.currentTestInfo()
        failUnless(self.a is self.b)


class Test_Info2(Suites.Suite):
    """More tests for test info access."""
    @test(xxx=1)
    def info_during_test(self):
        """Accessing currentTestInfo.

        The ``Tester.currentTestInfo`` object should give details
        about the running test.

        """
        info = Tester.currentTestInfo()
        failUnlessEqual(1, info.xxx)


if __name__ == "__main__":
    runModule()

