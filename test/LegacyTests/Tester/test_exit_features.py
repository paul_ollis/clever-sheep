#!/usr/bin/env python
"""Tests for the early exit features.

"""

import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *

import support


class TestEarlyExits(Suite):
    """Tests for early exit from tests."""

    def cleanUp(self):
        Files.rmFile("subtest.log")

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        pass

    def exit_all(self):
        """Verify we can exit all tests early, without failing.

        This is like `exit_suite`, but the first test invokes ``exit_all``.
        This means all following tests should be skipped.
        """
        support.run_test("test_exit_all.py", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | A test demonstrating the exit_all feature.
        |   This is a test suite
        |     1   : The first test....................................           EXIT_ALL
        |
        |
        | Summary of the failures
        | A test demonstrating the exit_all feature...................           PART_RUN
        |   This is a test suite......................................           PART_RUN
        |     1   : The first test....................................           EXIT_ALL
        |
        """), open("subtest.log").read())


if __name__ == "__main__":
    runModule()

