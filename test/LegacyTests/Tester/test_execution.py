#!/usr/bin/env python
"""Test for the Execution module in CleverSheep.Test.Tester.

This module provides support for running suites of tests. Specifically this
targets the Execution module, which handles the details of managing test runs.

"""
from __future__ import print_function


import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker, ImpUtils
from CleverSheep.Test.Tester import *

import support
from Tester_utils import LogLine

# The module under test.
from CleverSheep.Test.Tester import Execution


class Common(Suite):
    def cleanUp(self):
        #Files.rmFile("subtest.log")
        #Files.rmFile("temp.log")
        pass

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        self.cleanUp()

    def expectOutput(self, reprText, lineRanges=None):
        actual = open("subtest.log").read()
        if lineRanges is not None:
            lines = actual.splitlines()
            actual = []
            for a, b in lineRanges:
                actual.extend(lines[a:b])
            actual = "\n".join(actual)
        failUnlessEqualStrings(DataMaker.literalText2Text(reprText), actual)

    def expectLogOutput(self, data, reprText):
        failUnlessEqualStrings(DataMaker.literalText2Text(reprText), data.log,
                               lineWrapper=LogLine)


class Test_Hooks(Common):
    """Tests for the various Tester hooks.

    The framework provide various mechanisms to add hook functions, which get
    called a certain points before, during or after test execution.
    """
    # TODO: non-test.
    #@test
    def options_parsed(self):
        """Callback just after user options are parsed.

        """
        #support.run_test("test_many.py", "1", "4")
        self.expectOutput('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |
        ''')


class Test_AssertFunctions(Common):
    """Test that special assert function handling works.

    Assert functions are special in that they are trimmed from test failure
    tracebacks. The framework provides a number of built-in assert functions,
    but it is also possible to mark functions as assert functions.

    """
    @test
    def assertStopsTraceback(self):
        """Basic assert function traceback blocking.

        A test that fails using a built-in check function should not print
        details of the check function internals.

        """
        support.run_test("test_traceback1.py", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Dummy test module for testing failure tracebacks.
        |   A
        |     1   : a.................................................               FAIL
        |           Test failed
        |           Oops
        |           ./test_traceback1.py: b
        |              15  :
        |              16  :     def b(self):
        |              17  :         """b"""
        |           ==>18  :         fail("Oops")
        |              19  :
        |              20  :
        |              21  : if __name__ == "__main__":
        |           ./test_traceback1.py: a
        |              11  :     @test
        |              12  :     def a(self):
        |              13  :         """a"""
        |           ==>14  :         self.b()
        |              15  :
        |              16  :     def b(self):
        |              17  :         """b"""
        |
        |
        | Summary of the failures
        | Dummy test module for testing failure tracebacks............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : a.................................................               FAIL
        |
        '''), open("subtest.log").read())

    @test
    def checkerStopsTraceback(self):
        """Marking a function as an assertion blocks traceback from entering
        that function.

        """
        support.run_test("test_traceback2.py", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Dummy test module for testing failure tracebacks.
        |   A
        |     1   : a.................................................               FAIL
        |           Test failed
        |           Oops
        |           ./test_traceback2.py: a
        |              14  :     @test
        |              15  :     def a(self):
        |              16  :         """a"""
        |           ==>17  :         self.b()
        |              18  :
        |              19  :     @Tester.assertion
        |              20  :     def b(self):
        |
        |
        | Summary of the failures
        | Dummy test module for testing failure tracebacks............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : a.................................................               FAIL
        |
        '''), open("subtest.log").read())


class Test_ReporterHook(Common):
    """Exercise the custom reporter hook.

    The test framework provides a custom test reporting hook. The uses include
    such things as integration with continuous integration systems and hooking
    into a GUI front-end.

    """
    @test
    def basic_reporting(self):
        """All main phases can be reported.

        The hook should support suite enter/exit, test enter/exit and finish
        events.

        """
        support.run_test("test_report_hook1.py")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Enter suite Example script to test the reporting hooks.
        | Enter suite B
        | Enter test 1: a
        | Leave test
        | Leave suite
        | Leave suite
        | DONE
        |
        '''), open("report.log").read())

    @test
    def detailed_reporting(self):
        """Details for each suite and test are provided.

        The various hook methods are provided with useful information.

        """
        support.run_test("test_report_hook2.py")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Enter suite has_tests=None
        | D:Example script to test the reporting hooks.
        | D:
        | D:This checks detailed information.
        | Enter suite has_tests=True
        | D:The A suite
        | D:
        | D:With details provided.
        | Enter test 1
        | T:The (a) test.
        | T:
        | T:Also with details,
        | T:on more thn one line.
        | Leave test
        | Leave suite
        | Leave suite
        | DONE
        |
        '''), open("report.log").read())


class Test_Selection(Common):
    """Various ways to select tests.

    The framework provide various mechanisms to select which tests are run:

    - From the command line, using test numbers or patterns.
    - Tests can be temporarily disabled.

    """
    @test
    def selection_by_single_number(self):
        """Single numbers on the command line select tests.

        """
        support.run_test("test_many.py", "1", "4")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |
        '''), open("subtest.log").read())

    @test
    def line_range(self):
        """Line ranges are supported.

        """
        support.run_test("test_many.py", "1-2", "5-6")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     2   : A-b...............................................               PASS
        |   B
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        '''), open("subtest.log").read())

    @test
    def mixed_method(self):
        """Numbers and ranges can be mixed and order is not important.

        The framework always runs the tests in order, regardless of the selection
        order on the command line. Also duplicate numbers and overlapping ranges
        are handled.

        """
        support.run_test("test_many.py", "1", "5-6", "3", "1", "4-5")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        '''), open("subtest.log").read())

    @test
    def entire_suite_disabled(self):
        """When an entire suite is not selected, it does not appear.

        """
        support.run_test("test_many.py", "5-6")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   B
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        '''), open("subtest.log").read())

    @test
    def select_with_plain_strings(self):
        """Simple string select tests where the text appears in the summary text.

        """
        support.run_test("test_many.py", "a", "c")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Searching test summary for strings matching: ['a', 'c']
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        '''), open("subtest.log").read())


class Test_FailureModes(Common):
    """Various ways a test can fail, at a high level.

    More specifically, this suite verifies the way that the framework behaves
    in the face of different failure modes. Including:

    - Single test failure, stop on failure.
    - Multiple test failure, keep going on failure.
    - Test setup failure.
    - Test teardown failure.
    - Suite setup failure.
    - Suite teardown failure.

    """
    def xx_tpl(self):
        """Single numbers on the command line select tests.

        """
        support.run_test("test_many.py")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     2   : A-b...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        '''), open("subtest.log").read())

    @test
    def exitIndicatedTestFailure(self):
        """The program exit code should be non-zero if the test fails.

        TODO: This is for runModule, also need runTree.

        And, of course, zero if they all pass.

        """
        #> Do a successful run first and expect zero exit code.
        data = support.run_test("test_multimode.py", "-q")
        failUnlessEqual(0, data.exitCode)

        #> Now do a failing run and expect non-zero exit code.
        data = support.run_test("test_multimode.py", "-q", "--fail-at=2",
                exitCode=None)
        failUnlessEqual(1, data.exitCode)

    @test
    def multipleFailKeepGoing(self):
        """The keep-going option continues following normal failure.

        """
        support.run_test("test_multimode.py", "-q", "--fail-at=2", "--fail-at=4",
                "--keep-going", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     2   : A-b...............................................               FAIL
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               FAIL
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     2   : A-b...............................................               FAIL
        |   B.........................................................         CHILD_FAIL
        |     4   : B-a...............................................               FAIL
        |
        '''), open("subtest.log").read())

    # TODO: This used to be:
    # def testSetupFailKillsSuite(self):
    #     """A failure during test-setup disables all following tests in a suite.


class Test_IDs(Common):
    """The use of test IDs.

    """
    @test(testID="id-01")
    def assertStopsTraceback(self):
        """Basic assert function traceback blocking.

        A test that fails using a built-in check function should not print
        details of the check function internals.

        """
        support.run_test("test_id.py", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Dummy test module for testing failure tracebacks.
        |   A
        |     1   : [ID-1] a..........................................               FAIL
        |           Test failed
        |           Oops
        |           ./test_id.py: b
        |              15  :
        |              16  :     def b(self):
        |              17  :         """b"""
        |           ==>18  :         fail("Oops")
        |              19  :
        |              20  :
        |              21  : if __name__ == "__main__":
        |           ./test_id.py: a
        |              11  :     @test(testID="ID-1")
        |              12  :     def a(self):
        |              13  :         """a"""
        |           ==>14  :         self.b()
        |              15  :
        |              16  :     def b(self):
        |              17  :         """b"""
        |
        |
        | Summary of the failures
        | Dummy test module for testing failure tracebacks............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : [ID-1] a..........................................               FAIL
        |
        '''), open("subtest.log").read())


    @test
    def testSetupFailFailsTest(self):
        """A failure during test-setup makes the test fail.

        """
        support.run_test("test_multimode.py",
                         "-q",
                         "--fail-setup",
                         "--fail-at=4",
                         "--keep-going",
                         exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................          BAD_SETUP
        |     2   : A-b...............................................          BAD_SETUP
        |     3   : A-c...............................................          BAD_SETUP
        |   B
        |     4   : B-a...............................................               FAIL
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : A-a...............................................          BAD_SETUP
        |     2   : A-b...............................................          BAD_SETUP
        |     3   : A-c...............................................          BAD_SETUP
        |   B.........................................................         CHILD_FAIL
        |     4   : B-a...............................................               FAIL
        |
        '''), open("subtest.log").read())

    @test
    def postCheckFailsTest(self):
        """A postCheck method can fail an individual test.

        """
        support.run_test("test_multimode.py", "--fail-post", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               FAIL
        |           Test failed
        |           Fail post check
        |           ./test_multimode.py: postCheck
        |              39  :
        |              40  :     def postCheck(self):
        |              41  :         if Tester.userOptions.fail_postcheck:
        |           ==>42  :             fail("Fail post check")
        |              43  :
        |              44  :     @test
        |              45  :     def a(self):
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : A-a...............................................               FAIL
        '''), open("subtest.log").read())

    @test
    def postCheckNotRunAfterFail(self):
        """The post check is not run if the main part of the test fails.

        """
        support.run_test("test_multimode.py", "--fail-post", "--fail-at=1",
                exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               FAIL
        |           Test failed
        |           Selected test
        |           ./test_multimode.py: common
        |              24  :     def common(self):
        |              25  :         info = Tester.currentTestInfo()
        |              26  :         if info.cs_test_num in Tester.userOptions.fail_at:
        |           ==>27  :             fail("Selected test")
        |              28  :
        |              29  :
        |              30  : class A(Base):
        |           ./test_multimode.py: a
        |              44  :     @test
        |              45  :     def a(self):
        |              46  :         """A-a"""
        |           ==>47  :         self.common()
        |              48  :
        |              49  :     @test
        |              50  :     def b(self):
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : A-a...............................................               FAIL
        '''), open("subtest.log").read())

    @test
    def testTeardownContinues(self):
        """A failure during test-teardown allows the suite to continue.

        """
        support.run_test("test_multimode.py",
                         "-q",
                         "--fail-teardown",
                         "--fail-at=4",
                         "--keep-going",
                         exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     2   : A-b...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               FAIL
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     1   : A-a...............................................       BAD_TEARDOWN
        |     2   : A-b...............................................       BAD_TEARDOWN
        |     3   : A-c...............................................       BAD_TEARDOWN
        |   B.........................................................         CHILD_FAIL
        |     4   : B-a...............................................               FAIL
        |
        '''), open("subtest.log").read())


class Test_Resumption(Common):
    """Resuming after a failed test.

    """
    def setUp(self):
        super(Test_Resumption, self).setUp()
        Files.rmFile(os.path.join(os.getcwd(), ".csdata", "test_journal.bin"))

    @test
    def resumeFailingTest(self):
        """The resume option will continue from the previous failing test.

        """
        support.run_test("test_multimode.py", "-q", "--fail-at=2", exitCode=1,
                         enableJournal=True)
        print(open("subtest.log").read())

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     2   : A-b...............................................               FAIL
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     2   : A-b...............................................               FAIL
        |
        '''), open("subtest.log").read())

        #> Now a second run with the resume option should run the failing and
        #> subsequent tests.
        support.run_test("test_multimode.py", "-q", "--resume",
                         enableJournal=True)
        print(open("subtest.log").read())

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     2   : A-b...............................................               PASS
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        '''), open("subtest.log").read())

    @test
    def resumeFailingTest2(self):
        """The resume option skips previously passed tests.

        """
        support.run_test("test_multimode.py", "-q", "--fail-at=2",
                         "--fail-at=4", "--keep-going", exitCode=1,
                         enableJournal=True)
        print(open("subtest.log").read())

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     1   : A-a...............................................               PASS
        |     2   : A-b...............................................               FAIL
        |     3   : A-c...............................................               PASS
        |   B
        |     4   : B-a...............................................               FAIL
        |     5   : B-b...............................................               PASS
        |     6   : B-c...............................................               PASS
        |
        |
        | Summary of the failures
        | Multiple suites and tests for testing the Tester............         CHILD_FAIL
        |   A.........................................................         CHILD_FAIL
        |     2   : A-b...............................................               FAIL
        |   B.........................................................         CHILD_FAIL
        |     4   : B-a...............................................               FAIL
        '''), open("subtest.log").read())

        #> Now a second run with the resume option should run the failing and
        #> subsequent tests.
        support.run_test("test_multimode.py", "-q", "--resume",
                         enableJournal=True)
        print(open("subtest.log").read())

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Multiple suites and tests for testing the Tester.
        |   A
        |     2   : A-b...............................................               PASS
        |   B
        |     4   : B-a...............................................               PASS
        '''), open("subtest.log").read())


class TestMaxExecTime(Common):
    """Test the max exec command"""

    def tearDown(self):
        """Remove the generated file"""
        Files.rmFile("test_max_exec_time.py")

    @test
    def test_max_runtime_flag(self):
        """Test the max exec time command works as expected"""

        file_name = "test_max_exec_time.py"

        support.makePyFile(file_name, text='''
            | class A(Suite):
            |     """A"""
            |
            |     @test
            |     def a(self):
            |         """A-a"""
            |
            |     @test
            |     def b(self):
            |         """A-b"""
            |         # Long runtime so we can filter this out
            |         self.control.delay(2)
                    ''')
        # Run the test to make sure there is exec time data
        data = support.run_test(file_name, exitCode=0)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   A
            |     1   : A-a..........               PASS
            |     2   : A-b..........               PASS
            ''', data.terminal)

        # Run the test with the max exec set, this should filter out test b
        data = support.run_test("test_max_exec_time.py",
                                "--max-exec-time",
                                "1",
                                exitCode=0)

        support.checkTerminalOutput('''
                    | Testing the tester.
                    |   A
                    |     1   : A-a..........               PASS
                    ''', data.terminal)


class Test_Timeouts(Common):
    """Check that the framework's watchdog detects long-running code.

    """
    #CI - Does not work.
    #     All logging looks OK, but SIGNAL is not seen reliably!
    @test(testID="slowload")
    def slowLoad(self):
        """The tests take too long to load."""
        support.run_test("test_watchdog.py", "longload", "--timeout=2",
                exitCode=1)
        print(open("subtest.log").read())

    #@test
    def signal(self):
        """Send self-signal"""
        support.run_test("test_zzz.py", exitCode=0)
        print(open("subtest.log").read())


class Test_NestedOutput(Common):
    """The output produced by test runs should correctly reflect the nested
    test suite structure, including directories.

    This test targets a bug, which showed when the same test script is used
    in more than one sub-directory. This should happen even if the same script
    name occurs more than once.

    """
    @test(testID="sub-dir-1")
    def sub_dir_1(self):
        """A single level of sub-directory nesting should be shown."""
        support.makeTestDir("tree")
        support.makeTestDir("tree/A")
        support.makeTestDir("tree/B")
        support.makePySingleTestFile("tree/test_a.py", text='''
        | def a(self):
        |    """Test A"""
        ''')
        support.makePySingleTestFile("tree/A/test_a.py", text='''
        | def a(self):
        |    """Test A"""
        ''')
        support.makePySingleTestFile("tree/B/test_a.py", text='''
        | def a(self):
        |    """Test A"""
        ''')
        data = support.run_test("all_tests.py", startDir="tree")

        support.checkTerminalOutput('''
        | Testing the tester.
        |   Testing the tester.
        |     Suite A
        |       1   : Test A..........               PASS
        |   Testing the tester.
        |     Testing the tester.
        |       Suite A
        |         2   : Test A..........               PASS
        |   Testing the tester.
        |     Testing the tester.
        |       Suite A
        |         3   : Test A..........               PASS
        ''', data.terminal)


class FailureInEachPhase(Common):
    """The framework should correctly report failure in all phases.

    The phases are:

    - Suite set up.
    - Test set up.
    - The test itself.
    - Post test checks.
    - Test tear down.
    - Suite tear down.

    """

    @test(testID="err-suite-set-up-reporting")
    def test_suite_set_up_failure(self):
        """Failure during suite set up is reported correctly - first suite.

        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def suiteSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=3)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test suite set up failed - test(s) abandoned
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: suiteSetUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def suiteSetUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........    BAD_SUITE_SETUP
            |   Suite A..........    BAD_SUITE_SETUP
            |     1   : Test A..........    BAD_SUITE_SETUP
            ''', data.terminal)

    @test(testID="err-suite-set-up-second-suite")
    def test_suite_set_up_failure_suite_second_suite(self):
        """Failure during suite set up is reported correctly - second suite.

        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |     def suiteSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=3)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A............................................               PASS
            |   Suite B
            |           Test suite set up failed - test(s) abandoned
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: suiteSetUp
            |              12  : class B(Suite):
            |              13  :     """Suite B"""
            |              14  :     def suiteSetUp(self):
            |           ==>15  :         assert 0
            |              16  :
            |              17  :     @test
            |              18  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........    BAD_SUITE_SETUP
            |   Suite B..........    BAD_SUITE_SETUP
            |     2   : Test A..........    BAD_SUITE_SETUP
            ''', data.terminal)

    @test(testID="err-suite-set-up-execution-stops")
    def test_suite_set_up_failure_execution_stops(self):
        """Test that execution stops if an error occurs during suite set up"""
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def suiteSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=3)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test suite set up failed - test(s) abandoned
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: suiteSetUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def suiteSetUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........    BAD_SUITE_SETUP
            |   Suite A..........    BAD_SUITE_SETUP
            |     1   : Test A..........    BAD_SUITE_SETUP
            ''', data.terminal)

    @test(testID="err-suite-set-up-keep-going")
    def test_suite_set_up_failure_keep_going(self):
        """Test that execution continues if an error occurs during suite set up
        and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def suiteSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=3)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test suite set up failed - test(s) abandoned
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: suiteSetUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def suiteSetUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester..........    BAD_SUITE_SETUP
            |   Suite A..........    BAD_SUITE_SETUP
            |     1   : Test A..........    BAD_SUITE_SETUP
            ''', data.terminal)

    @test(testID="err-test-set-up-reporting")
    def test_set_up_failure(self):
        """Failure during test setup is reported correctly
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def setUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test set up failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: setUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def setUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |     1   : Test A..........          BAD_SETUP
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........          BAD_SETUP
            ''', data.terminal)

    @test(testID="err-test-set-up-execution-stops")
    def test_set_up_failure_execution_stops(self):
        """Test that execution stops if an error occurs during test set up
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def setUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test set up failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: setUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def setUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |     1   : Test A..........          BAD_SETUP
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........          BAD_SETUP
            ''', data.terminal)

    @test(testID="err-test-set-up-keep-going")
    def test_set_up_failure_keep_going(self):
        """Test that execution continues if an error occurs during test set up
        and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def setUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test set up failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: setUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def setUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |     1   : Test A..........          BAD_SETUP
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........          BAD_SETUP
            ''', data.terminal)

    @test(testID="err-post-set-up-reporting")
    def test_post_set_up_failure(self):
        """Failure during post set up is reported correctly
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def postSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test set up failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: postSetUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def postSetUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |     1   : Test A..........          BAD_SETUP
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........          BAD_SETUP
            ''', data.terminal)

    @test(testID="err-post-set-up-execution-stops")
    def test_post_set_up_failure_execution_stops(self):
        """Test that execution stops if an error occurs during post set up
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def postSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test set up failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: postSetUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def postSetUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |     1   : Test A..........          BAD_SETUP
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........          BAD_SETUP
            ''', data.terminal)

    @test(testID="err-post-set-up-keep-going")
    def test_post_set_up_failure_keep_going(self):
        """Test that execution continues if an error occurs during post set up
        and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def postSetUp(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |           Test set up failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: postSetUp
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def postSetUp(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |     1   : Test A..........          BAD_SETUP
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........          BAD_SETUP
            ''', data.terminal)

    @test(testID="err-test-reporting")
    def test_failure(self):
        """Failure during test is reported correctly
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |         assert 0
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               FAIL
            |           Test failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: a
            |              9   :     @test
            |              10  :     def a(self):
            |              11  :         """Test A"""
            |           ==>12  :         assert 0
            |              13  :
            |              14  : if __name__ == "__main__":
            |              15  :     runModule()
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........               FAIL
            ''', data.terminal)

    @test(testID="err-test-execution-stops")
    def test_failure_execution_stops(self):
        """Test that execution stops if an error occurs during the test itself
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |         assert 0
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               FAIL
            |           Test failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: a
            |              9   :     @test
            |              10  :     def a(self):
            |              11  :         """Test A"""
            |           ==>12  :         assert 0
            |              13  :
            |              14  : class B(Suite):
            |              15  :     """Suite B"""
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........               FAIL
            ''', data.terminal)

    @test(testID="err-test-keep-going")
    def test_failure_keep_going(self):
        """Test that execution continues if an error occurs during the test
        and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |         assert 0
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               FAIL
            |           Test failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: a
            |              9   :     @test
            |              10  :     def a(self):
            |              11  :         """Test A"""
            |           ==>12  :         assert 0
            |              13  :
            |              14  : class B(Suite):
            |              15  :     """Suite B"""
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........               FAIL
            ''', data.terminal)

    @test(testID="err-post-check-reporting")
    def test_post_check_failure(self):
        """Failure during post check is reported correctly
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def postCheck(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               FAIL
            |           Test failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: postCheck
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def postCheck(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........               FAIL
            ''', data.terminal)

    @test(testID="err-post-check-execution-stops")
    def test_post_check_failure_execution_stops(self):
        """Test that execution stops if an error occurs during post check
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def postCheck(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               FAIL
            |           Test failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: postCheck
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def postCheck(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........               FAIL
            ''', data.terminal)

    @test(testID="err-post-check-keep-going")
    def test_post_check_failure_keep_going(self):
        """Test that execution continues if an error occurs during post check
        and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def postCheck(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               FAIL
            |           Test failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: postCheck
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def postCheck(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........               FAIL
            ''', data.terminal)

    @test(testID="err-tear-down-reporting")
    def test_tear_down_failure(self):
        """Failure during test tear down is reported correctly
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def tearDown(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               PASS
            |           Test tear down failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: tearDown
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def tearDown(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........       BAD_TEARDOWN
            ''', data.terminal)

    @test(testID="err-tear-down-execution-stops")
    def test_tear_down_failure_execution_stops(self):
        """Test that execution stops if an error occurs during tear down
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def tearDown(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               PASS
            |           Test tear down failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: tearDown
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def tearDown(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........       BAD_TEARDOWN
            ''', data.terminal)

    @test(testID="err-tear-down-keep-going")
    def test_tear_down_failure_keep_going(self):
        """Test that execution continues if an error occurs during tear down
        and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def tearDown(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=1)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               PASS
            |           Test tear down failed
            |           Unhandled exception occurred:
            |             AssertionError:
            |           ./test_a.py: tearDown
            |              6   : class A(Suite):
            |              7   :     """Suite A"""
            |              8   :     def tearDown(self):
            |           ==>9   :         assert 0
            |              10  :
            |              11  :     @test
            |              12  :     def a(self):
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester..........         CHILD_FAIL
            |   Suite A..........         CHILD_FAIL
            |     1   : Test A..........       BAD_TEARDOWN
            ''', data.terminal)

    @test(testID="err-suite-tear-down-reporting")
    def suite_tear_down_failure(self):
        """Failure during suite tear down is reported correctly

        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def suiteTearDown(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=5)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               PASS
            |         Suite tear down failed
            |         Unhandled exception occurred:
            |           AssertionError:
            |         ./test_a.py: suiteTearDown
            |            6   : class A(Suite):
            |            7   :     """Suite A"""
            |            8   :     def suiteTearDown(self):
            |         ==>9   :         assert 0
            |            10  :
            |            11  :     @test
            |            12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester.......... BAD_SUITE_TEARDOWN
            |   Suite A.......... BAD_SUITE_TEARDOWN
            |     1   : Test A.......... BAD_SUITE_TEARDOWN
            ''', data.terminal)

    @test(testID="err-suite-tear-down-execution-stops")
    def suite_tear_down_failure_execution_stops(self):
        """Test that execution stops if an error occurs during suite tear down

        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def suiteTearDown(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", exitCode=5)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               PASS
            |         Suite tear down failed
            |         Unhandled exception occurred:
            |           AssertionError:
            |         ./test_a.py: suiteTearDown
            |            6   : class A(Suite):
            |            7   :     """Suite A"""
            |            8   :     def suiteTearDown(self):
            |         ==>9   :         assert 0
            |            10  :
            |            11  :     @test
            |            12  :     def a(self):
            |
            |
            | Summary of the failures
            | Testing the tester.......... BAD_SUITE_TEARDOWN
            |   Suite A.......... BAD_SUITE_TEARDOWN
            |     1   : Test A.......... BAD_SUITE_TEARDOWN
            ''', data.terminal)

    @test(testID="err-suite-tear-down-keep-going")
    def suite_tear_down_failure_keep_going(self):
        """Test that execution continues if an error occurs during suite tear
        down and the '--keep-going' flag has been specified
        """
        support.makePyFile("test_a.py", text='''
            | class A(Suite):
            |     """Suite A"""
            |     def suiteTearDown(self):
            |         assert 0
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            |
            | class B(Suite):
            |     """Suite B"""
            |
            |     @test
            |     def a(self):
            |         """Test A"""
            ''')
        data = support.run_test("test_a.py", '--keep-going', exitCode=5)

        support.checkTerminalOutput('''
            | Testing the tester.
            |   Suite A
            |     1   : Test A..........               PASS
            |         Suite tear down failed
            |         Unhandled exception occurred:
            |           AssertionError:
            |         ./test_a.py: suiteTearDown
            |            6   : class A(Suite):
            |            7   :     """Suite A"""
            |            8   :     def suiteTearDown(self):
            |         ==>9   :         assert 0
            |            10  :
            |            11  :     @test
            |            12  :     def a(self):
            |   Suite B
            |     2   : Test A..........               PASS
            |
            |
            | Summary of the failures
            | Testing the tester.......... BAD_SUITE_TEARDOWN
            |   Suite A.......... BAD_SUITE_TEARDOWN
            |     1   : Test A.......... BAD_SUITE_TEARDOWN
            ''', data.terminal)


class SimpleCallbacks(Common):
    """The built-in PollManager supports a simple call-back mechanism.

    """
    @test(testID="cb-clear")
    def cb_clear(self):
        """Callbacks should be cleared between tests.

        """
        support.makePyFile("test_a.py", text='''
        | class A(Suite):
        |     """Suite A"""
        |     def cb(self):
        |         self.data.append(None)
        |     @test
        |     def a(self):
        |         """Test A"""
        |         self.data = []
        |         self.control.addCallback(self.cb)
        |
        |     @test
        |     def b(self):
        |        """Test B"""
        |        self.control.delay(0.1)
        ''')
        data = support.run_test("test_a.py")

        support.checkTerminalOutput('''
        | Testing the tester.
        |   Suite A
        |     1   : Test A..........               PASS
        |     2   : Test B..........               PASS
        ''', data.terminal)


class RunInProcess(Common):
    """The framework can execute some tests in a sub-process."""
    @test(testID="test-in-fork-01")
    def singleTest(self):
        """Test marked for forking should run in sub-process.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |         self.f = open("data.log", "a")
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     def suiteTearDown(self):
        |         self.f.close()
        |
        |     @test(cs_fork=True)
        |     def a(self):
        |         """Test A"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test
        |     def b(self):
        |         """Test B"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        ''')
        data = support.run_test("test_a.py")
        pids = [int(s) for s in data.data.splitlines()]
        failIfEqual(pids[0], pids[1])
        failUnlessEqual(pids[0], pids[2])

    @test(testID="test-in-fork-02")
    def setUpIsForked(self):
        """The setUp method runs in the same process as the test.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |         self.f = open("data.log", "a")
        |
        |     def suiteTearDown(self):
        |         self.f.close()
        |
        |     def setUp(self):
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test(cs_fork=True)
        |     def a(self):
        |         """Test A"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test
        |     def b(self):
        |         """Test B"""
        |         self.f.write("%d\n" % os.getpid())
        ''')
        data = support.run_test("test_a.py")
        pids = [int(s) for s in data.data.splitlines()]
        failUnlessEqual(pids[0], pids[1])
        failIfEqual(pids[1], pids[2])

    @test(testID="test-in-fork-03")
    def tearDownIsForked(self):
        """The tearDown method runs in the same process as the test.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |         self.f = open("data.log", "a")
        |
        |     def suiteTearDown(self):
        |         self.f.close()
        |
        |     def tearDown(self):
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test(cs_fork=True)
        |     def a(self):
        |         """Test A"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test
        |     def b(self):
        |         """Test B"""
        |         self.f.write("%d\n" % os.getpid())
        ''')
        data = support.run_test("test_a.py")
        pids = [int(s) for s in data.data.splitlines()]
        failUnlessEqual(pids[0], pids[1])
        failIfEqual(pids[1], pids[2])

    @test("bug", testID="test-in-fork-04")
    def loggingWritesInMainProcess(self):
        """Logged (print) output is redirected to be written within the main
        process.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     @test(cs_fork=True)
        |     def a(self):
        |         """Test A"""
        |         print("Test print")
        ''')
        data = support.run_test("test_a.py")

        # If the log redirection is working then the indentation of the test
        # output will be correct. Otherwise the following output will not
        # match.
        self.expectLogOutput(data, """
        | 19:18:54.217 INFO    Started on 06/07/2014
        | 19:18:54.220 INFO    Suite: Testing the tester.
        | 19:18:54.222 INFO    Suite:   Suite A
        | 19:18:54.238 INFO    Test:      1   : Test A
        | 19:18:54.241 DEBUG                    Test print
        | 19:18:54.243 INFO    Test:            PASS
        | 19:18:54.251 INFO
        | 19:18:54.251 INFO    Summary of the entire run
        | 19:18:54.251 INFO    Suite: Testing the tester...................................               PASS
        | 19:18:54.251 INFO    Suite:   Suite A............................................               PASS
        | 19:18:54.251 INFO    Test:      1   : Test A.....................................               PASS
        """)

    @test("bug", testID="test-in-fork-05")
    def loggingWritesInMainProcess2(self):
        """Logged output is redirected to be written within the main process.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     @test(cs_fork=True)
        |     def a(self):
        |         """Test A"""
        |         log.info("Test print")
        ''')
        data = support.run_test("test_a.py")

        # If the log redirection is working then the indentation of the test
        # output will be correct. Otherwise the following output will not
        # match.
        self.expectLogOutput(data, """
        | 19:18:54.217 INFO    Started on 06/07/2014
        | 19:18:54.220 INFO    Suite: Testing the tester.
        | 19:18:54.222 INFO    Suite:   Suite A
        | 19:18:54.238 INFO    Test:      1   : Test A
        | 19:18:54.241 INFO                     Test print
        | 19:18:54.243 INFO    Test:            PASS
        | 19:18:54.251 INFO
        | 19:18:54.251 INFO    Summary of the entire run
        | 19:18:54.251 INFO    Suite: Testing the tester...................................               PASS
        | 19:18:54.251 INFO    Suite:   Suite A............................................               PASS
        | 19:18:54.251 INFO    Test:      1   : Test A.....................................               PASS
        """)

    @test(testID="all-tests-fork-05")
    def allTestsRunForked(self):
        """A suite can be marked such that all tests execute forked.

        Each test runs in a separate process, while the suiteSetUp,
        suiteTearDown and main thread of control runs in the root process.
        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | from CleverSheep.Prog import Files
        | @cs_fork_all
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         Files.rmFile("data.log")
        |         self.f = open("data.log", "a")
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     def suiteTearDown(self):
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |         self.f.close()
        |
        |     def setUp(self):
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test
        |     def a(self):
        |         """Test A"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test
        |     def b(self):
        |         """Test B"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        ''')
        data = support.run_test("test_a.py")
        pids = [int(s) for s in data.data.splitlines()]
        failIfEqual(pids[0], pids[1])
        failUnlessEqual(pids[1], pids[2])
        failIfEqual(pids[2], pids[3])
        failUnlessEqual(pids[3], pids[4])
        failUnlessEqual(pids[0], pids[5])

    @test(testID="fork-summary-01")
    def singleTest(self):
        """Test marked for forking can still be summarised.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |         self.f = open("data.log", "a")
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     def suiteTearDown(self):
        |         self.f.close()
        |
        |     @test(cs_fork=True)
        |     def a(self):
        |         """Test A"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        |
        |     @test
        |     def b(self):
        |         """Test B"""
        |         self.f.write("%d\n" % os.getpid())
        |         self.f.flush()
        ''')
        data = support.run_test("test_a.py", "--summary")
        self.expectOutput("""
        | Testing the tester.
        |   Suite A
        |     1   : Test A
        |     2   : Test B
        """)


class MarkBrokenTests(Common):
    """Tests can be marked a broken.

    Broken tests are not executed by default.
    """
    @test(testID="broken-01")
    def brokenNotRun(self):
        """A broken test is not executed.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |
        |     @test()
        |     def a(self):
        |         """Test A"""
        |         self.f.write("A")
        |
        |     @test(cs_broken=True)
        |     def b(self):
        |         """Test B"""
        |         self.f.write("B")
        |
        |     @test
        |     def c(self):
        |         """Test C"""
        |         self.f.write("C")
        ''')
        data = support.run_test("test_a.py")
        failUnlessEqual("AC", data.data)

    @test(testID="broken-02")
    def brokenNotRunDeprecated(self):
        """Deprecated broken marking is supported.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |
        |     @test()
        |     def a(self):
        |         """Test A"""
        |         self.f.write("A")
        |
        |     @test("broken")
        |     def b(self):
        |         """Test B"""
        |         self.f.write("B")
        |
        |     @test
        |     def c(self):
        |         """Test C"""
        |         self.f.write("C")
        ''')
        data = support.run_test("test_a.py")
        failUnlessEqual("AC", data.data)

    @test(testID="broken-03")
    def brokenNotRun(self):
        """Command line can force run of broken tests.

        """
        support.makePyFile("test_a.py", text=r'''
        | import os
        | class A(Suite):
        |     """Suite A"""
        |     def suiteSetUp(self):
        |         self.f = open("data.log", "w")
        |
        |     @test()
        |     def a(self):
        |         """Test A"""
        |         self.f.write("A")
        |
        |     @test(cs_broken=True)
        |     def b(self):
        |         """Test B"""
        |         self.f.write("B")
        |
        |     @test
        |     def c(self):
        |         """Test C"""
        |         self.f.write("C")
        ''')
        data = support.run_test("test_a.py", "--run-broken")
        failUnlessEqual("ABC", data.data)


class PretendToFail(Common):
    """You can execute a test and pretend to fail an assertion.

    This is useful to verify that a failure message is clear and meaningful,
    without having to temporarily edit the test code.
    """
    script = r'''
        | class A(Suite):
        |     """Suite A"""
        |     @test()
        |     def a(self):
        |         """Test A"""
        |         failUnlessEqual(1, 1)
        |         failUnlessEqual(2, 1 + 1)
        |
        |     @test()
        |     def b(self):
        |         """Test B"""
        |         failUnlessEqual(3, 3)
        |         failUnlessEqual(4, 2 + 2)
        '''

    def setUp(self):
        support.makePyFile("test_a.py", text=self.script)

    @test(testID="spoof-fail-01")
    def spoofFirst(self):
        """Spoof a failure of the first assertion."""
        data = support.run_test("test_a.py", "--fail-on=1", exitCode=1)
        self.expectOutput("""
        |           ==>11  :         failUnlessEqual(1, 1)
        """, lineRanges=((11, 12),))

    @test(testID="spoof-fail-02")
    def spoofSecond(self):
        """Spoof a failure of the second assertion."""
        data = support.run_test("test_a.py", "--fail-on=2", exitCode=1)
        self.expectOutput("""
        |           ==>12  :         failUnlessEqual(2, 1 + 1)
        """, lineRanges=((11, 12),))

    @test(testID="spoof-fail-03")
    def spoofAppliesToAllTestsFirst(self):
        """The --fail-on option applies to all executed tests - first fail.
        """
        data = support.run_test("test_a.py", "--fail-on=1", "--keep-going",
                                exitCode=1)
        self.expectOutput("""
        |           ==>11  :         failUnlessEqual(1, 1)
        |           ==>17  :         failUnlessEqual(3, 3)
        """, lineRanges=((11, 12), (24, 25)))

    @test(testID="spoof-fail-04")
    def spoofAppliesToAllTestsSecond(self):
        """The --fail-on option applies to all executed tests - second fail.
        """
        data = support.run_test("test_a.py", "--fail-on=2", "--keep-going",
                                exitCode=1)
        self.expectOutput("""
        |           ==>12  :         failUnlessEqual(2, 1 + 1)
        |           ==>18  :         failUnlessEqual(4, 2 + 2)
        """, lineRanges=((11, 12), (24, 25)))


class TestDisableRunAndTrace(Suite):
    """Test the #> comments can be disabled via the command line"""

    @test()
    def test_special_comments_come_out_as_standard(self):
        """Run a test and make sure the special comments come out by default"""
        support.makePyFile("test_a.py", text=r'''
                | import os
                | class A(Suite):
                |     """Suite A"""
                |
                |     @test()
                |     def a(self):
                |         """Test A"""
                |         #> Comment 1
                |         self.control.delay(1)

                ''')
        data = support.run_test("test_a.py")
        failUnless("#> 1. Comment 1" in data.log)

    @test()
    def test_disabling_special_comments(self):
        """Run a test and make sure the special comments can be disabled"""
        support.makePyFile("test_a.py", text=r'''
                        | import os
                        | class A(Suite):
                        |     """Suite A"""
                        |
                        |     @test()
                        |     def a(self):
                        |         """Test A"""
                        |         #> Comment 1
                        |         self.control.delay(1)

                        ''')
        data = support.run_test("test_a.py", "--disable-run-and-trace")
        failIf("#> 1. Comment 1" in data.log)


if __name__ == "__main__":
    runModule()
