#!/usr/bin/env python
"""CleverSheep.Prog.Iter unit tests"""

import os
import sys

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import DataMaker
from TestSupport.Files import FileBasedTests, ensureNoFile, delDirs


# The module under test.
from CleverSheep.Test import ImpUtils


def unloadPath(path):
    """Unload a module with the given path."""
    path = os.path.abspath(path)
    for name, mod in sys.modules.items():
        try:
            if mod.__file__ == path:
                break
        except AttributeError:
            pass
    else:
        return
    del sys.modules[name]


def unloadModule(mod):
    """Unload a given module."""
    for name, mod in sys.modules:
        if mod is mod:
            break
    else:
        return
    del sys.modules[name]


class Test_importPath(FileBasedTests):
    """Tests for the importPath function."""
    def tearDown(self):
        super(Test_importPath, self).tearDown()
        unloadPath("A/a.py")
        unloadPath("A/b.py")
        unloadPath("B/a.py")
        unloadPath("B/b.py")
        delDirs(("A", "B"))

    @test
    def importUsingRelPath(self):
        """Import a module using just a relative path.

        The module's name should be automatically generated.

        """
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "A.a"
        """))
        mod = ImpUtils.importPath("A/a.py")
        failUnless(mod)
        failUnlessEqual("A.a", mod.name)
        failIfEqual("A.a", mod.__name__)

    @test
    def importWithoutPyExtension(self):
        """Import a module using a path with no '.py' extension."""
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "A.a"
        """))
        mod = ImpUtils.importPath("A/a")
        failUnless(mod)
        failUnlessEqual("A.a", mod.name)
        failIfEqual("A.a", mod.__name__)

    @test
    def importUsingAbs(self):
        """Import a module using an absolute path."""
        self.addFile("A/a.py", DataMaker.literalText2Text("""
            | name = "A.a2"
            """))
        mod = ImpUtils.importPath(os.path.abspath("A/a.py"))
        failUnless(mod)
        failUnlessEqual("A.a2", mod.name)
        failIfEqual("A.a", mod.__name__)

    @test
    def importWithName(self):
        """Import a module specifying a name."""
        self.addFile("A/b.py", DataMaker.literalText2Text("""
            | name = "A.b"
            """))
        mod = ImpUtils.importPath("A/b.py", name="fred")
        failUnless(mod)
        failUnlessEqual("A.b", mod.name)
        failUnlessEqual("fred", mod.__name__)

    @test
    def importWithCwdOverride(self):
        """Import a module specifying the path should be relative to CWD."""
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | import os
        | from CleverSheep.Test import ImpUtils
        | os.chdir("B")
        | b = ImpUtils.importPath('b.py', cwd=True)
        | name = "A.a"
        """))
        # This module should *not* get imported.
        self.addFile("A/b.py", DataMaker.literalText2Text("""
            | name = "A.b"
            """))
        # This module should be the one that is imported.
        self.addFile("B/b.py", DataMaker.literalText2Text("""
            | name = "B.b"
            """))
        mod = ImpUtils.importPath("A/a.py")
        failUnless(mod)
        failUnlessEqual("A.a", mod.name)
        failUnlessEqual("B.b", mod.b.name)

    @test
    def importStar(self):
        """Import a module using the star option."""
        altModContent = DataMaker.literalText2Text("""
        | from CleverSheep.Test import ImpUtils
        | ImpUtils.importPath('b.py', star=True)
        """)
        self.addFile("A/a.py", altModContent)
        self.addFile("A/b.py", DataMaker.literalText2Text("""
            | name = "from-b"
            """))
        mod = ImpUtils.importPath("A/a.py")
        failUnless(mod)
        failUnlessEqual("from-b", mod.name)

    @test
    def importStarAltDict(self):
        """Import a module using start with a specific dict."""
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | from CleverSheep.Test.Tester import failUnless, failUnlessEqual
        | from CleverSheep.Test import ImpUtils
        |
        | name = "A.a"
        | ns = {"__file__": __file__}
        | ImpUtils.importPath('b.py', star=True, callerDict=ns)
        |
        | failUnless("name" in ns)
        | failUnlessEqual("from-b", ns["name"])
        | failUnlessEqual("A.a", name)
        """))
        self.addFile("A/b.py", DataMaker.literalText2Text("""
            | name = "from-b"
            """))
        mod = ImpUtils.importPath("A/a.py")
        failUnless(mod)

    @test
    def badPath(self):
        """A bad path should raise an import error."""
        failUnlessRaises(ImportError, ImpUtils.importPath,
                "A/no_such_file.py", name="A.bb")

    # TODO: Disabled because this feature is of dubious utility and my
    #       attempts to make it work broke existing behaviour.
    #@test
    def nestedImports(self):
        """Import a module, which imports a nested, local module.

        This test also verifies that ``sys.path`` is left in the correct
        state after the import.
        """
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | import b
        | import sys
        | sys.path[0:0] = ["hi"]
        """))
        self.addFile("A/b.py", DataMaker.literalText2Text("""
        | name = "local b"
        """))
        self.addFile("A/__init__.py", DataMaker.literalText2Text("""
        |
        """))
        try:
            mod = ImpUtils.importPath("A/a.py")
            failUnless(mod)
            failUnlessEqual("local b", mod.b.name)
            failUnlessEqual("hi", sys.path[0])
        finally:
            if sys.path[0] == "hi":
                del sys.path[0]
            if sys.path[1] == "hi":
                del sys.path[1]


class Test_findRoot(FileBasedTests):
    """Tests for the findRoot function."""
    @test
    def findDefault(self):
        """Find the root, using the default name .PROJECT_ROOT"""
        self.addDir("A/B/C")
        self.addFile("A/.PROJECT_ROOT")
        self.addFile("A/B/PROJECT_ROOT")
        os.chdir("A/B/C")
        failUnlessEqual(os.path.abspath("../.."), ImpUtils.findRoot())

    @test
    def findAltDefault(self):
        """Find the root, using the alternative default name PROJECT_ROOT"""
        self.addDir("/tmp", "A/B/C")
        self.addFile("/tmp/A/PROJECT_ROOT")
        os.chdir("/tmp/A/B/C")
        failUnlessEqual(os.path.abspath("/tmp/A"), ImpUtils.findRoot())

    @test
    def findSpecificMarker(self):
        """Find the root, specifying the marker file"""
        self.addDir("A/B/C")
        self.addFile("A/MymFile")
        self.addFile("A/B/.PROJECT_ROOT")
        os.chdir("A/B/C")
        failUnlessEqual(os.path.abspath("../.."),
                ImpUtils.findRoot("MymFile"))

    @test
    def findFailure(self):
        """Find failure."""
        self.addDir("/tmp", "A/B/C")
        os.chdir("/tmp/A/B/C")
        failUnlessEqual(None, ImpUtils.findRoot())


class Test_addPathAtRoot(FileBasedTests):
    """Tests for the addPathAtRoot function."""
    def tearDown(self):
        super(Test_addPathAtRoot, self).tearDown()
        unloadPath("A/a.py")
        unloadPath("A/B/C/b.py")
        unloadPath("A/B/C/c.py")
        delDirs(("A", "B"))

    @test
    def addRootItself(self):
        """Adding the root directory itself as to the Path."""
        self.addDir("A/B/C")
        self.addFile("A/.PROJECT_ROOT")
        self.addFile("A/B/PROJECT_ROOT")
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "a"
        """))
        self.addFile("A/B/C/b.py", DataMaker.literalText2Text("""
        | import a
        | name = "b"
        """))
        here = os.getcwd()
        os.chdir("A/B/C")
        try:
            failUnlessRaises(ImportError, ImpUtils.importPath, "./b.py")
            self.addFile("c.py", DataMaker.literalText2Text("""
            | from CleverSheep.Test import ImpUtils
            | ImpUtils.addPathAtRoot(".")
            | import a
            | name = "c"
            """))
            # Import should work OK now.
            ImpUtils.importPath("./c.py", cwd=True)
        finally:
            os.chdir(here)


class Test_importAbove(FileBasedTests):
    """Tests for the importAbove function."""
    def tearDown(self):
        super(Test_importAbove, self).tearDown()
        unloadPath("a.py")
        unloadPath("A/a.py")
        unloadPath("A/b.py")
        unloadPath("A/B/b.py")
        delDirs(("A", "B"))

    @test
    def simpleImport(self):
        """A simple import of a module in a parent directory."""
        self.addDir("A/B")
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "a"
        """))
        self.addFile("A/B/b.py", DataMaker.literalText2Text("""
        | from CleverSheep.Test import ImpUtils
        | a = ImpUtils.importAbove('a.py')
        """))
        self.addFile("a.py", DataMaker.literalText2Text("""
        | name = "not-wanted"
        """))
        mod = ImpUtils.importPath("A/B/b.py")
        failUnless(mod)
        failUnlessEqual("a", mod.a.name)

    @test
    def importTwices(self):
        """Importing a second time is a NULL operation."""
        self.addDir("A/B")
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "a"
        """))
        self.addFile("A/B/b.py", DataMaker.literalText2Text("""
        | from CleverSheep.Test import ImpUtils
        | a = ImpUtils.importAbove('a.py')
        """))
        self.addFile("a.py", DataMaker.literalText2Text("""
        | name = "not-wanted"
        """))
        mod = ImpUtils.importPath("A/B/b.py")
        failUnless(mod)
        failUnlessEqual("a", mod.a.name)

        mod = ImpUtils.importPath("A/B/b.py")
        failUnless(mod)
        failUnlessEqual("a", mod.a.name)

    @test
    def usingCwdOverride(self):
        """Use the CWD override to alter import-from directory."""
        self.addDir("A/B")
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "not-wanted"
        """))
        self.addFile("A/B/b.py", DataMaker.literalText2Text("""
        | from CleverSheep.Test import ImpUtils
        | ImpUtils.importAbove('a.py', mode="*", cwd=True)
        """))
        self.addFile("a.py", DataMaker.literalText2Text("""
        | name = "a"
        """))
        mod = ImpUtils.importPath("A/B/b.py", cwd=1)
        failUnless(mod)
        failUnlessEqual("a", mod.name)

    @test
    def importStar(self):
        """Importing with the star option."""
        self.addDir("A/B")
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "a"
        """))
        self.addFile("A/b.py", DataMaker.literalText2Text("""
        | from CleverSheep.Test import ImpUtils
        | ImpUtils.importAbove('a.py', star=True)
        | amB = True
        """))
        os.chdir("A/B")
        mod = ImpUtils.importAbove("b.py", cwd=True)
        failUnless(mod)
        failUnlessEqual("a", mod.name)
        failUnless(mod.amB)

    @test
    def importStarMode(self):
        """Importing with mode="*" option, equivalent to star mode."""
        self.addDir("A/B")
        self.addFile("A/a.py", DataMaker.literalText2Text("""
        | name = "a"
        """))
        self.addFile("A/b.py", DataMaker.literalText2Text("""
        | from CleverSheep.Test import ImpUtils
        | ImpUtils.importAbove('a.py', mode="*")
        | amB = True
        """))
        os.chdir("A/B")
        mod = ImpUtils.importAbove("b.py", cwd=1)
        failUnless(mod)
        failUnlessEqual("a", mod.name)
        failUnless(mod.amB)

    @test
    def importFail(self):
        """Import failure should raise ImportError."""
        failUnlessRaises(ImportError, ImpUtils.importAbove,
                "no_such_file.py")


if __name__ == "__main__":
    runModule()
