#!/usr/bin/env python
"""CleverSheep.Test.TestEventStore unit tests"""

import time
import os
import errno

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test.Tester import Suites

# The module under test.
from CleverSheep.Test import TestEventStore

control = Suites.control()


class TestEvent(Suite):
    """Tests for the TestEvent class."""
    @test
    def empty_event(self):
        """We can create an empty TestEvent instance."""
        e = TestEventStore.TestEvent()
        failUnlessEqual([], e.args)
        failUnlessEqual({}, e.info)

    @test
    def event_with_args(self):
        """We can create an TestEvent instance with arguments"""
        e = TestEventStore.TestEvent(2, 99)
        failUnlessEqual([2, 99], e.args)
        failUnlessEqual({}, e.info)

    @test
    def event_with_kwargs(self):
        """We can create an TestEvent instance with keyword arguments"""
        e = TestEventStore.TestEvent(a=5, b=999)
        failUnlessEqual([], e.args)
        failUnlessEqual({"a": 5, "b": 999}, e.info)

    @test
    def event_with_both_args(self):
        """We can create an TestEvent instance with positional and keyword
        arguments"""
        e = TestEventStore.TestEvent(2, 99, a=5, b=999)
        failUnlessEqual([2, 99], e.args)
        failUnlessEqual({"a": 5, "b": 999}, e.info)

    @test
    def attribute_style_access(self):
        """Keyword arguments can be accessed like attributes."""
        e = TestEventStore.TestEvent(2, 99, a=5, b=999)
        failUnlessEqual(5, e.a)
        failUnlessEqual(999, e.b)

    @test
    def comparison(self):
        """Events can be compared for equality."""
        e = TestEventStore.TestEvent(2, 99, a=5, b=999)
        e2 = TestEventStore.TestEvent(2, 99, b=999, a=5)
        failUnless(e == e2, msg="Events did not compare equal")
        failIf(e != e2, msg="Events did not compare equal")

        e2 = TestEventStore.TestEvent(2, 99, b=999, a=6)
        failIf(e == e2, msg="Events unexpectedly compared equal")
        failUnless(e != e2, msg="Events unexpectedly compared equal")

        e2 = TestEventStore.TestEvent(2, 98, b=999, a=5)
        failIf(e == e2, msg="Events unexpectedly compared equal")
        failUnless(e != e2, msg="Events unexpectedly compared equal")

        e2 = TestEventStore.TestEvent(2, b=999, a=5)
        failIf(e == e2, msg="Events unexpectedly compared equal")
        failUnless(e != e2, msg="Events unexpectedly compared equal")

        e2 = TestEventStore.TestEvent(2, 99, a=5)
        failIf(e == e2, msg="Events unexpectedly compared equal")
        failUnless(e != e2, msg="Events unexpectedly compared equal")


class EventStore(Suite):
    """Tests for the EventStore class."""
    @test
    def the_store(self):
        """There should be a ready made TestEventStore."""
        failUnless(hasattr(TestEventStore, "eventStore"))
        failUnless(isinstance(TestEventStore.eventStore,
            TestEventStore.EventStore))

    @test
    def creation_defaults(self):
        """Creation of an EventStore using default parameters."""
        s = TestEventStore.EventStore()
        failUnlessEqual(10.0, s.maxAge)
        failUnlessEqual(100, s.maxLength)
        failUnlessEqual(0, len(s))
        failUnlessEqual(None, s.getOldest())

    @test
    def creation_nondefaults(self):
        """Creation of an EventStore overriding default constraints."""
        s = TestEventStore.EventStore()
        s.maxAge = 20.0
        s.maxLength = 50
        failUnlessEqual(20.0, s.maxAge)
        failUnlessEqual(50, s.maxLength)
        failUnlessEqual(0, len(s))

    @test
    def adding_events(self):
        """Basic adding of events to the event store."""
        s = TestEventStore.EventStore()
        s.addEvent(1)
        s.addEvent(2)
        s.addEvent(3)
        failUnlessEqual(3, len(s))

    @test
    def clear_all_events(self):
        """Clearing all the events in the store."""
        s = TestEventStore.EventStore()
        s.addEvent(1)
        s.addEvent(2)
        s.addEvent(3)
        failUnlessEqual(3, len(s))
        s.clear()
        failUnlessEqual(0, len(s))

    @test
    def exceeding_max_length(self):
        """Oldest events are removed when length is exceeded."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(10):
            s.addEvent(i)
        failUnlessEqual(10, len(s))
        failUnlessEqual(0, s.getOldest())
        s.addEvent(10)
        failUnlessEqual(10, len(s))
        failUnlessEqual(1, s.getOldest())

    @test
    def trim_by_length(self):
        """Store can be trimmed to a specific length."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(10):
            s.addEvent(i)
        failUnlessEqual(10, len(s))
        s.trim(maxLength=5)
        failUnlessEqual(5, len(s))
        s.trim(maxLength=1)
        failUnlessEqual(1, len(s))
        s.trim(maxLength=0)
        failUnlessEqual(0, len(s))
        failUnlessEqual(None, s.getOldest())

    @test
    def trim_by_age(self):
        """Store can be trimmed to a specific age."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(4):
            if i:
                control.delay(0.1)
            s.addEvent(i)
        failUnlessEqual(4, len(s))
        control.delay(0.05)
        s.trim(maxAge=0.2)
        failUnlessEqual(2, len(s))
        s.trim(maxAge=0.1)
        failUnlessEqual(1, len(s))
        s.trim(maxAge=0)
        failUnlessEqual(0, len(s))
        failUnlessEqual(None, s.getOldest())

    @test
    def filtered_iteration_with_class(self):
        """We can iterate the store, selecting with a filter, limiting hits to
        a specific class."""
        # TestEvents first.
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))
        i = 0
        for i, e in enumerate(s.filter(lambda x:True)):
            failUnless(isinstance(e, TestEventStore.TestEvent))
        failUnlessEqual(3, i)

        # Look for even integers.
        for i, e in enumerate(s.filter(lambda x: x.args[0] % 2 == 0)):
            failUnless(e.args[0] % 2 == 0)
        failUnlessEqual(1, i)

        # Specific class
        for i, e in enumerate(s.filter(lambda x:True, int)):
            failUnless(isinstance(e, int))
        failUnlessEqual(1, i)

        # More than one class
        for i, e in enumerate(s.filter(lambda x:True,
                (TestEventStore.TestEvent, int))):
            pass
        failUnlessEqual(5, i)

    @test
    def filtered_iteration_with_no_class(self):
        """We can iterate the store, selecting with a filter ignoring class"""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))
        i = 0
        for i, e in enumerate(s.filter(lambda x:True, None)):
            pass
        failUnlessEqual(5, i)

    @test
    def find_with_class_and_evt(self):
        """We can find items in the store, limiting hits to a specific
        class."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(TestEventStore.TestEvent(2))
        failUnlessEqual(2, e.args[0])
        e = s.find(3)
        failUnless(e is None)

    @test
    def find_with_no_class_and_evt(self):
        """We can find items in the store, ignoring the class."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(3, klass=None)
        failUnlessEqual(3, e)

    @test
    def find_with_predicate(self):
        """We can find items in the using a predicate."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(predicate=lambda x: x.args[0] == 4)
        failUnlessEqual(4, e.args[0])

        e = s.find(predicate=lambda x: x.args[0] == 4)
        failUnlessEqual(4, e.args[0])

    @test
    def find_with_chop(self):
        """We can chop the found and older items."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(predicate=lambda x: x.args[0] == 1, chop=True)
        failUnlessEqual(1, e.args[0])
        e = s.find(predicate=lambda x: x.args[0] == 2, chop=True)
        failUnlessEqual(2, e.args[0])

        e = s.find(predicate=lambda x: x.args[0] == 1, chop=True)
        failUnless(e is None)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failUnless(e is None)

        e = s.find(predicate=lambda x: x.args[0] == 4, chop=True)
        failUnlessEqual(4, e.args[0])

    @test
    def manual_chop_with_item(self):
        """We can chop and entry and older items."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnlessEqual(1, e.args[0])
        s.chop(e)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failUnlessEqual(2, e.args[0])
        s.chop(e)

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnless(e is None)
        s.chop(e)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failUnless(e is None)
        s.chop(e)

        e = s.find(predicate=lambda x: x.args[0] == 4)
        failUnlessEqual(4, e.args[0])

    @test
    def manual_chop_without_item(self):
        """We can chop entries older than an item."""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnlessEqual(1, e.args[0])
        s.chop(e, inclusive=False)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failUnlessEqual(2, e.args[0])
        s.chop(e, inclusive=False)

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnless(e is None)
        s.chop(e, inclusive=False)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failIf(e is None)
        s.chop(e, inclusive=False)

        e = s.find(predicate=lambda x: x.args[0] == 4)
        failUnlessEqual(4, e.args[0])

    @test
    def removal(self):
        """We remove individual items"""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnlessEqual(1, e.args[0])
        s.remove(e)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failUnlessEqual(2, e.args[0])
        s.remove(e)

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnless(e is None)
        e = s.find(predicate=lambda x: x.args[0] == 2)
        failUnless(e is None)

        e = s.find(predicate=lambda x: x.args[0] == 4)
        failUnlessEqual(4, e.args[0])
        s.remove(e)
        e = s.find(predicate=lambda x: x.args[0] == 4)
        failUnless(e is None)

    @test
    def removal_of_unknown(self):
        """Attempt to remove an unknown item is silently ignored"""
        s = TestEventStore.EventStore()
        s.maxLength = 10
        for i in range(6):
            if i % 3 != 0:
                s.addEvent(TestEventStore.TestEvent(i))
            else:
                s.addEvent(i)
        failUnlessEqual(6, len(s))

        e = s.find(predicate=lambda x: x.args[0] == 1)
        failUnlessEqual(1, e.args[0])
        s.remove(e)
        s.remove(e)

if __name__ == "__main__":
    runModule()
