#!/usr/bin/env python
"""CleverSheep.Test.Table unit tests"""

import CheckEnv

import six

from six.moves import cStringIO as StringIO

from CleverSheep.Test.Tester import *
from CleverSheep.Test.DataMaker import literalText2Text

# The module under test.
from CleverSheep.Test import Table


class Counters(Suite):
    """Tests for the Counters class."""
    @test
    def construction(self):
        """Verify basic construction"""
        c = Table.Counters()
        failUnlessEqual(0, c.v_xxx)
        failUnlessEqual(0, c.t_xxx)
        failUnlessEqual(0, c.v("xxx"))
        failUnlessEqual(0, c.tot("xxx"))

    @test
    def incrementing(self):
        """Verify incrementing counters"""
        c = Table.Counters()
        failUnlessEqual(0, c.v_xxx)
        failUnlessEqual(0, c.t_xxx)
        c.inc("xxx")
        failUnlessEqual(1, c.v_xxx)
        failUnlessEqual(1, c.t_xxx)

        c.inc("xxx", "yyy")
        failUnlessEqual(2, c.v_xxx)
        failUnlessEqual(2, c.t_xxx)
        failUnlessEqual(2, c.v("xxx"))
        failUnlessEqual(2, c.tot("xxx"))
        failUnlessEqual(1, c.v_yyy)
        failUnlessEqual(1, c.t_yyy)
        failUnlessEqual(1, c.v("yyy"))
        failUnlessEqual(1, c.tot("yyy"))

    @test
    def querying_names(self):
        """Verify that the defined names can be queried"""
        c = Table.Counters()
        failUnlessEqual([], c.names())

        c.inc("xxx")
        failUnlessEqual(["xxx"], c.names())
        c.inc("a", "b", "c")
        failUnlessEqual(["a", "b", "c", "xxx"], sorted(c.names()))

    @test
    def resetting(self):
        """Verify that the counters can be reset, leaving totals unchanged."""
        c = Table.Counters()
        c.inc("a", "b")
        c.inc("a")
        failUnlessEqual(2, c.v_a)
        failUnlessEqual(1, c.v_b)

        c.reset()
        failUnlessEqual(0, c.v_a)
        failUnlessEqual(0, c.v_b)
        failUnlessEqual(2, c.t_a)
        failUnlessEqual(1, c.t_b)

        c.inc("a")
        failUnlessEqual(1, c.v_a)
        failUnlessEqual(3, c.t_a)

    @test
    def invalid_attribute(self):
        """Verify invalid attribute names raise exceptions"""
        c = Table.Counters()
        failUnlessRaises(AttributeError, lambda: c.V_xxx)

    @test
    def setting(self):
        """Verify that the counters can explicitly set.

        However, the total should be incremented by the newly set value.

        """
        c = Table.Counters()
        c.set("a", 123)
        c.set("b", 999)
        failUnlessEqual(123, c.v_a)
        failUnlessEqual(999, c.v_b)

        c.set("a", 321)
        c.set("b", 1)
        failUnlessEqual(321, c.v_a)
        failUnlessEqual(1, c.v_b)
        failUnlessEqual(444, c.t_a)
        failUnlessEqual(1000, c.t_b)


class Test_Table(Suite):
    """Tests for the Table class."""
    @test
    def drawNonTable(self):
        """Check that a table with no columns draws OK.

        It does not draw anything very interesting of course.
        """
        t = Table.Table()
        s = StringIO()
        t.draw(s)
        failUnlessEqual("\n\n", s.getvalue())

    @test
    def drawEmptyTable(self):
        """Check that an empty table draws OK."""
        t = Table.Table()
        t.addColumn("aaa", "%s", "%s", uline=1, ljust=1)
        t.addColumn("bbbb", "%s", "%s", uline=0, ljust=1)
        s = StringIO()
        t.draw(s)
        failUnlessEqualStrings(literalText2Text("""
        | aaa bbbb
        | ===
        |
        """), s.getvalue())

    @test
    def drawSimpleTable(self):
        """Check that simple table (only strings) draws OK."""
        t = Table.Table()
        t.addColumn("North", "%s", "%s", uline=1, ljust=1)
        t.addColumn("East ", "%s", "%s", uline=1, ljust=1)
        t.addColumn("South", "%s", "%s", uline=1, ljust=1)
        t.addColumn("West ", "%s", "%s", uline=1, ljust=1)
        t.addRow("Cold", "Chill", "Warm", "Windy")
        t.addRow("0 C", "-2 C", "20 C", "15 C")
        s = StringIO()
        t.draw(s)
        failUnlessEqualStrings(literalText2Text("""
        | North East  South West
        | ===== ===== ===== =====
        | Cold  Chill Warm  Windy
        | 0 C   -2 C  20 C  15 C
        |
        """), s.getvalue())

    @test
    def headerUndelines(self):
        """Check that we can selectively underline headins."""
        t = Table.Table()
        t.addColumn("North", "%s", "%s", uline=0, ljust=1)
        t.addColumn("East ", "%s", "%s", uline=1, ljust=1)
        t.addColumn("South", "%s", "%s", uline=0, ljust=1)
        t.addColumn("West ", "%s", "%s", uline=1, ljust=1)
        t.addRow("Cold", "Chill", "Warm", "Windy")
        t.addRow("0 C", "-2 C", "20 C", "15 C")
        s = StringIO()
        t.draw(s)
        failUnlessEqualStrings(literalText2Text("""
        | North East  South West
        |       =====       =====
        | Cold  Chill Warm  Windy
        | 0 C   -2 C  20 C  15 C
        |
        """), s.getvalue())

    @test
    def justification(self):
        """Check column justification control"""
        t = Table.Table()
        t.addColumn("North", "%s", "%s", uline=1, ljust=0)
        t.addColumn("East ", "%s", "%s", uline=1, ljust=1)
        t.addColumn("South", "%s", "%s", uline=1, ljust=0)
        t.addColumn("West ", "%s", "%s", uline=1, ljust=1)
        t.addRow("Cold", "Chill", "Warm", "Windy")
        t.addRow("0 C", "-2 C", "20 C", "15 C")
        s = StringIO()
        t.draw(s)
        failUnlessEqualStrings(literalText2Text("""
        | North East  South West
        | ===== ===== ===== =====
        |  Cold Chill  Warm Windy
        |   0 C -2 C   20 C 15 C
        |
        """), s.getvalue())

    @test
    def addingUnderlines(self):
        """Check we can add extra unlines."""
        t = Table.Table()
        t.addColumn("North", "%s", "%s", uline=1, ljust=0)
        t.addColumn("East ", "%s", "%s", uline=1, ljust=1)
        t.addColumn("South", "%s", "%s", uline=1, ljust=0)
        t.addColumn("West ", "%s", "%s", uline=1, ljust=1)
        t.addColumn("All",   "%s", "%s", uline=0, ljust=1)
        t.addRow("Cold", "Chill", "Warm", "Windy")
        t.addRow("0 C", "-2 C", "20 C", "15 C")
        t.addLine()
        t.addRow("No", "No", "Yes", "No", "Maybe")
        s = StringIO()
        t.draw(s)
        failUnlessEqualStrings(literalText2Text("""
        | North East  South West  All
        | ===== ===== ===== =====
        |  Cold Chill  Warm Windy
        |   0 C -2 C   20 C 15 C
        | ----- ----- ----- -----
        |    No No      Yes No    Maybe
        |
        """), s.getvalue())

    @test
    def alternativeFormatting(self):
        """Check that simple table (only strings) draws OK."""
        t = Table.Table()
        t.addColumn("North", "%.2f", "%s", uline=1, ljust=1)
        t.addColumn("East ", "%.2f", "%s", uline=1, ljust=1)
        t.addColumn("South", "%.2f", "%s", uline=1, ljust=1)
        t.addColumn("West ", "%.2f", "%s", uline=1, ljust=1)
        t.addRow("Cold", "Chill", "Warm", "Windy")
        t.addRow(0, -2, 20, 15)
        s = StringIO()
        t.draw(s)
        failUnlessEqualStrings(literalText2Text("""
        | North East  South West
        | ===== ===== ===== =====
        | Cold  Chill Warm  Windy
        | 0.00  -2.00 20.00 15.00
        |
        """), s.getvalue())


if __name__ == "__main__":
    runModule()
