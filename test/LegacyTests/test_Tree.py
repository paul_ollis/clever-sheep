#!/usr/bin/env python
"""CleverSheep.VisTools.Tree unit tests"""

import six

from six.moves import cStringIO as StringIO

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import DataMaker
from CleverSheep.Log import FileTracker
from CleverSheep.Prog.Curry import Curry

from TestSupport.Files import FileBasedTests, ensureNoFile


# The module under test.
from CleverSheep.VisTools import Tree

makeText = Curry(DataMaker.literalText2Text, noTail=True)

class N(object):
    """A node class to build trees for testing.

    """
    def __init__(self, name):
        self.name = name
        self.children = []



class TreeDrawing(Suite):
    """Drawing tree of various kinds.

    """
    @test
    def very_simple_tree(self):
        """Create and a very simple tree.

        The tree has a single parent and single child node.

        """
        a = N('A')
        b = N('B')
        a.children = [b]
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=lambda n:n.name,
                stream=s)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | `--o->A
        |    `---->B
        |
        """), s.getvalue())

    @test
    def empty_tree(self):
        """A tree with not child nodes.

        """
        a = N('A')
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=lambda n:n.name,
                stream=s)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | `---->A
        |
        """), s.getvalue())

    @test
    def complex_tree(self):
        """A complex, nested tree with multiple child nodes.

        This is a kind of stress test.

        """
        a = N('A')
        a.children = [N(c) for c in "BCDEF"]
        a.children[0].children = [N(c) for c in "NOP"]
        a.children[0].children[1].children = [N(c) for c in "XY"]
        a.children[3].children = [N(c) for c in "12"]
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=lambda n:n.name,
                stream=s)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | `--o->A
        |    |--o->B
        |    |  |---->N
        |    |  |--o->O
        |    |  |  |---->X
        |    |  |  `---->Y
        |    |  `---->P
        |    |---->C
        |    |---->D
        |    |--o->E
        |    |  |---->1
        |    |  `---->2
        |    `---->F
        |
        """), s.getvalue())

    @test
    def components(self):
        """The tree building components can be altered.

        """
        a = N('A')
        a.children = [N(c) for c in "DEF"]
        a.children[1].children = [N(c) for c in "12"]
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=lambda n:n.name,
                stream=s, chars='+@=/:')

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | +==@=:A
        |    /====:D
        |    /==@=:E
        |    /  /====:1
        |    /  +====:2
        |    +====:F
        |
        """), s.getvalue())

    @test
    def tall_form(self):
        """Can draw trees with extra vertical spacing.

        """
        a = N('A')
        a.children = [N(c) for c in "BEF"]
        a.children[1].children = [N(c) for c in "12"]
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=lambda n:n.name,
                stream=s, tall=True)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        |
        | `--o->A
        |    |
        |    |---->B
        |    |
        |    |--o->E
        |    |  |
        |    |  |---->1
        |    |  |
        |    |  `---->2
        |    |
        |    `---->F
        |
        """, noTail=True), s.getvalue())

    @test
    def multiline_nodes(self):
        """Nodes with multipl lines are handled.

        """
        def asText(n):
            if n.name in "AB12":
                return "Value is:\n  %s" % n.name
            return n.name

        a = N('A')
        a.children = [N(c) for c in "BEF"]
        a.children[1].children = [N(c) for c in "12"]
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=asText,
                stream=s)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | `--o->Value is:
        |    |    A
        |    |---->Value is:
        |    |       B
        |    |--o->E
        |    |  |---->Value is:
        |    |  |       1
        |    |  `---->Value is:
        |    |          2
        |    `---->F
        |
        """, noTail=True), s.getvalue())

    @test
    def prefix(self):
        """A prefix can be added to any node.

        """
        def asText(n):
            if n.name in "AB12":
                return "Value is:\n  %s" % n.name
            return n.name

        def prefix(n):
            if n.name in "B2":
                return "==>"
            return "   "

        a = N('A')
        a.children = [N(c) for c in "BEF"]
        a.children[1].children = [N(c) for c in "12"]
        s = StringIO()
        Tree.drawTree(a,
                getChildren=lambda n: n.children, strFunc=asText,
                stream=s, prefixFunc=prefix)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        |    `--o->Value is:
        |       |    A
        | ==>   |---->Value is:
        |       |       B
        |       |--o->E
        |       |  |---->Value is:
        |       |  |       1
        | ==>   |  `---->Value is:
        |       |          2
        |       `---->F
        |
        """, noTail=True), s.getvalue())


if __name__ == "__main__":
    runModule()

