#!/usr/bin/env python
"""CleverSheep.Prog.Curry unit tests"""

import sys

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.Prog import Curry
Curry = Curry.Curry


def testFunc(a, b, c, x=None, y=None, z=None):
    return " ".join([str(v) for v in (a, b, c, x, y, z)])

@test
def test_01_anon_args():
    """Check use of just anonymous args."""
    f = Curry(testFunc)
    failUnlessEqual(f(1, 2, 3), "1 2 3 None None None")
    f = Curry(testFunc, 1)
    failUnlessEqual(f(2, 3), "1 2 3 None None None")
    f = Curry(testFunc, 1, 2)
    failUnlessEqual(f(3), "1 2 3 None None None")
    f = Curry(testFunc, 1, 2, 3)
    failUnlessEqual(f(), "1 2 3 None None None")

    failUnlessEqual(f(4), "1 2 3 4 None None")
    failUnlessEqual(f(4, 5), "1 2 3 4 5 None")
    failUnlessEqual(f(4, 5, 6), "1 2 3 4 5 6")

    failUnlessEqual(f.function(), testFunc)

@test
def test_02_kwargs():
    """Check use of keyword args."""
    f = Curry(testFunc, 1, 2, 3)
    failUnlessEqual(f(y=5), "1 2 3 None 5 None")
    failUnlessEqual(f(x=4), "1 2 3 4 None None")
    failUnlessEqual(f(z=6, x=4), "1 2 3 4 None 6")

@test
def test_03_mixed():
    """Check use of mixed args."""
    f = Curry(testFunc, 1)
    failUnlessEqual(f(2, 3), "1 2 3 None None None")

    failUnlessEqual(f(2, 3, y=5), "1 2 3 None 5 None")
    failUnlessEqual(f(2, 3, x=4), "1 2 3 4 None None")
    failUnlessEqual(f(2, 3, z=6, x=4), "1 2 3 4 None 6")

@test
def test_04_append():
    """Check use of append method."""
    f = Curry(testFunc)
    failUnlessEqual(f(1, 2, 3), "1 2 3 None None None")

    f.append(1)
    failUnlessEqual(f(2, 3), "1 2 3 None None None")

    f.append(2, 3)
    failUnlessEqual(f(), "1 2 3 None None None")

    failUnlessEqual(f(4), "1 2 3 4 None None")
    failUnlessEqual(f(4, 5), "1 2 3 4 5 None")
    failUnlessEqual(f(4, 5, 6), "1 2 3 4 5 6")

@test
def test_05_set():
    """Check use of set method."""
    f = Curry(testFunc, 1, 2, 3)
    f.set(y=5)
    failUnlessEqual(f(y=5), "1 2 3 None 5 None")

    f = Curry(testFunc, 1, 2, 3)
    f.set(x=4)
    failUnlessEqual(f(x=4), "1 2 3 4 None None")

    f = Curry(testFunc, 1, 2, 3)
    f.set(z=6, x=4)
    failUnlessEqual(f(z=6, x=4), "1 2 3 4 None 6")


if __name__ == "__main__":
    runModule()
