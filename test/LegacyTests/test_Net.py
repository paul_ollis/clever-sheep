#!/usr/bin/env python
"""CleverSheep.Test.Net unit tests"""

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Test import Net


class IpAddress(Suite):
    """Tests for the IpAddress class."""
    @test
    def set_valid_address(self):
        """Verify valid addresses are accepted."""
        a = Net.IpAddress("0.0.0.0")
        a = Net.IpAddress("255.255.255.255")
        a = Net.IpAddress("127.0.0.1")
        a = Net.IpAddress(0x00000000)
        a = Net.IpAddress(0xFFFFFFFF)

    @test
    def invalid_address(self):
        """Verify invalid addresses raise exception."""
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "256.0.0.0")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "0.256.0.0")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "0.0.256.0")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "0.0.0.256")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "10.1.1.")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "10.1.1")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "0.0.0.-1")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, "0.0.0.a")
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, -1)
        failUnlessRaises(Net.InvalidFormat, Net.IpAddress, 0x100000000)

    @test
    def ival(self):
        """Verify the integer representation works."""
        a = Net.IpAddress("0.0.0.0")
        failUnlessEqual(0, a.ival)
        a = Net.IpAddress("255.255.255.255")
        failUnlessEqual(0xFFFFFFFF, a.ival)

    @test
    def hval(self):
        """Verify the hexadecimal representation works."""
        a = Net.IpAddress("0.0.0.0")
        failUnlessEqual("0x00000000", a.hval)
        a = Net.IpAddress("255.255.255.255")
        failUnlessEqual("0xffffffff", a.hval)

    @test
    def sval(self):
        """Verify the dotted representation works."""
        a = Net.IpAddress("0.0.0.0")
        failUnlessEqual("0.0.0.0", a.sval)
        a = Net.IpAddress("255.255.255.255")
        failUnlessEqual("255.255.255.255", a.sval)
        a = Net.IpAddress("1.10.100.199")
        failUnlessEqual("1.10.100.199", a.sval)

    @test
    def str(self):
        """The IpAddress.__str__ maps to sval.
        
        This basically repeats the `sval` test, but uses ``str()``.

        """
        a = Net.IpAddress("0.0.0.0")
        failUnlessEqual("0.0.0.0", str(a))
        a = Net.IpAddress("255.255.255.255")
        failUnlessEqual("255.255.255.255", str(a))
        a = Net.IpAddress("1.10.100.199")
        failUnlessEqual("1.10.100.199", str(a))

    @test
    def bytes(self):
        """Verify the bytes array representation works."""
        a = Net.IpAddress("0.0.0.0")
        failUnlessEqual([0, 0, 0, 0], a.bytes)
        a = Net.IpAddress("255.255.255.255")
        failUnlessEqual([255, 255, 255, 255], a.bytes)
        a = Net.IpAddress("1.10.100.199")
        failUnlessEqual([1, 10, 100, 199], a.bytes)

    @test
    def incrementing(self):
        """Verify incrementing addresses works."""
        a = Net.IpAddress("0.0.0.0")
        a.increment()
        failUnlessEqual(1, a.ival)
        a.increment(10)
        failUnlessEqual(11, a.ival)
        a.increment(-1)
        failUnlessEqual(10, a.ival)

        a = Net.IpAddress("0.0.1.255")
        a.increment()
        failUnlessEqual("0.0.2.0", a.sval)

        a = Net.IpAddress("254.255.255.255")
        a.increment()
        failUnlessEqual("255.0.0.0", a.sval)

    @test
    def set_using_address(self):
        """Verify we can construct one IpAddress from another."""
        a = Net.IpAddress("127.0.0.1")
        b = Net.IpAddress(a)
        failUnlessEqual("127.0.0.1", b.sval)


class InetAddr(Suite):
    """Tests for the InetAddress class."""
    @test
    def basic(self):
        """Verify we can set address and port"""
        a = Net.InetAddr("1.10.100.199", 1234)
        failUnlessEqual("1.10.100.199", a.sval)
        failUnlessEqual(1234, a.port)


class MaskedIpAddress(Suite):
    """Tests for the MaskedIpAddress class."""
    @test
    def set_valid_address(self):
        """Verify valid addresses are accepted."""
        a = Net.MaskedIpAddress("0.0.0.0")
        a = Net.MaskedIpAddress("255.255.255.255", 24)
        a = Net.MaskedIpAddress("127.0.0.1", 0)
        a = Net.MaskedIpAddress(0x00000000, 31)
        a = Net.MaskedIpAddress(0xFFFFFFFF, 16)
        a = Net.MaskedIpAddress("127.0.0.1/24")

    @test
    def invalid_address(self):
        """Verify invalid addresses raise exception."""
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "256.0.0.0")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "0.256.0.0")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "0.0.256.0")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "0.0.0.256")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "10.1.1.")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "10.1.1")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "0.0.0.-1")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, "0.0.0.a")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, -1)
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress, 0x100000000)
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress,
                0x7f000001, -1)
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress,
                0x7f000001, 33)
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress,
                "10.1.1.0/-1")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress,
                "10.1.1.0/33")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress,
                "10.1.1.0/24/16")
        failUnlessRaises(Net.InvalidFormat, Net.MaskedIpAddress,
                "10.1.1.0/a")

    @test
    def ival(self):
        """Verify the integer representation works."""
        a = Net.MaskedIpAddress("0.0.0.0")
        failUnlessEqual(0, a.ival)
        a = Net.MaskedIpAddress("255.255.255.255")
        failUnlessEqual(0xFFFFFFFF, a.ival)

    @test
    def hval(self):
        """Verify the hexadecimal representation works."""
        a = Net.MaskedIpAddress("0.0.0.0")
        failUnlessEqual("0x00000000", a.hval)
        a = Net.MaskedIpAddress("255.255.255.255")
        failUnlessEqual("0xffffffff", a.hval)

    @test
    def sval(self):
        """Verify the dotted representation works."""
        a = Net.MaskedIpAddress("0.0.0.0")
        failUnlessEqual("0.0.0.0/32", a.sval)
        a = Net.MaskedIpAddress("255.255.255.255", 24)
        failUnlessEqual("255.255.255.255/24", a.sval)
        a = Net.MaskedIpAddress("1.10.100.199", 0)
        failUnlessEqual("1.10.100.199/0", a.sval)

    @test
    def bytes(self):
        """Verify the bytes array representation works."""
        a = Net.MaskedIpAddress("0.0.0.0")
        failUnlessEqual([0, 0, 0, 0], a.bytes)
        a = Net.MaskedIpAddress("255.255.255.255")
        failUnlessEqual([255, 255, 255, 255], a.bytes)
        a = Net.MaskedIpAddress("1.10.100.199")
        failUnlessEqual([1, 10, 100, 199], a.bytes)

    @test
    def bits(self):
        """Verify the bits property works."""
        a = Net.MaskedIpAddress("0.0.0.0/0")
        failUnlessEqual(0, a.bits)
        a = Net.MaskedIpAddress("0.0.0.0/32")
        failUnlessEqual(32, a.bits)

    @test
    def mask(self):
        """Verify the mask property works."""
        a = Net.MaskedIpAddress("0.0.0.0/0")
        failUnlessEqual(0, a.mask)
        a = Net.MaskedIpAddress("0.0.0.0/32")
        failUnlessEqual(0xffffffff, a.mask)

    @test
    def hmask(self):
        """Verify the hmask property works."""
        a = Net.MaskedIpAddress("0.0.0.0/0")
        failUnlessEqual("0x00000000", a.hmask)
        a = Net.MaskedIpAddress("0.0.0.0/32")
        failUnlessEqual("0xffffffff", a.hmask)

    @test
    def incrementing(self):
        """Verify incrementing addresses works."""
        a = Net.MaskedIpAddress("0.0.0.0")
        a.increment()
        failUnlessEqual(1, a.ival)
        a.increment(10)
        failUnlessEqual(11, a.ival)
        a.increment(-1)
        failUnlessEqual(10, a.ival)

        a = Net.MaskedIpAddress("0.0.1.255")
        a.increment()
        failUnlessEqual("0.0.2.0/32", a.sval)

        a = Net.MaskedIpAddress("254.255.255.255")
        a.increment()
        failUnlessEqual("255.0.0.0/32", a.sval)

    @test
    def set_using_address(self):
        """Verify we can construct one MaskedIpAddress from another."""
        a = Net.MaskedIpAddress("127.0.0.1/24")
        b = Net.MaskedIpAddress(a)
        failUnlessEqual("127.0.0.1/24", b.sval)

        a = Net.IpAddress("127.0.0.1")
        b = Net.MaskedIpAddress(a)
        failUnlessEqual("127.0.0.1/32", b.sval)


if __name__ == "__main__":
    runModule()
