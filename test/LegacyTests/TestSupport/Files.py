#!/usr/bin/env python
"""File manipulation support for unit testing.

This module provides:

  - Various functions to create directories, files, etc.
      1. `ensureDir`
      2. `ensureDirs`
      3. `ensureFile`
      4. `ensureFiles`
      5. `ensureSymLink`

  - Various functions to make sure files, etc. do not exist.
      1. `ensureNoFile`

  - Various functions to help clean up after tests have been run.
      1. `delFiles`
      2. `delDirs`
"""


import shutil
import os
import errno

from CleverSheep.Test.Tester import Suite


def normpath(path):
    """Like os.path.normpath but also collapses '//'."""
    return os.path.normpath(path).replace("//", "/")


def ensureDir(path, made=None):
    """Make sure that the specified directory exists.

    @Return:
        The absolute form of path.
    @Param path:
        The name of the directory required. May be relative to the CWD.
    """
    # Code is modified from os.makedirs.
    made = made or []
    path = os.path.abspath(path)
    head, tail = os.path.split(path)
    if not tail:
        head, tail = os.path.split(head)

    if head and tail and not os.path.exists(head):
        try:
            made = ensureDir(head, made)
        except OSError as e:
            # be happy if someone already created the path
            if e.errno != errno.EEXIST:
                raise
        if tail == os.curdir: # xxx/newdir/. exists if xxx/newdir exists
            return made

    if not os.path.exists(path):
        os.mkdir(path)
        made.append(os.path.abspath(path))

    return made


def ensureDirs(dirNames):
    """Make sure that the specified set of directories exist.

    @Return:
        A list of the absolute paths of all names in dirNames.
    @Param dirNames:
        A list of directory names.
    """
    made = []
    for path in dirNames:
        made.extend(ensureDir(path))
    return made


def ensureFile(path, contents=None):
    """Make sure that the specified file exists.

    @Return:
        The absolute form of path.
    @Param path:
        The name of the file required. May be relative to the CWD.
    """

    dirs = []
    path = os.path.abspath(path)
    # dirname, base = os.path.split(path)
    # ensureDir(dirname)
    if contents is None:
        contents = path
    dirs = ensureDir(os.path.dirname(path))
    if not os.path.exists(path):
        f = open(path, "w")
        f.write(contents)
        f.close()

    return path, dirs


def ensureSymLink(path, targetPath):
    """Make sure that the specified symbolic link exists.

    @Return:
        The absolute form of path.
    @Param path:
        The name of the sym link required. May be relative to the CWD.
    """
    path = os.path.abspath(path)
    # dirname, base = os.path.split(path)
    # ensureDir(dirname)
    if not os.path.islink(path):
        os.symlink(targetPath, path)

    return path


def ensureNoFile(path):
    """Make sure that the specified file does not exist.

    @Param path:
        The name of the file that must not exist.
    """
    if os.path.exists(path):
        os.remove(path)


def ensureFiles(fileNames):
    """Make sure that the specified set of files exist.

    @Return:
        A list of the absolute paths of all names in fileNames.
    @Param fileNames:
        A list of directory names.
    """
    files, dirs = [], []
    for path in fileNames:
        newFile, newDirs = ensureFile(path)
        files.append(newFile)
        dirs.extend(newDirs)
    return files, dirs


def delFiles(fileNames):
    """Try to remove the specified set of directories.

    @Return:
        A list of any files that could not be removed.
        All names are converted to the absolute form.
    @Param fileNames:
        A list of file names.
    """
    absPaths = []

    for path in fileNames:
        if os.path.exists(path):
            try:
                os.unlink(path)
            except OSError:
                if path not in absPaths:
                    absPaths.append(path)

    return absPaths


def delDirs(dirNames):
    """Try to remove the specified set of directories.

    @Return:
        A list of any directories that could not be removed.
        All names are converted to the absolute form.
    @Param dirNames:
        A list of directory names.
    """
    absPaths = []
    def noteFailure(function, path, excinfo):
        dirName = os.path.abspath(path)
        if not os.isdir(dirName):
            dirName = os.path.dirname(dirName)

        if dirName not in absPaths:
            absPaths.append(dirName)

    for path in dirNames:
        if os.path.isdir(path):
            shutil.rmtree(path, 0, noteFailure)

    return absPaths


class FileBasedTests(Suite):
    """Mixin: Provides support for tests that involve manipulation of files."""

    def setUp (self):
        self.dirNames = []
        self.fileNames = []
        self.links = []
        self.cwd = os.getcwd()

    def tearDown (self):
        os.chdir(self.cwd)
        delFiles(self.fileNames)
        for path in self.fileNames:
            if path.endswith(".py"):
                delFiles([path + "c"])
        delDirs(self.dirNames)
        self.dirNames = []
        self.fileNames = []

    def addDir(self, root, tail=None):
        if tail is None:
            dir = tail = root
        else:
            dir = os.path.join(root, tail)
            a, b = os.path.split(tail)
            r = root
            while True:
                dd = os.path.join(r, a)
                if dd in self.dirNames:
                    break
                self.dirNames.append(dd)

        self.dirNames.extend(ensureDir(dir))

    def addFile(self, file, contents=None):
        path, dirs = ensureFile(file, contents)
        self.fileNames.append(path)
        self.dirNames.extend(dirs)

    def addSymLink(self, name, target):
        self.links.append(ensureSymLink(name, target))

    def addDirs(self, dirs):
        for dir in dirs:
            self.addDir(dir)

    def addFiles(self, files, contents=None):
        files, dirs = ensureFiles(files)
        self.dirNames.extend(dirs)
