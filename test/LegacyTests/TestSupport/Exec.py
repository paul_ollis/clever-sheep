#!/usr/bin/env python
"""Test execution support.

This module provides:

  - Enhanced Test Runner class for Unit Tests. This is a slightly modified
    version of the TextTestRunner, which is part of the standard Python
    unittest module.

@Todo:
  Theere are a number of other utilities to be documented or removed.
"""
from __future__ import print_function

import sys
import os
import unittest
import time
import re


class Struct: pass
state = Struct()
state.passes = 0
state.failures = 0
# state.redirector = Redirector("/@Results", clear=1)

def reset():
    state.passes = 0
    state.failures = 0
    # state.redirector = Redirector("/@Results", clear=1)


def check(v, expectType, expectValue, testName):
    if not isinstance(v, expectType):
        print("FAIL", testName)
        print("  Expected type %s but got %s\n" % (expectType, type(v)))
        state.failures += 1
    elif v != expectValue:
        print("FAIL", testName)
        print("  Expected value %s but got %s\n" % (expectValue, v))
        state.failures += 1
    else:
        state.passes += 1


def summarise():
    total = state.passes + state.failures
    if state.failures:
        print("Ran %d tests, %d passed and %d failed" % (
            total, state.passes, state.failures))
    else:
        print("Ran %d tests, all passed" % (total))

    # state.redirector = None


# ============================================================================
def flush():
    t = time.time()
    if (t - state.prevFlush) > 0.1:
        sys.stdout.flush()
        state.prevFlush = t


# ============================================================================
class Tee(object):
    def __init__(self, path, f2, mode="a"):
        self.f = file(path, mode)
        self.f2 = f2
        self.text = ""

    def write(self, text):
        self.f.write(text)
        self.f2.write(text)


class Struct: pass
state = Struct()
state.tee = Tee("test.log", sys.stdout, mode="w")
state.stdout = sys.stdout
state.stderr = sys.stderr
state.prevFlush = time.time()


# ============================================================================
class _WritelnDecorator:
    """Used to decorate file-like objects with a handy 'writeln' method"""
    def __init__(self,stream):
        self.stream = stream

    def __getattr__(self, attr):
        return getattr(self.stream,attr)

    def writeln(self, *args):
        if args: self.write(*args)
        self.write('\n') # text-mode streams translate to \r\n if needed


class _TextTestResult(unittest.TestResult):
    """A test result class that can print(formatted text results to a stream.)

    Used by TextTestRunner.
    """
    dispWidth = 78
    separator1 = '#' * dispWidth
    separator2 = '=' * dispWidth
    separator3 = '-' * dispWidth

    def __init__(self, stream, descriptions, verbosity):
        unittest.TestResult.__init__(self)
        self.stream = stream
        self.showAll = verbosity > 1
        self.dots = verbosity == 1
        self.descriptions = descriptions
        self.xxxName = ""

    def getCaseDescription (self, test):
        doc = test.__doc__
        return doc and doc.split("\n")[0].strip() or str(test)

    def getDescription(self, test):
        if self.descriptions:
            return test.shortDescription() or str(test)
        else:
            return str(test)

    def startTest(self, test):
        unittest.TestResult.startTest(self, test)
        if self.showAll:
            xxxName = self.getCaseDescription(test)
            if xxxName != self.xxxName:
                self.stream.writeln("")
                self.stream.writeln(xxxName)
                self.stream.writeln(self.separator3)
                self.xxxName = xxxName
            self.stream.write("%-*s" % (
                    self.dispWidth - 6, self.getDescription(test) + " ... "))

    def addSuccess(self, test):
        unittest.TestResult.addSuccess(self, test)
        if self.showAll:
            self.stream.writeln("   ok")
        elif self.dots:
            self.stream.write('.')

    def addError(self, test, err):
        unittest.TestResult.addError(self, test, err)
        if self.showAll:
            self.stream.writeln("ERROR")
        elif self.dots:
            self.stream.write('E')

    def addFailure(self, test, err):
        unittest.TestResult.addFailure(self, test, err)
        if self.showAll:
            self.stream.writeln("FAIL")
        elif self.dots:
            self.stream.write('F')

    def printErrors(self):
        if self.dots or self.showAll:
            self.stream.writeln()
        self.printErrorList('ERROR', self.errors)
        self.printErrorList('FAIL', self.failures)

    def printErrorList(self, flavour, errors):
        for test, err in errors:
            self.stream.writeln(self.separator1)
            self.stream.writeln("%s: %s" % (flavour,self.getDescription(test)))
            self.stream.writeln(self.separator2)
            self.stream.writeln("%s" % err)


class TextTestRunner:
    """A test runner class that displays results in textual form.

    It prints out the names of tests as they are run, errors as they
    occur, and a summary of the results at the end of the test run.
    """
    def __init__(self, stream=state.tee, descriptions=1, verbosity=1):
        self.stream = _WritelnDecorator(stream)
        self.descriptions = descriptions
        self.verbosity = verbosity

    def _makeResult(self):
        return _TextTestResult(self.stream, self.descriptions, self.verbosity)

    def run(self, test):
        "Run the given test case or test suite."
        result = self._makeResult()
        startTime = time.time()
        test(result)
        stopTime = time.time()
        timeTaken = float(stopTime - startTime)
        result.printErrors()
        self.stream.writeln(result.separator2)
        run = result.testsRun
        self.stream.writeln("Ran %d test%s in %.3fs" %
                            (run, run != 1 and "s" or "", timeTaken))
        self.stream.writeln()
        if not result.wasSuccessful():
            self.stream.write("FAILED (")
            failed, errored = map(len, (result.failures, result.errors))
            if failed:
                self.stream.write("failures=%d" % failed)
            if errored:
                if failed: self.stream.write(", ")
                self.stream.write("errors=%d" % errored)
            self.stream.writeln(")")
        else:
            self.stream.writeln("OK")
        return result


class MyExc(AssertionError):
    def __init__(self, msgOrCallable):
        if callable(msgOrCallable):
            msg = msgOrCallable()
        else:
            msg = msgOrCallable
        AssertionError.__init__(self, msg)


class TestCase(unittest.TestCase):
    """TestCase with modified assertion methods

    This modifies the standard failUnless, etc. to support a callable as an
    alternative to the plain C{msg} parameter.
    """
    failureException = MyExc


def make_Tests(globs, prefix="test_"):
    # TODO: Is this function re-usable for all test modules?
    loader = unittest.TestLoader()
    loader.testMethodPrefix = "test_"

    suite = unittest.TestSuite ()
    rTest = re.compile(r"Test_[0-9]+_")

    klassNames = [n for n, k in globs.items() if rTest.match(n)]
    klassNames.sort()
    for name in klassNames:
        # TODO: Nasty hack for Python 2.2
        suite.addTests (loader.loadTestsFromTestCase(globs[name])._tests)

    return suite


def run_Harness(globs, prefix="test_"):
    # "hi"
    suite = make_Tests(globs, prefix=prefix)
    runner = TextTestRunner (verbosity=2)
    results = runner.run (suite)
    if results.errors or results.failures:
        return 1
    return 0

