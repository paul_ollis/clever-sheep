#!/usr/bin/env python
"""CleverSheep.Prog.Inspect unit tests"""

import os

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Prog import Inspect


def doCall2(func):
    return func()


def doCall(func):
    return doCall2(func)


class CallerLocation(Suite):
    """Tests for the callerLocation function."""
    @test
    def immediateCaller(self):
        """Verify we can get the immediate caller's location"""
        ret = []
        def local():
            path, lNum = Inspect.callerLocation()
            ret[:] = [path, lNum]

        doCall(local)
        path = os.path.basename(ret[0])
        failUnlessEqual("test_Inspect.py", path)
        failUnlessEqual(15, ret[1])

    @test
    def secondLevelCaller(self):
        """Verify we can get the earlier caller's location"""
        ret = []
        def local():
            path, lNum = Inspect.callerLocation(2)
            ret[:] = [path, lNum]

        doCall(local)
        path = os.path.basename(ret[0])
        failUnlessEqual("test_Inspect.py", path)
        failUnlessEqual(19, ret[1])


class CallerFrame(Suite):
    """Tests for the callerFrame function."""
    @test
    def immediateCaller(self):
        """Verify we can get the immediate caller's frame"""
        funcCode = []
        def local():
            frame = Inspect.callerFrame()
            funcCode.append(frame.f_code)
            del frame

        doCall(local)
        failUnless(funcCode[0] is doCall2.__code__)

    @test
    def immediateCaller(self):
        """Verify we can get the immediate caller's frame"""
        funcCode = []
        def local():
            frame = Inspect.callerFrame(2)
            funcCode.append(frame.f_code)
            del frame

        doCall(local)
        failUnless(funcCode[0] is doCall.__code__)



if __name__ == "__main__":
    runModule()
