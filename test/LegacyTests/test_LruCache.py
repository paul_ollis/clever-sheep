#!/usr/bin/env python
"""CleverSheep.Prog.LruCache unit tests"""

import sys
import random
import copy

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Prog import LruCache

def make_full_cache(del_cb=None):
    """Create a cache and fill it.

    :Return:
        A tuple of (data, lru). The data is a list of the values used to
        fill the cache and ``lru`` is the cache itself.
    """
    data = list(range(20))
    lru = LruCache.LruCache(maxSize=100, delCB=del_cb)
    for n in data:
        lru.add(key=n, value=n*2, size=5)
    return data, lru


class Test_LruCache(Suite):
    """Tests for the LruCache module."""
    @test
    def fill_up_cache(self):
        """Verify we can add items up to the cache size limit.

        We also check that aldding a single small extra item flushes the
        oldest item fro the cache.
        """
        # We should be able find all values in a newly filled cache.
        data, lru = make_full_cache()
        for n in data:
            v = lru.get(n)
            failIf(v[0] is None)
            failUnlessEqual((n * 2, 5), v)

        # Since we have accessed all members in order, the entry for '0' should
        # still be the oldest. So adding an item of size '1' should push out it
        # out.
        data.append(len(data))
        n = data[-1]
        lru.add(key=n, value=n*2, size=1)
        for n in data[0:1]:
            v = lru.get(n)
            failUnless(v[0] is None)

        for n in data[1:]:
            v = lru.get(n)
            failIf(v[0] is None)
            failUnlessEqual(n * 2, v[0])

    @test
    def iteration(self):
        """Verify that we can iterate through the cached list.

        The iteration should yield items in oldest first order.
        """
        data, lru = make_full_cache()

        # Do some proper lookups and verify that the resulting queue is still in
        # order.
        keys = list(data)
        random.shuffle(keys)
        for k in [0, 19] + keys:
            n = data[k]
            data.remove(n)
            data.append(n)
            v = lru.get(n)
            failUnlessEqual((n * 2, 5), v)

            def oops():
                return "At index=%d, key=%d: expected %d but got %d" % (
                            i, data[i], data[i] * 2, v)
            for i, v in enumerate(lru):
                failUnlessEqual(data[i] * 2, v, makeMessage=oops)

    @test
    def iteration_data_time_size(self):
        """Verify that we can iterate using iterDataTimeSize.

        The iteration should yield items in oldest first order.
        """
        data, lru = make_full_cache()

        # Do some proper lookups and verify that the resulting queue is still
        # in order.
        keys = list(data)
        random.shuffle(keys)
        for k in [0, 19] + keys:
            n = data[k]
            data.remove(n)
            data.append(n)
            v = lru.get(n)
            failUnlessEqual((n * 2, 5), v)

            def oops():
                return "At index=%d, key=%d: expected %d but got %d" % (
                            i, data[i], data[i] * 2, v)
            prevT = 0
            for i, (v, t, size) in enumerate(lru.iterDataTimeSize()):
                failUnlessEqual(5, size)
                failUnlessEqual(data[i] * 2, v, makeMessage=oops)
                failUnless(prevT <= t)
                prevT = t

    @test
    def cycling(self):
        """Verify that can cycle new values into the cache.

        We start with a full cache and then get some values, whilst
        occasionally adding new values into the cache.
        """
        def getOops():
            return ("Did not get expected value of %r on iteration %r\n"
                    " actual value was %r and key was %r"
                    % (n * 2, idx, v, n))
        data, lru = make_full_cache()

        # Do some proper lookups and verify that the resulting queue is still
        # in order.
        keys = list(data)
        random.shuffle(keys)
        for idx, k in enumerate([0, 19] + keys):
            n = data[k]
            data.remove(n)
            data.append(n)
            v = lru.get(n)
            failUnlessEqual((n * 2, 5), v, makeMessage=getOops)

            if idx % 2 == 0:
                n = idx + len(data)
                lru.add(n, n * 2, 5)
                data.append(n)
                del data[0]

            def oops():
                return "At index=%d, key=%d: expected %d but got %d" % (
                            i, data[i], data[i] * 2, v)
            for i, v in enumerate(lru):
                failUnlessEqual(data[i] * 2, v, makeMessage=oops)

    @test
    def delete_cb(self):
        """Check that the delete callback feature works.

        We start with a full cache and then add some new values, verifying that
        the delete callback is invoked for the items that fall out of the cache.
        """
        def del_cb(key, value):
            failUnlessEqual(key * 2, value)
            failUnless(key in data)
            data.remove(key)

        data, lru = make_full_cache(del_cb=del_cb)

        failUnless(0 in data)
        lru.add(key=21, value=100, size=5)
        failIf(0 in data)
        lru.add(key=22, value=101, size=5)
        failIf(1 in data)
        v = lru.get(2)
        lru.add(key=23, value=102, size=5)
        failIf(3 in data)

    @test
    def no_delete_cb_on_remove(self):
        """Check that the delete callback is not invoked by remove operations.

        """
        def del_cb(key, value):
            fail("Delete callback should not have been invoked")

        data, lru = make_full_cache(del_cb=del_cb)

        lru.remove(0)
        failUnlessEqual((None, 0), lru.get(0))
        lru.remove(19)
        failUnlessEqual((None, 0), lru.get(19))
        lru.remove(9)
        failUnlessEqual((None, 0), lru.get(9))
        failUnlessEqual(17, len(lru))


    @test
    def get_set_state(self):
        """Verify that we can get and restore (set) the state.

        """
        data, lru = make_full_cache()
        origSeq = [v for v in lru]

        # Save the original state. We need to make a deep copy because the
        # state contains objects. In real use the state would be, for example
        # pickled, and we are effectively emulating such a scenario.
        state = copy.deepcopy(lru.getState())

        # Change the LRU state and veiry the change.
        v = lru.get(5)
        curSeq = [v for v in lru]
        failIfEqual(origSeq, curSeq)

        # Restore the state and verify that the LRU's order is restored.
        lru.setState(state)
        curSeq = [v for v in lru]
        failUnlessEqual(origSeq, curSeq)


if __name__ == "__main__":
    runModule()
