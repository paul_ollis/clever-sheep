#!/usr/bin/env python
"""CleverSheep.Prog.Args unit tests.

The tests for argument parsing.

"""

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.Prog import Args


@test
def tuplise_list():
    """A list should become a tuple."""
    failUnlessEqual((1, 2, 3), Args.tuplise([1, 2, 3]))

@test
def tuplize_tuple():
    """Verify the alternative name ``tuplize`` works."""
    failUnlessEqual((1, 2, 3), Args.tuplize((1, 2, 3)))


class Test_iterise(Suite):
    """Tests for the iterise function.
    
    This function converts things into flattened, iterators.

    """
    @test
    def iterise_is_generator(self):
        """Verify that iterise is a generator.

        This is important for some reason.

        """
        gen = iter([])
        it = Args.iterise([])
        failUnlessEqual(type(gen), type(it))

    @test
    def iterise_None(self):
        """None should yield no elements"""
        i = -1
        for i, el in enumerate(Args.iterise(None)):
            pass
        failUnlessEqual(-1, i)

    @test
    def iterise_list(self):
        """A list should be iterated as expected."""
        i = -1
        elements = []
        for i, el in enumerate(Args.iterise([1, 2, 3])):
            elements.append(el)
        failUnlessEqual(2, i)
        failUnlessEqual([1, 2, 3], elements)

    @test
    def iterise_tuple(self):
        """A tuple should be iterated as expected."""
        i = -1
        elements = []
        for i, el in enumerate(Args.iterise((1, 2, 3))):
            elements.append(el)
        failUnlessEqual(2, i)
        failUnlessEqual([1, 2, 3], elements)

    @test
    def iterise_gen(self):
        """A generator should be iterated as expected."""
        i = -1
        elements = []
        for i, el in enumerate(Args.iterise(iter((1, 2, 3)))):
            elements.append(el)
        failUnlessEqual(2, i)
        failUnlessEqual([1, 2, 3], elements)

    @test
    def iterise_string(self):
        """A multi-character string should yield a single element."""
        i = -1
        elements = []
        for i, el in enumerate(Args.iterise("Hello")):
            elements.append(el)
        failUnlessEqual(0, i)
        failUnlessEqual(["Hello"], elements)

    @test
    def iterise_simple_value(self):
        """A simple value, such as an integer should be yield a single element."""
        i = -1
        elements = []
        for i, el in enumerate(Args.iterise(22)):
            elements.append(el)
        failUnlessEqual(0, i)
        failUnlessEqual([22], elements)

    @test
    def iterise_instance(self):
        """An class instance should be yield a single element."""
        class X:
            pass
        i = -1
        elements = []
        x = X()
        for i, el in enumerate(Args.iterise(x)):
            elements.append(el)
        failUnlessEqual(0, i)
        failUnlessEqual([x], elements)
        failUnless(elements[0] is x)

    @test
    def iterize_list(self):
        """Verify the alternative name ``iterize`` works."""
        i = -1
        elements = []
        for i, el in enumerate(Args.iterize([1, 2, 3])):
            elements.append(el)
        failUnlessEqual(2, i)
        failUnlessEqual([1, 2, 3], elements)


class Test_listize(Suite):
    """Tests for the listise function."""
    @test
    def listize_is_list(self):
        """Verify that listise returns a list."""
        li = Args.listise([])
        failUnlessEqual(type([]), type(li))

    @test
    def listise_tuple(self):
        """A tuple should become a list."""
        failUnlessEqual([1, 2, 3], Args.listise((1, 2, 3)))

    @test
    def listize_tuple(self):
        """Verify the alternative name ``listize`` works."""
        failUnlessEqual([1, 2, 3], Args.listize((1, 2, 3)))


class Test_tuplize(Suite):
    """Tests for the tuplise function."""
    @test
    def tuplize_is_tuple(self):
        """Verify that tuplise returns a tuple."""
        li = Args.tuplise([])
        failUnlessEqual(type(()), type(li))

    @test
    def tuplise_list(self):
        """A list should become a tuple."""
        failUnlessEqual((1, 2, 3), Args.tuplise([1, 2, 3]))

    @test
    def tuplize_tuple(self):
        """Verify the alternative name ``tuplize`` works."""
        failUnlessEqual((1, 2, 3), Args.tuplize((1, 2, 3)))


if __name__ == "__main__":
    runModule()
