#!/usr/bin/env python
"""Various parts of the Tester support infrastructure.

"""

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Test.TestEventStore import TestEvent


class NonEqualEvents(Suite):
    """The built-in TestEvent class.

    Sibling sub-types of TextEvent or any two sub-types that ultimately inherit
    from sibling sub-types of TextEvent should not compare equal.

    """
    @test(issue=78, testID="sub-types-not-equal")
    def siblings(self):
        """Sibling TextEvent sub-types should not compare equal.

        """
        class E1(TestEvent):
            pass

        class E2(TestEvent):
            pass

        e1 = E1()
        e2 = E2()
        failIfEqual(e1, e2,
                "Sibling subtypes of TestEvent should not compare equal.")

    @test(issue=78, testID="child-can-be-equal")
    def childToParent(self):
        """Sibling TextEvent sub-types should not compare equal.

        """
        class E1(TestEvent):
            pass

        class E2(E1):
            pass

        e1 = E1()
        e2 = E2()
        failUnlessEqual(e1, e2,
                "A child of TestEvent should, by default compare equal"
                " to its parent instance")

    @test(issue=78, testID="grandkids-not-equal")
    def grandkidsNotEqual(self):
        """Grandchild TextEvent sub-types should not compare equal.

        """
        class E1(TestEvent): pass
        class E2(TestEvent): pass
        class G1(E1): pass
        class G2(E2): pass

        g1 = G1()
        g2 = G2()
        failIfEqual(g1, g2,
                "Grandchild subtypes of TestEvent should not compare equal.")


if __name__ == "__main__":
    runModule()
