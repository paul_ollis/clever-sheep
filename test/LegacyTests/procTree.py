#!/usr/bin/env python
"""Small program that creates a small process tree."""


import os
import subprocess
import time
import signal

def cleanUp(*args):
    os.kill(proc1.pid, 9)
    os.kill(proc2.pid, 9)


proc1 = subprocess.Popen(("sleep", "123456"))
proc2 = subprocess.Popen(("sleep", "123457"))

signal.signal(15, cleanUp)

f = open("procTree.started", "w")
f.close()

time.sleep(10)
cleanUp()



