#!/usr/bin/env python
"""CleverSheep.Prog.Enum unit tests"""

import six

import sys
import random
import six.moves.cPickle as pickle

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils


# The module under test.
from CleverSheep.Prog import Enum


class Test_Enum(Suite):
    """Tests for the Enum class.

    The Enum class supports definitions of enumerated types and values
    of a particular type.

    """
    def setUp(self):
        Enum._definedTypes.clear()

    @test
    def basic(self):
        """Basic use of Enums"""
        Colour = Enum.Enum("Colour")
        red = Colour("Red")
        blue = Colour("Blue")
        failUnlessEqual(0, red)
        failUnlessEqual(1, blue)
        failUnless(isinstance(red, int))

    @test
    def basic(self):
        """Same enum value name provides the same value, when not strict."""
        Colour = Enum.Enum("Colour", strict=False)
        red = Colour("Red")
        blue = Colour("Blue")
        also_red = Colour("Red")
        failUnlessEqual(0, red)
        failUnlessEqual(1, blue)
        failUnlessEqual(0, also_red)

    @test
    def string_conversion(self):
        """Conversion to strings"""
        Colour = Enum.Enum("Colour")
        red = Colour("Red")
        failUnlessEqual("Red", str(red))
        failUnlessEqual("Colour:Red=0", repr(red))

    @test
    def same_name_different_type(self):
        """The same enum name is allowed for a different type"""
        Colour = Enum.Enum("Colour")
        blue = Colour("Blue")
        red = Colour("Red")
        Shade = Enum.Enum("Shade")
        redShade = Shade("Red")
        failUnlessEqual(1, red)
        failUnlessEqual(0, redShade)
        failUnlessEqual("Colour:Red=1", repr(red))
        failUnlessEqual("Shade:Red=0", repr(redShade))

    @test
    def duplicate_type(self):
        """A duplicate enum type name raises exception"""
        Colour = Enum.Enum("Colours")
        failUnlessRaisesMsg(Enum.DuplicateTypeName, Enum.Enum,
                "There is already an Enum called 'Colours'", "Colours")

    @test
    def duplicate_enum(self):
        """A duplicate enum type name raises exception.

        By default, any attempt to define a duplicate name for an Enum
        will raise `Enum.DuplicateEnum`.

        """
        #> Create a ``Colours`` enum.
        Colour = Enum.Enum("Colours")

        #> Add ``red`` then try to add ``red`` again. This should raise
        #> `Enum.DuplicateEnum`.
        red = Colour("Red")
        failUnlessRaisesMsg(Enum.DuplicateEnum, Colour,
                "There is already an Enum called 'Red' for Colours", "Red")

    @test
    def pickling(self):
        """Enums should pickle and unpickle OK.

        The internal implementation of Enum has some special code to handle
        unpickling, hence this test.

        """
        Colour = Enum.Enum("Colour", strict=False)
        red = Colour("Red")
        blue = Colour("Blue")
        x = (red, blue)
        xx = pickle.loads(pickle.dumps(x, -1))
        failUnlessEqual(0, xx[0])
        failUnlessEqual(1, xx[1])
        failUnless(x[0] is xx[0])
        failUnless(x[1] is xx[1])


if __name__ == "__main__":
    runModule()

