#!/usr/bin/env python
"""CleverSheep.Prog.Dict unit tests"""

import sys
import weakref

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.Prog import Dict


class Test_TrackerDict(Suite):
    """Tests for the TrackerDict class."""
    def setUp(self):
        class MyTracker(Dict.TrackerDict):
            def onAdd(self, key):
                this().added.setdefault(key, 0)
                this().added[key] += 1
            def onDel(self, key):
                this().added.setdefault(key, 0)
                this().added[key] -= 1
        this = weakref.ref(self)
        self.added = {}
        self.MyTracker = MyTracker

    @test
    def set_key_invokes_onAdd(self):
        """Check that onAdd method gets called as expected."""
        a = self.MyTracker()
        b = self.MyTracker()
        a["x"] = None
        failUnlessEqual(1, self.added["x"])
        b["x"] = None
        failUnlessEqual(2, self.added["x"])

    @test
    def onAdd_only_for_first_setting_op(self):
        """Check that onAdd only gets invoked the first time a value is set
        for a given key.

        If we do:<py>:

            a = self.MyTracker()
            a["x"] = None
            a["x"] = "test"

        Then ``onAdd`` should only be invoked for the first setting of
        ``a["x"]``.
        """
        a = self.MyTracker()
        b = self.MyTracker()
        a["x"] = None
        failUnlessEqual(1, self.added["x"])
        a["x"] = "test"
        failUnlessEqual(1, self.added["x"])

    @test
    def del_key_invokes_onDel(self):
        """Check that onDel is invoked when a key is deleted."""
        a = self.MyTracker()
        b = self.MyTracker()
        a["x"] = None
        b["x"] = None
        failUnlessEqual(2, self.added["x"])
        del a["x"]
        failUnlessEqual(1, self.added["x"])
        del b["x"]
        failUnlessEqual(0, self.added["x"])

    @test
    def all_ways_to_set_a_value_trigger_onAdd(self):
        """Check that all different ways to set a dictionary value trigger
        a call to onAdd.
        """
        a = self.MyTracker()

        a["x"] = None
        a["x"] = "test"
        failUnlessEqual(1, self.added["x"])

        b = self.MyTracker()
        b.setdefault("x", None)
        b.setdefault("x", "test")
        failUnlessEqual(2, self.added["x"])

        c = self.MyTracker()
        c.update(a)
        failUnlessEqual(3, self.added["x"])

        d = c.copy()
        failUnlessEqual(4, self.added["x"])

        e = c.fromkeys(a.keys(), None)
        failUnlessEqual(5, self.added["x"])

        f = self.MyTracker(a)
        failUnlessEqual(6, self.added["x"])

        g = self.MyTracker((("x", None), ("y", None),))
        failUnlessEqual(7, self.added["x"])

        h = self.MyTracker(x=None, y=None)
        failUnlessEqual(8, self.added["x"])

    @test
    def destruction_invokes_onDel(self):
        """Verify that destruction of a TrackerDict causes onDel to be invoked
        for all keys.
        """
        a = self.MyTracker()
        b = self.MyTracker()
        a["x"] = None
        failUnlessEqual(1, self.added["x"])
        b["x"] = None
        b["y"] = None
        failUnlessEqual(2, self.added["x"])
        failUnlessEqual(1, self.added["y"])

        del b
        failUnlessEqual(1, self.added["x"])
        failUnlessEqual(0, self.added["y"])


class Test_KeyCountDict(Suite):
    """Tests for the KeyCountDict class."""
    def setUp(self):
        class MyCounter(Dict.KeyCountDict):
            keyCounts = {}
        self.MyCounter = MyCounter

    @test
    def set_increments_count(self):
        """Check that setting a value makes keys get counted."""
        a = self.MyCounter()
        b = self.MyCounter()
        a["x"] = None
        failUnlessEqual(1, self.MyCounter.keyCounts["x"])
        b["x"] = None
        failUnlessEqual(2, self.MyCounter.keyCounts["x"])

    @test
    def increment_only_for_first_setting_op(self):
        """Check that count only gets incremented the first time a value is set
        for a given key.

        If we do:<py>:

            a = self.MyCounter()
            a["x"] = None
            a["x"] = "test"

        Then ``self.MyCounter.keyCounts["x"])`` should only be ``1``.
        """
        a = self.MyCounter()
        b = self.MyCounter()
        a["x"] = None
        failUnlessEqual(1, self.MyCounter.keyCounts["x"])
        a["x"] = "test"
        failUnlessEqual(1, self.MyCounter.keyCounts["x"])

    @test
    def del_key_decrements_or_removes_count(self):
        """Check that count is decremented when a key is deleted and
        removed if it reaches zero."""
        a = self.MyCounter()
        b = self.MyCounter()
        a["x"] = None
        b["x"] = None
        failUnlessEqual(2, self.MyCounter.keyCounts["x"])
        del a["x"]
        failUnlessEqual(1, self.MyCounter.keyCounts["x"])

        failUnless("x" in self.MyCounter.keyCounts)
        del b["x"]
        failUnless("x" not in self.MyCounter.keyCounts)

    @test
    def destruction_decrements_or_deletes_counts(self):
        """Check that count is decremented/deleted when a KeyCountDict is
        destroyed"""
        a = self.MyCounter()
        b = self.MyCounter()
        a["x"] = None
        failUnlessEqual(1, self.MyCounter.keyCounts["x"])
        b["x"] = None
        b["y"] = None
        failUnlessEqual(2, self.MyCounter.keyCounts["x"])
        failUnlessEqual(1, self.MyCounter.keyCounts["y"])
        failUnless("y" in self.MyCounter.keyCounts)

        del b
        failUnlessEqual(1, self.MyCounter.keyCounts["x"])
        failUnless("y" not in self.MyCounter.keyCounts)

if __name__ == "__main__":
    runModule()
