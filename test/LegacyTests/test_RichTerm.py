#!/usr/bin/env python
"""CleverSheep.TTY_Utils.RichTerm unit tests.

This is a bit tricky because the whole idea is that you have a terminal and
the output has bold and pretty colours.

However, at least with a proper OS, there are ways. Well at least one way I
have thought of, which is to use the ``script`` program.

However that involves running tests as separate processes, so for now this
module is less ambitious.
"""
from __future__ import print_function

import six
from six.moves import reload_module

import CheckEnv

import os
import subprocess
import sys

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.TTY_Utils import RichTerm


def getoutput(cmd):
    try:
        return subprocess.check_output(
            cmd, shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        return ""


def bad_setupterm():
    import curses
    raise curses.error


def bad_tigetstr(a):
    import curses
    raise curses.error


class Struct: pass

class Test_RichTerm(Suite):
    """Main tests for the RichTerm module."""
    def suiteSetUp(self):
        # TODO: Should detect that we are not using the local library.
        self.saved = Struct()
        self.saved.curses = RichTerm.curses
        self.saved.cap = RichTerm.cap
        self.saved.colourCode = RichTerm.colourCode
        self.saved.stdout = sys.stdout
        self.saved.stderr = sys.stderr

    def setUp(self):
        import curses
        self.curses = curses
        self.saved_setupterm = curses.setupterm
        self.saved_tigetstr = curses.tigetstr
        self.out = RichTerm.RichTerminal(sys.stdout)

    def tearDown(self):
        import curses
        curses.setupterm = self.saved_setupterm
        curses.tigetstr = self.saved_tigetstr
        RichTerm.curses = self.saved.curses
        RichTerm.cap = self.saved.cap
        RichTerm.colourCode = self.saved.colourCode
        sys.stdout = self.saved.stdout
        sys.stderr = self.saved.stderr

    @test
    def get_dims(self):
        """Test getting the terminal's dimensions."""
        lines, columns = self.out.getDims()
        text = getoutput("stty size")
        try:
            l, c = text.split()
            l, c = [int(s) for s in (l, c)]
        except ValueError:
            l, c = 24, 80
        failUnlessEqual((l, c), (lines, columns))

    @test
    def set_width(self):
        """Setting an arbitrary terminal width."""
        l, c = self.out.getDims()
        self.out.set_width(c - 2)
        lines, columns = self.out.getDims()
        failUnlessEqual((l, c - 2), (lines, columns))

    @test
    def get_dim_fail(self):
        """Dimensions cannot be obtained from environ.

        This occurs when ROWS and COLUMNS are not set the environment.
        In this case, the 'stty' command will be tried and if that is no good
        then a default of 24, 80 will be used.

        """
        saved = os.environ.get("ROWS", None), os.environ.get("COLUMNS", None)
        os.environ["ROWS"], os.environ["COLUMNS"] = "", ""
        try:
            lines, columns = RichTerm.getTerminalSize()
        finally:
            r, c = saved
            if r is not None:
                os.environ["ROWS"] = r
            else:
                del os.environ["ROWS"]
            if c is not None:
                os.environ["COLUMNS"] = c
            else:
                del os.environ["COLUMNS"]
        text = getoutput("stty size")
        try:
            l, c = text.split()
            l, c = [int(s) for s in (l, c)]
        except ValueError:
            l, c = 24, 80
        failUnlessEqual((l, c), (lines, columns))

    @test
    def get_dim_use_env(self):
        """Dimensions are obtained from the environment.

        We set specific dimensions in the environment then call the
        getTerminalSize function. The environment values should over-ride
        actual terminal size.

        """
        text = getoutput("stty size")
        try:
            l, c = text.split()
            l, c = [int(s) for s in (l, c)]
        except ValueError:
            l, c = 24, 80

        saved = os.environ.get("ROWS", None), os.environ.get("COLUMNS", None)
        dims = tuple([(x+1) for x in (l, c)])
        os.environ["ROWS"], os.environ["COLUMNS"] = [str(x) for x in dims]
        try:
            lines, columns = RichTerm.getTerminalSize()
        finally:
            r, c = saved
            if r is not None:
                os.environ["ROWS"] = r
            else:
                del os.environ["ROWS"]
            if c is not None:
                os.environ["COLUMNS"] = c
            else:
                del os.environ["COLUMNS"]

        failUnlessEqual(dims, (lines, columns))

    @test
    def coverage_hacking(self):
        """Some good old coverage hacking.

        This is only really for coverage. This is not as bad as it sounds
        because this stuff cannot easily be automatically verified, but it is
        pretty much guaranteed to be executed by automated testing. However, I
        prefer to get covergae by execution, rather than mark code as
        untestable.
        """
        s = RichTerm.colourSeq(fg="blue", bg="red", bold=1)
        out, err = sys.stdout, sys.stderr
        try:
            RichTerm.wrapStdxxx()
        finally:
            sys.stdout, sys.stderr = out, err

        try:
            RichTerm.wrapStdxxx(columns=40)
        finally:
            sys.stdout, sys.stderr = out, err

    def doReload(self, mp, check=None):
        pid = os.fork()
        if pid == 0:
            mp()
            self.curses.setupterm = bad_setupterm
            try:
                reload_module(RichTerm)
            except Exception as exc:
                os._exit(1)
            if check is not None:
                check()
            os._exit(0)
        else:
            pid, st = os.waitpid(pid, 0)
        return st

    @test
    def cannot_init_1(self):
        """The RichTerm module should gracefully handle failure of
        curses.setupterm.

        For this test we over-ride the ``setupterm`` function in the curses
        module with a version that raises curses.error. Then reload the
        RichTerm module, which should work without any errors.

        """
        def mp():
            self.curses.setupterm = bad_setupterm
        st = self.doReload(mp)
        failUnlessEqual(0, st)

    @test
    def cannot_init_2(self):
        """The RichTerm module should gracefully handle failure of
        curses.tigetstr.

        This is pretty much like `cannot_init_1`, but this time we make
        ``curses.tigetstr`` appear to fail.
        """
        def mp():
            self.curses.tigetstr = bad_tigetstr
        st = self.doReload(mp)
        failUnlessEqual(0, st)

    @test
    def init_failure_uses_null(self):
        """If initialisation fails then we should get dumb terminal behaviour.
        """
        def mp():
            self.curses.setupterm = bad_setupterm
        def check():
            s = RichTerm.colourSeq(fg="blue", bg="red", bold=1)
            if s !=  "":
                os._exit(1)
            if not isinstance(RichTerm.curses, RichTerm.NullCurses):
                os._exit(1)
        st = self.doReload(mp, check)
        failUnlessEqual(0, st)


class Test_NullCurses(Suite):
    """Test for the NullCurses class."""
    @test
    def all_empty_string(self):
        """All the NullCurses methods should return empty strings."""
        n = RichTerm.NullCurses()
        failUnlessEqual(six.b(""), n.
                tigetstr(""))
        failUnlessEqual(six.b(""), n.setupterm())
        failUnlessEqual(six.b(""), n.tparm("", ""))


colourNames = (
        "black", "red", "green", "yellow",
        "blue", "magenta", "cyan", "white")


if __name__ == "__main__":
    runModule()
