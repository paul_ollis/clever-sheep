#!/usr/bin/env python3
"""The Clever Sheep Test Suite"""

import CheckEnv

from CleverSheep.Test import Tester

if __name__ == "__main__":
    Tester.runTree()
