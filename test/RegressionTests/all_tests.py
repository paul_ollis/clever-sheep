#!/usr/bin/env python
"""Regression Tests.

These tests cover regression testing bugs that have been fixed to make sure
they are not reintroduced

"""
import CheckEnv

from CleverSheep.Test import Tester


if __name__ == "__main__":
    Tester.runTree()

