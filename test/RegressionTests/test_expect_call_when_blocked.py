#!/usr/bin/env python
"""Regression test a potential race condition fixed in 0.6.5"""
import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import TestEventStore


class ExpectCallRaceCondition(Suite):
    """Test the race condition in the expect function"""

    @test
    def test_race_condition(self):
        """Reproduce the situation causing incorrect event removal

        Previously when expecting an event what would occur is two timers would
        be queued up, one to trigger immediately and one to trigger after the
        check interval. If the first timer callback finds the event the second
        should be removed. If a different callback is already queued and takes
        longer than the check interval to run both of our check callbacks occur
        as all expired timeouts are triggered. This leads to two matching events
        being removed from the event store when only one should have been.

        While a quite specific sequence of events have to occur for this to
        happen it is a bug that was seen in the wild.
        """
        event_store = TestEventStore.eventStore
        event_store.maxAge = 60

        # Add two events to the event store
        event_store.addEvent(TestEventStore.TestEvent(value=1))
        event_store.addEvent(TestEventStore.TestEvent(value=2))

        # Add a function on a timeout that takes a long time
        def long_running_callback():
            x = 0
            for i in range(10000):
                for p in range(1000):
                    x += 1

        self.control.addRepeatingTimeout(0.1, long_running_callback)
        self.control.addRepeatingTimeout(0.2, long_running_callback)

        # Call expect using a check function that will match both events
        def match(event):
            if event.value < 5:
                return event

        def check():
            event = event_store.find(predicate=match)
            if event is not None:
                event_store.remove(event)
            return event

        def error_msg():
            return "Failed to find expected event"

        self.control.delay(1)
        self.control.expect(1.0, check, error_msg)

        # Check only one event was removed
        failUnlessEqual(1, len(event_store))


if __name__ == "__main__":
    runModule()

