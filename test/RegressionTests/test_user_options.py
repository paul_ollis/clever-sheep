#!/usr/bin/env python
"""Regression test an issue where the help text for user defined options was
not being shown"""
import CheckEnv

import subprocess

from CleverSheep.Test.Tester import failUnless
from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test import Tester

option_help_text = "Test option to check help text"
Tester.add_argument("--test-option", action="store_true",
                    help=option_help_text)


class UserOptionSuite(Suite):
    """User Option Help Text Regression Suite"""

    @test(testID='user_option_help_text')
    def test_user_option_help_text_is_displayed(self):
        """Check that the defined user option comes out in the help text"""
        output = subprocess.check_output(["./test_user_options.py", "--help"])
        failUnless(option_help_text in output.decode('latin-1'))


if __name__ == "__main__":
    runModule()
